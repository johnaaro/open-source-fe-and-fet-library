##---------------------------------------------------------------------------##
##
## .cmake/General/configure_defaults.cmake
##
## Copyright (C) 2019 Oak Ridge National Laboratory, UT-Battelle, LLC.
##---------------------------------------------------------------------------##
## CMAKE for configuration of project defaults
##---------------------------------------------------------------------------##
cmake_minimum_required (VERSION 3.12.4)

include (colors)
include (formats)
include (PreventInSourceBuild)
include (platform)

##---------------------------------------------------------------------------##
##                   end of .cmake/General/configure_defaults.cmake
##---------------------------------------------------------------------------##
