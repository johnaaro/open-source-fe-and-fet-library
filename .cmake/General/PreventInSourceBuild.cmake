##---------------------------------------------------------------------------##
##
## .cmake/General/PreventInSourceBuild.cmake
##
## Copyright (C) 2019 Oak Ridge National Laboratory, UT-Battelle, LLC.
##---------------------------------------------------------------------------##
## CMAKE for preventing in-source project builds
##---------------------------------------------------------------------------##
cmake_minimum_required (VERSION 3.12.4)
include (colors)

macro (PreventInSourceBuild)

  # If in a directory with a 'CMakeLists.txt' file, terminate execution.
  file(TO_CMAKE_PATH "${PROJECT_BINARY_DIR}/CMakeLists.txt" LOC_PATH)
  if (EXISTS "${LOC_PATH}")
    message(FATAL_ERROR
      "\n"
      "${Red}ERROR!${ColorReset}\n"
      "     CMAKE_CURRENT_SOURCE_DIR="
      "${LightBlue}${CMAKE_CURRENT_SOURCE_DIR}${ColorReset}\n"
      "     CMAKE_CURRENT_BINARY_DIR="
      "${LightMagenta}${CMAKE_CURRENT_BINARY_DIR}${ColorReset}\n"
      "\n"
      "The ${PROJECT_NAME} library does not support in-source builds!\n"
      "     You must now delete the CMakeCache.txt file and the CMakeFiles/\n"
      "     directory under the current ${PROJECT_NAME} directory or you will\n"
      "     not be able to configure correctly!"
      "\n"
      "You must now run something like the following:${Cyan}\n"
      "   $ rm -r CMakeCache.txt CMakeFiles/\n"
      "${ColorReset}\n"
      "\n"
      "Please create a different directory and configure the library"
      "     under that different directory, such as:${Cyan}\n"
      "   $ mkdir MY_BUILD\n"
      "   $ cd    MY_BUILD\n"
      "   $ cmake [OPTIONS] ../\n${ColorReset}"
      "\n"
      )
  endif()
endmacro()

##---------------------------------------------------------------------------##
##                   end of .cmake/General/PreventInSourceBuild.cmake
##---------------------------------------------------------------------------##
