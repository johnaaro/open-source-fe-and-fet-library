##---------------------------------------------------------------------------##
##
## .cmake/Utilities/library.cmake
##
## Copyright (C) 2019 Oak Ridge National Laboratory, UT-Battelle, LLC.
##---------------------------------------------------------------------------##
## CMAKE for subpackaging libraries (based off of TriBITS)
##---------------------------------------------------------------------------##
cmake_minimum_required (VERSION 3.12.4)
include (tools)
include (test)

##---------------------------------------------------------------------------##
## Forward-declares a library and declares local variables that are helpful
##---------------------------------------------------------------------------##
macro(library LIBNAME_IN)

  set(LIB_NAME_SHORT "${LIBNAME_IN}")
  set(LIBRARY_NAME "${PROJECT_NAME}_${LIBNAME_IN}")

endmacro()


##---------------------------------------------------------------------------##
## Wrapper for adding a sub-library
##---------------------------------------------------------------------------##
macro(add_library_wrapper)
  # Print configuration message
  message(STATUS
    "-- Configuring the ${Yellow}${LIBRARY_NAME}${ColorReset} library"
    )

  # Parse arguments
  parse_arguments(
    LIB                            # Prefix
    "SOURCES;DEPENDENCIES;FLAGS"   # Arguments
    "BOOST;EXCLUDE_FROM_ALL"       # Optional arguments
    ${ARGN}                        # All arguments given to wrapper
    )

  # Process if library is to be excluded from the 'all' target or not
  set(EXCLUDED "")
  if (${LIB_EXCLUDE_FROM_ALL})
    set(EXCLUDED EXCLUDE_FROM_ALL)
  endif()

  # Set if library is static or shared
  set(lib_type "STATIC")
  if (${PROJECT_NAME}.SHARED)
    set(lib_type "SHARED")
  endif()

  # Create library and it's alias
  add_library(${LIBRARY_NAME} "${lib_type}" ${EXCLUDED} "")
  add_library(${PROJECT_NAME}::${LIB_NAME_SHORT} ALIAS ${LIBRARY_NAME})

  # Install header files to common location
  file(GLOB LIB_HEADERS "*.hh")
  install_headers(
    HEADERS  ${LIB_HEADERS}
    SUBDIR  "${LIBRARY_NAME}"
    )

  # Set the sources
  target_sources(
    ${LIBRARY_NAME}
    PUBLIC
    ${LIB_HEADERS}
    PRIVATE
    ${LIB_SOURCES}
    )

  # Set CACHE variable for the include directory of the library
  #    Note: This is used for external projects and for libraries here
  set(
    ${LIBRARY_NAME}_INCLUDE_DIR
    "${CMAKE_CURRENT_SOURCE_DIR}"
    CACHE PATH
    "Include directory for the ${LIBRARY_NAME} library"
    )

  # Set include directories. This prevents developers of the library and
  # externally from needing to know the exact location of the headers.
  target_include_directories(
    ${LIBRARY_NAME}
    PRIVATE
    "${CMAKE_CURRENT_SOURCE_DIR}"
    )
  # Populate the global include directories
  include_directories(
    ${CMAKE_CURRENT_SOURCE_DIR}
    )

  # Set dependencies
  if (LIB_DEPENDENCIES)
    # Prepend the "${PROJECT_NAME}_" to the library name
    set (LIBS "")
    foreach (LIB ${LIB_DEPENDENCIES})
      list(APPEND LIBS "${PROJECT_NAME}_${LIB}")
    endforeach()
    if (${PROJECT_NAME}.Boost AND ${LIB_BOOST})
      list(APPEND LIBS ${Boost_LIBRARIES})
    endif()
    target_link_libraries(
      ${LIBRARY_NAME}
      PRIVATE
      ${LIBS}
      )

    # Append the dependency header files to the target's
    foreach (LIB ${LIBS})
      target_include_directories(
        ${LIBRARY_NAME}
        PUBLIC
        "$<BUILD_INTERFACE:${${LIB}_INCLUDE_DIR}>"
        )
    endforeach()
  endif()

  # Create target compiler flags now
  target_compile_options(
    ${LIBRARY_NAME}
    PRIVATE
    # Use either Debug or Release flags
    $<$<CONFIG:Debug>:${${PROJECT_NAME}_CXX_FLAGS_DEBUG}>
    $<$<CONFIG:Release>:${${PROJECT_NAME}_CXX_FLAGS_RELEASE}>
    # Add any special library compilation flags
    ${LIB_FLAGS}
    )

  # Create target compile definitions too
  target_compile_definitions(
    ${LIBRARY_NAME}
    PUBLIC
    ${${PROJECT_NAME}_compile_definitions}
    )

  target_compile_features(
    ${LIBRARY_NAME}
    PUBLIC
    cxx_std_11
    # cxx_std_14
    # cxx_std_17
    # cxx_std_20
    )
  set_target_properties(
    ${LIBRARY_NAME}
    PROPERTIES
    CXX_EXTENSIONS OFF
    )

  # Post-process the library
  postprocess_library()


endmacro()

##---------------------------------------------------------------------------##
## Postprocessing of a library
##---------------------------------------------------------------------------##
function(postprocess_library)

  # Include test for the library, if present
  add_test_directory(test tests)

  # Enable exporting for external projects to find package with a find_package()
  # TRIBITS_WRITE_PACKAGE_CLIENT_EXPORT_FILES(${PACKAGE_NAME})
endfunction()



##---------------------------------------------------------------------------##
##                   end of .cmake/utilities/library.cmake
##---------------------------------------------------------------------------##
