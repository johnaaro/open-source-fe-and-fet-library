##---------------------------------------------------------------------------##
##
## .cmake/Utilities/tools.cmake
##
## Copyright (C) 2019 Oak Ridge National Laboratory, UT-Battelle, LLC.
##---------------------------------------------------------------------------##
## CMAKE for some simple macros to simplify the CMake usage
##---------------------------------------------------------------------------##
cmake_minimum_required (VERSION 3.12.4)
PreventInSourceBuild()


##---------------------------------------------------------------------------##
## Adds several sub-directories at once
##---------------------------------------------------------------------------##
macro (add_subdirectories)
  foreach(dir ${ARGV})
    add_subdirectory(${dir})
  endforeach()
endmacro()


##---------------------------------------------------------------------------##
## For prepending text to a string
##---------------------------------------------------------------------------##
macro(prepend var prefix)
  # Establish and create a new list - "list_var" - from any additional args
  set (list_var "")

  # Generate the list with the new prefix
  foreach (f ${ARGN})
    list(APPEND list_var "${prefix}${f}")
  endforeach()

  # Update the provided variable, var, in the calling file
  set(${var} "${list_var}" PARENT_SCOPE)

endmacro()


##---------------------------------------------------------------------------##
## Parses arguments given to a macro
## USAGE:
##    parse_arguments(tst "SOURCES;DEPENDENCIES" "WILL_FAIL" ${ARGN})
## RETURNS:
##    Variables names 'tst_SOURCES', 'tst_DEPENDENCIES', etc. and variables
##    'tst_WILL_FAIL' as booleans
##---------------------------------------------------------------------------##
macro(parse_arguments prefix arg_names option_names)

  # Create variables according to the specified prefix
  foreach(arg_name ${arg_names})
    set(${prefix}_${arg_name} "")
  endforeach()

  # Do the same for options, set to false by default
  foreach(option ${option_names})
    set(${prefix}_${option} FALSE)
  endforeach()

  # Set default variables
  set(DEFAULT_ARGS "")
  set(current_arg_name "${DEFAULT_ARGS}")
  set(current_arg_list "")

  # Loop through arguments now
  foreach(arg ${ARGN})
    # Determine argument name and determine if argument or option
    set(larg_names ${arg_names})
    list(FIND larg_names "${arg}" is_arg_name)
    if (is_arg_name GREATER -1)
      # Argument found
      #
      # Now, set current list of args to the appropriate list
      set(${prefix}_${current_arg_name} "${current_arg_list}")

      # Move to next argument name and reset the arg list
      set(current_arg_name "${arg}")
      set(current_arg_list "")
    else()
      # Option or arg to argument/option:
      set(loption_names "${option_names}")
      list(FIND loption_names "${arg}" is_option)
      if (is_option GREATER -1)
        # Option was found; turn it on
        set(${prefix}_${arg} TRUE)
      else()
        # Argument value found; append to the list of current args
        list(APPEND current_arg_list "${arg}")
      endif()
    endif()
  endforeach()

  # After searching last element, add to the most recently active arg list
  set(${prefix}_${current_arg_name} "${current_arg_list}")
endmacro()

##---------------------------------------------------------------------------##
## For installing headers for library of the current project
##---------------------------------------------------------------------------##
function(install_headers)

  # Parse arguments into headers, directory location, and components
  parse_arguments(
    PARSED                             # Prefix
    "HEADERS;SUBDIR;COMPONENT"         # Lists
    ""                                 # Options
    ${ARGN}                            # Arguments
    )


  # Set installation location
  # \todo: The SUBDIR argument doesn't work!
  set(INSTALL_DIR "/usr/local/include/FET/headers") # ORIG -> ${CMAKE_BINARY_DIR}
  if (PARSED_SUBDIR)
    set(INSTALL_DIR
      "${INSTALL_DIR}/${PARSED_SUBDIR}"
      )
  endif()

  # Set component if given
  if (PARSED_COMPONENT)
    set(COMPONENT ${PARSED_COMPONENT})
  else()
    set(COMPONENT ${PROJECT_NAME})
  endif()

  # Now install the headers
  install(
    FILES        ${PARSED_HEADERS}
    DESTINATION "${INSTALL_DIR}"
    COMPONENT    ${COMPONENT}
    )

endfunction()


##---------------------------------------------------------------------------##
##                   end of .cmake/utilities/tools.cmake
##---------------------------------------------------------------------------##
