##---------------------------------------------------------------------------##
##
## .cmake/Utilities/test.cmake
##
## Copyright (C) 2019 Oak Ridge National Laboratory, UT-Battelle, LLC.
##---------------------------------------------------------------------------##
## CMAKE for creating test executibles
##---------------------------------------------------------------------------##
cmake_minimum_required (VERSION 3.12.4)
include (tools)
include (executable)

##---------------------------------------------------------------------------##
## Adds a test directory for the parent library as specified locally
##---------------------------------------------------------------------------##
function (add_test_directory)
  if (${PROJECT_NAME}.TEST)
    foreach(TEST_DIR ${ARGN})
      if (EXISTS "${CMAKE_CURRENT_SOURCE_DIR}/${TEST_DIR}/CMakeLists.txt")
        add_subdirectory(${TEST_DIR})
      endif()
    endforeach()
  endif()
endfunction()


##---------------------------------------------------------------------------##
## Adds executable to the prefix/text directory
##---------------------------------------------------------------------------##
function(create_test NAME)

  # Parse arguments for creating the executible
  parse_arguments(
    tst   # Prefix
    "SOURCES;DEPENDENCIES;INSTALL;TEST"   # Argument lists
    ""                                    # Optional lists
    ${ARGN}
    )

  # Create test name
  set(tstName "tst${NAME}")

  # Add the executable
  add_executable_wrapper(
    ${tstName}
    SOURCES      ${tst_SOURCES}
    DEPENDENCIES ${tst_DEPENDENCIES}
    INSTALL      ${tst_INSTALL}
    DESTINATION  ${CMAKE_BINARY_DIR}/test   # Test executable in /bin/test
    EXCLUDE_FROM_ALL
    )

  # Now add the test in CTest
  add_test_wrapper(${tstName} ${tst_TEST})

endfunction()


##---------------------------------------------------------------------------##
## Wrapper for CMake internal `add_test`
##---------------------------------------------------------------------------##
function (add_test_wrapper tstExec)
  # Prepend the package's name to the test
  set(tstName "${PROJECT_NAME}_${tstExec}")

  # Add the test
  add_test(
    ${tstName}
    ${tstExec}
    ${ARGN}
    CONFIGURATIONS Debug Release
    )

  # Add to the 'testing' command
  add_dependencies(
    testing
    ${tstExec}
    )
endfunction()


##---------------------------------------------------------------------------##
##                   end of .cmake/utilities/test.cmake
##---------------------------------------------------------------------------##
