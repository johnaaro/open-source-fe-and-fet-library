##---------------------------------------------------------------------------##
##
## .cmake/FET/linting.cmake
##
## Copyright (C) 2019 Oak Ridge National Laboratory, UT-Battelle, LLC.
##---------------------------------------------------------------------------##
## Sets linting as appropriate:
##    FET.[CLANG-TIDY/CPPLINT/CPPCHECK]  [ON]   Flags linter usage (DEBUG ONLY)
##---------------------------------------------------------------------------##
cmake_minimum_required (VERSION 3.12.4)
PreventInSourceBuild()
include (add_option)

# Only using linting in DEBUG
if (NOT ${PROJECT_NAME}.RELEASE)

  # clang-tidy
  add_option(
    "CLANG-TIDY"
    "Use clang-tidy, if available, for linting"
    ON)

  # cpplint
  add_option(
    "CPPLINT"
    "Use cpplint, if available, for linting"
    ON)

  # cppcheck
  add_option(
    "CPPCHECK"
    "Use cppcheck, if available, for linting"
    ON)

  # Look for clang-tidy
  if (${PROJECT_NAME}.CLANG-TIDY)
    find_program(
      CLANG_TIDY_EXE
      NAMES "clang-tidy"
      DOC "Path to clang-tidy executable"
      )
    if (CLANG_TIDY_EXE)
      set(USE_CLANG_TIDY TRUE)
    endif()
  endif()

  # Look for cpplint
  if (${PROJECT_NAME}.CPPLINT)
    find_program(
      CPPLINT_EXE
      NAMES "cpplint"
      DOC "Path to cpplint executable"
      )
    if (CPPLINT_EXE)
      set(USE_CPPLINT TRUE)
    endif()
  endif()

  # Look for cppcheck
  if (${PROJECT_NAME}.CPPCHECK)
    find_program(
      CPPCHECK_EXE
      NAMES "cppcheck"
      DOC "Path to cppcheck executable"
      )
    if (CPPCHECK_EXE)
      set(USE_CPPCHECK TRUE)
    endif()
  endif()


  # Set the linter variables
  set(LINTER)
  if (USE_CLANG_TIDY)
    list(APPEND LINTER "clang-tidy")
    set(CMAKE_CXX_CLANG_TIDY "${CLANG_TIDY_EXE}"
      "-fix"
      )
  endif()
  if (USE_CPPLINT)
    list(APPEND LINTER "cpplint")
    set(CMAKE_CXX_CPPLINT "${CPPLINT_EXE}"
      "--linelength=80"
      "--filter=-whitespace,-readability/todo,-runtime/references"
      "--quiet"
      )
  endif()
  if (USE_CPPCHECK)
    list(APPEND LINTER "cppcheck")
    set(CMAKE_CXX_CPPCHECK "${CPPCHECK_EXE}"
      # "--enable=style,information,warning"
      "--enable=performance,portability,information,warning"
      "--suppress=missingIncludeSystem"
      "--suppress=noExplicitConstructor"
      "--suppress=unmatchedSuppression"
      "--suppress=preprocessorErrorDirective"
      # "--suppress=passedByValue"
      "--quiet"
      )
  endif()

  # Print the linter used:
  if (LINTER)
    message(STATUS "   Analyzing code using the ${LINTER} linter(s).")
  else()
    message(STATUS "   ${Yellow}No supported linter was found.${ColorReset}")
  endif()
endif()

##---------------------------------------------------------------------------##
##                   end of .cmake/FET/linting.cmake
##---------------------------------------------------------------------------##
