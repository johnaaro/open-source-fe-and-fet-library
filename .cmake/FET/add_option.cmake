##---------------------------------------------------------------------------##
##
## .cmake/FET/add_option.cmake
##
## Copyright (C) 2019 Oak Ridge National Laboratory, UT-Battelle, LLC.
##---------------------------------------------------------------------------##
## CMAKE function for adding a new option to the library
##---------------------------------------------------------------------------##
cmake_minimum_required (VERSION 3.12.4)

function(add_option OPTION DESCRIPTION DEFAULT)
  # Modify option to be 'FET.OpenMP', for example
  set(OPTION "${PROJECT_NAME}.${OPTION}")

  # Create the option:
  option(${OPTION}
    "${DESCRIPTION}"
    "${DEFAULT}"
    )
endfunction(add_option)

##---------------------------------------------------------------------------##
##                   end of .cmake/FET/add_option.cmake
##---------------------------------------------------------------------------##
