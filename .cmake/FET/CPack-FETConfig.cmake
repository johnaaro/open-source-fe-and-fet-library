##---------------------------------------------------------------------------##
##
## .cmake/FET/CPack-FETConfig.cmake
##
## Copyright (C) 2019 Oak Ridge National Laboratory, UT-Battelle, LLC.
##---------------------------------------------------------------------------##
## Library configuration package settings
##---------------------------------------------------------------------------##
cmake_minimum_required (VERSION 3.12.4)


if(${PROJECT_NAME}.PACKAGE)

  # SET COMMON VARIABLES
  set(CPACK_PACKAGE_NAME "${PROJECT_NAME}${VERSION_MAJOR}-${VERSION_MINOR}")
  set(CPACK_PACKAGE_VENDOR
    "Idaho State University and Oak Ridge National Laboratory")
  set(CPACK_RESOURCE_FILE "${CMAKE_SOURCE_DIR}/ReadMe.md")

  # SET RESOURCE FILES
  # set(CPACK_RESOURCE_FILE_LICENSE "license.txt")
  set(CPACK_RESOURCE_FILE_README  "ReadMe.md")
  # set(CPACK_RESOURCE_FILE_WELCOME "welcome.txt")

  # STRIP LIBRARY EXECUTABLES
  set(CPACK_STRIP_FILES TRUE)

  # INCLUDE CPACK
  include (CPack)
  message(STATUS
    "${Green}The ${PROJECT_NAME} package is configured${ColorReset}"
    )

endif()

##---------------------------------------------------------------------------##
##                   end of .cmake/FET/CPack-FETConfig.cmake
##---------------------------------------------------------------------------##
