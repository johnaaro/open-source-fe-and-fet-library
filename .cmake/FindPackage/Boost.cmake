##---------------------------------------------------------------------------##
##
## .cmake/FindLibrary/Boost.cmake
##
## Copyright (C) 2019 Oak Ridge National Laboratory, UT-Battelle, LLC.
##---------------------------------------------------------------------------##
## CMAKE for finding use including Boost compilation
##---------------------------------------------------------------------------##
cmake_minimum_required (VERSION 3.12.4)

if (${PROJECT_NAME}.Boost)

  # Use CMake internal to find Boost support
  set(Boost_USE_STATIC_LIBS ON)
  if (${PROJECT_NAME}.SHARED)
    set(Boost_USE_STATIC_LIBS OFF)
  endif()
  find_package(Boost)

  # Include Boost compilation flags if found
  if (Boost_FOUND)
    include_directories(${Boost_INCLUDE_DIR})
    add_definitions(-DBOOST)
  else()
    # Change Boost flag to OFF and warn user
    set(${PROJECT_NAME}.Boost OFF)
    message(WARNING
      "Boost support was not found."
      )
  endif()

endif()
##---------------------------------------------------------------------------##
##                   end of .cmake/FindLibrary/Boost.cmake
##---------------------------------------------------------------------------##
