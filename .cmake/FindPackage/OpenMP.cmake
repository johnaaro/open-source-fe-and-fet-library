##---------------------------------------------------------------------------##
##
## .cmake/FindLibrary/OpenMP.cmake
##
## Copyright (C) 2019 Oak Ridge National Laboratory, UT-Battelle, LLC.
##---------------------------------------------------------------------------##
## CMAKE for finding use including OpenMP compilation
##---------------------------------------------------------------------------##
cmake_minimum_required (VERSION 3.12.4)

if (${PROJECT_NAME}.OpenMP)

  # Use CMake internal to find OpenMP support
  find_package(OpenMP)

  # Include OpenMP compilation flags if found
  if (OPENMP_FOUND)

    # For C
    list(APPEND
      CMAKE_C_FLAGS_DEBUG
      "${OpenMP_C_FLAGS}"
      )
    list(APPEND
      CMAKE_C_FLAGS_RELEASE
      "${OpenMP_C_FLAGS}"
      )

    # For CXX
    list(APPEND
      CMAKE_CXX_FLAGS_DEBUG
      "${OpenMP_CXX_FLAGS}"
      )
    list(APPEND
      CMAKE_CXX_FLAGS_RELEASE
      "${OpenMP_CXX_FLAGS}"
      )

  else()
    # Change OpenMP flag to OFF and warn user
    set(${PROJECT_NAME}.OpenMP OFF)
    message(WARNING
      "OpenMP support was not found"
      )
  endif()

endif()
##---------------------------------------------------------------------------##
##                   end of .cmake/FindLibrary/OpenMP.cmake
##---------------------------------------------------------------------------##
