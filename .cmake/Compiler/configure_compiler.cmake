##---------------------------------------------------------------------------##
##
## .cmake/Compiler/configure_compiler.cmake
##
## Copyright (C) 2019 Oak Ridge National Laboratory, UT-Battelle, LLC.
##---------------------------------------------------------------------------##
## CMAKE for configuring the compiler flags
##---------------------------------------------------------------------------##
cmake_minimum_required (VERSION 3.12.4)
PreventInSourceBuild()

# Specify library compilation definitions
include (compile_definitions)

# Create general lists of project compilation flags
include (generate_flags)

# Load compiler-specific flags
include (${CMAKE_CXX_COMPILER_ID} OPTIONAL RESULT_VARIABLE FOUND)
if (NOT FOUND)
  message (STATUS
    "The ${CMAKE_CXX_COMPILER_ID} C++ compiler ${Red}does not have${ColorReset}"
    " established\n"
    "   Release or Debug flags for this project.\n"
    "   Additional compilation flags are ${Cyan}recommended${ColorReset}"
    " for robustness.")
endif()

# Cache the compiler flag variables
include (cache_flags)


# Print flags to the user:
message (STATUS
  "The ${PROJECT_NAME} library will compile using"
  " ${Yellow}${CMAKE_CXX_COMPILER_ID}${ColorReset} with the\n"
  "   following list of compiler flags:\n"
  "      (Release) ${Cyan}${${PROJECT_NAME}_CXX_FLAGS_RELEASE}${ColorReset}\n"
  "      (Debug)   ${Cyan}${${PROJECT_NAME}_CXX_FLAGS_DEBUG}${ColorReset}"
  )


# Now check for capabilities of the compiler
# include (validate_compiler_capabilities)


##---------------------------------------------------------------------------##
##                   end of .cmake/Compiiler/configure_compiler.cmake
##---------------------------------------------------------------------------##
