##---------------------------------------------------------------------------##
##
## .cmake/Compiler/template.cmake
##
## Copyright (C) 2019 Oak Ridge National Laboratory, UT-Battelle, LLC.
##---------------------------------------------------------------------------##
## CMAKE for configuring the XXX compiler flags for the project
##---------------------------------------------------------------------------##
cmake_minimum_required (VERSION 3.12.4)
PreventInSourceBuild()

# Set default flags for both
list(APPEND
  ${PROJECT_NAME}_CXX_FLAGS
  )


# Add flags for debug
list(APPEND
  ${PROJECT_NAME}_CXX_FLAGS_DEBUG
  )


# Add flags for release
list(APPEND
  ${PROJECT_NAME}_CXX_FLAGS_RELEASE
  )


##---------------------------------------------------------------------------##
##                   end of .cmake/Compiiler/template.cmake
##---------------------------------------------------------------------------##
