##---------------------------------------------------------------------------##
##
## .cmake/Compiler/generate_flags.cmake
##
## Copyright (C) 2019 Oak Ridge National Laboratory, UT-Battelle, LLC.
##---------------------------------------------------------------------------##
## CMAKE for configuring general compiler flags for the project
##---------------------------------------------------------------------------##
cmake_minimum_required (VERSION 3.12.4)
PreventInSourceBuild()

# Create the variable (reset for existing CMakeCache.txt files)
set (${PROJECT_NAME}_CXX_FLAGS "")

# Append any flags already placed into the general CXX flags
if (CMAKE_CXX_FLAGS)
  list(APPEND
    ${PROJECT_NAME}_CXX_FLAGS
    "${CMAKE_CXX_FLAGS}"
    )
endif()

# Append specific flags for our project
list(APPEND
  ${PROJECT_NAME}_CXX_FLAGS
  )


# Set flags for debug
set(
  ${PROJECT_NAME}_CXX_FLAGS_DEBUG
  "${CMAKE_CXX_FLAGS_DEBUG}"
  )


# Set flags for release
set(
  ${PROJECT_NAME}_CXX_FLAGS_RELEASE
  # "${CMAKE_CXX_FLAGS_RELEASE}"
  "-O3"          # Enable compiler optimizations
  "-DNDEBUG"     # Disable assert statements
  )


##---------------------------------------------------------------------------##
##                   end of .cmake/Compiiler/generate_flags.cmake
##---------------------------------------------------------------------------##
