# SHIFT/SCALE Homogenous Examples

Models of a homogenous fuelpin and a Godiva within a SHIFT/SCALE implementation using FET tallies are provided here. These files are structured according to the Omnibus input file documentation using a SCALE geometry model in CSAS5.

## SHIFT Input Files

[godiva_FE_Flux.omn](godiva_FE_Flux.omn) contains mesh and FET tallies for SHIFT where the boundaries are rectangular in nature and encapsulate the traditional Godiva reactor. [PWRInfty.omn](PWRInfty.omn) contains mesh and FET tallies for SHIFT where the boundaries are rectangular in nature and encapsulate one of an infinite array of UO2 fuelpins in water. Both of these files print out their flux results in an HDF5 file. The files can be run from Omnibus with the following command:

```shell
omnibus [filename.omn]
```


## Geometry Files

[godiva.inp](geometry/godiva.inp) is a SCALE CSAS5 Godiva reactor with a continuous energy cross section library. [PWRInfty.inp](geometry/PWRInfty) is a SCALE CSAS5 infinite UO2 lattice 1.5% enriched surrounded by water.


## Questions?


Please direct all questions to the active ExaSMR team, a sub-set of the Computational Engineering And Data Science (CEADS) group at the Idaho State University (ISU), hosted by [Dr. Leslie Kerby](mailto:kerblesl@isu.edu). As of the most recent modification of this file, this team includes, but may not be limited to, the following:


Active developers:
+ [Dr. Leslie Kerby](mailto:kerblesl@isu.edu)
+ [Aaron Johnson](mailto:johnaaro@isu.edu)
+ [Chase Juneau](mailto:junechas@isu.edu)
+ [Emily Elzinga](mailto:elziemil@isu.edu)
+ [Kallie McLaren](mailto:mclakall@isu.edu)
+ [Patience Lamb](mailto:lambpati@isu.edu)

Previous developers:
+ [Katherine Wilsdon](mailto:wilskat7@isu.edu)
+ [Steven Wacker](mailto:wackstev@isu.edu)
+ [Ryan Stewart](mailto:stewryan@isu.edu)


## Acknowledgments

This work was supported by the ExaScale Computing Project (17-SC-2-SC).

<img src="../images/logos/ecp-logo.png" width=150>

<img src="../images/logos/ISULogo.png" width=125>
<img src="../images/logos/ornlLogo-Green.png" width=100>
