# The Functional Expansion Library

This library contains a set of Functional Expansion (FE) and Functional Expansion Tally (FET) tools built from a bottom-up approach using the C++11 standard. Consumers desiring to utilize the library may easily do so.

Note that this work is supported by the ExaScale Computing Project for the purpose of developing FE and FET methods for ExaScale computing in nuclear codes, namely the Shift neutronics code, a model within the SCALE package.

To utilize the library, see [building the library](Building the Library) to build, install, and link to the library.


## What are FEs and FETs?

FEs and FETs provide a powerful approach to obtaining edits, or information, for various applications. One such common application is Fourier series, where a combination of cosine and sinusoidal terms are utilized to represent another function. The applications here focus on providing an alternative to mesh-based edits, or tallies. This is performed by providing a set of basis functions, such as Legendre or Zernike polynomials, to well describe a continuous space and to provide a robust multi-physics coupling for modeling.

Mathematically, FEs and FETs utilize series expansions $`\psi_{n}`$ coupled with coefficients $`a_{n}`$ to re-create a distribution $`F(x)`$.
```math
F(x) = \sum \limits_{n=0}^{N \rightarrow \infty} a_{n} \psi_{n}(x)
```

Where the coefficients $`a_{n}`$ are sampled using the distribution $`F(x)`$ and the series' weighting function $`\rho(x)`$:
```math
a_{n} = \int \limits_{a}^{b} \rho(x) \psi_{n}(x) F(x) dx
```

Examples of such continuous domains include both physical and phase (momentum) space, energy, angular, and temporal domains. While FEs and FETs work best over continuous domains, if a non-continuous domain is modeled, FEs and FETs can still provide a rough estimation of the behavior across the domain, with little loss in accuracy near discontinuities for higher order FEs and FETs. In such cases near discontinuities, FEs and FETs tend to dampen the effect of the discontinuity. Low order FEs and FETs will provide results on average to the values approaching the discontinuities, with higher order FEs and FETs providing more accurate representations with associated Gibb's phenomenum. In the case where discontinuities are known, such as at geometric boundaries, the created FEs and FETs may be split over that domain to provide more accurate representations of the sampled distribution.


## Library Documentation

FE and FET library documentation, [contained here](https://lambpati.gitlab.io/open-source-fe-and-fet-library), is updated with each merge to the master branch automatically. Consumers may additionally generate the documentation external to this location via the following:
```shell
# Create an out-of-source build directory
mkdir build
cd build
cmake ../

# Build the documentation
make docs -j
```


## Building the Library

The FET library uses `CMake` to allow for a diversity of installations on a variety of platforms. Users and libraries ought to easily interface to it. The FET library additionally provides many options for its compilation. The FET library may be built via the following:

```shell
# Create an out-of-source build directory
mkdir build
cd build

# Generate Makefile
cmake [-D[OPTION=ON/OFF]] ../

# Build and install the FET library
make -j
make install
```

Further, individual sub-libraries may be built as well via `make FET_LIBRARY_NAME`. Linking to this library via `CMake` should handle all sub-library dependencies.

The FET library utilizes several options for its build. Note that not all options have been utilized within the creation of the library at this time.  The below documents the available options:

```shell
# Generate Makefile
cmake [-D[OPTION=ON/OFF]] ../

# Options, default value, and purpose
FET.Boost        ON    Compiles with Boost enabled
FET.BUILD_DOCS   ON    Builds library documentation via Doxygen
FET.MPI          OFF   Compiles the library using MPI
FET.OpenMP       OFF   Compiles the library with OpenMP
FET.PACKAGE      OFF   Packages the library
FET.RELEASE      OFF   Builds the library as a release (debug if OFF)
FET.SHARED       ON    Builds the libraries as shared libraries
FET.TEST         ON    Enables testing of the library via CTest

# DEBUG ONLY options (i.e. FET.RELEASE=OFF)
FET.CLANG-TIDY   ON    Enables linting via clang-tidy, if available.
FET.CPPLINT      ON    Enables linting via cpplint, if available.
FET.CPPCHECK     ON    Enables linting via cppcheck, if available.
```

Further usage of CMake and Makefile generation may be found on the [CMake homepage](https://cmake.org/).


## Library Structure

The repository is separated into several main directories. These include:

+ [docs](./docs): Contains the Doxygen configuration file for in-code documentation as well as further details on FEs and FETs.
+ [examples](./examples): Contains example files for running Exnihilo, particularly with usage of the Shift FETs. Examples of single FET usage are not shown here. Such examples, if any, will lie within a `test` sub-directory of the [source](./src) directory.
+ [fuelpin](./fuelpin): Contains models of HB Robinson fuelpins as an example of running FET Tallies within the Shift/SCALE environment.
+ [homogenous_fuelpin](./homoegenous_fuelpin): Contains models of a simple homogenous PWR fuelpins and a Godiva as an example of running FET Tallies within the Shift/SCALE environment.
+ [src](./src): Contains the all FET-related code implementation.
+ [images](./images): Contains images for all documentation contained in the repository.


## Code Modifications

Major modifications to the source code and this library are contained within a [ChangeLog](ChangeLog.md) file. Details on major API changes, layout, and structure of the library are enumerated here.


## Contribute

When contributing to this repository, please first discuss the change you wish to make via issue, email, or any other method with the owners of this repository before making a change.

Contributions to this FE and FET implementation may be submitted via merge request to this repository. Please assign the members of the ExaSMR team to the merge request as appropriate.

Prior to creating a merge request, please ensure the following:

+ Ensure all added features are properly documented within the code according to the Doxygen-compliant commenting practices utilized. Ensure consistent style with the [library documentation practices](src/ReadMe.md) used here.
+ Please ensure any install or build dependencies are removed to permit the library to exist with few external dependencies.
+ Update this [ReadMe](ReadMe.md) and [ChangeLog](ChangeLog.md) with details of changes to the interface, including new environment variables, exposed ports, useful file locations, container parameters, and so forth.
+ Merge requests may be merged in once the modifications have been reviewed and approved by all appropriate reviewers, including at least one member of the ExaSMR team. Reviewers may merge the code once all appropriate reviewers have approved the modifications.


## Questions?

Please direct all questions to the active ExaSMR team, a sub-set of the Computational Engineering And Data Science (CEADS) group at the Idaho State University (ISU), hosted by [Dr. Leslie Kerby](mailto:kerblesl@isu.edu). As of the most recent modification of this file, this team includes, but may not be limited to, the following:

Active developers:
+ [Dr. Leslie Kerby](mailto:kerblesl@isu.edu)
+ [Aaron Johnson](mailto:johnaaro@isu.edu)
+ [Chase Juneau](mailto:junechas@isu.edu)
+ [Emily Elzinga](mailto:elziemil@isu.edu)
+ [Kallie McLaren](mailto:mclakall@isu.edu)
+ [Patience Lamb](mailto:lambpati@isu.edu)

Previous developers:
+ [Katherine Wilsdon](mailto:wilskat7@isu.edu)
+ [Steven Wacker](mailto:wackstev@isu.edu)
+ [Ryan Stewart](mailto:stewryan@isu.edu)


## Acknowledgments

This work was supported by the ExaScale Computing Project, under contract 17-SC-2-SC.

<img src="images/logos/ecp-logo.png" width=150>

<img src="images/logos/ISULogo.png" width=125>
<img src="images/logos/ornlLogo-Green.png" width=100>
