//---------------------------------*-C++-*-----------------------------------//
/*!
 * \file   src/Polynomials/Chebyshev_Poly.i.hh
 * \brief  Chebyshev_Poly class definitions.
 */
//---------------------------------------------------------------------------//
#ifndef src_Polynomials_Chebyshev_Poly_i_hh
#define src_Polynomials_Chebyshev_Poly_i_hh

#include "Chebyshev_Poly.hh"

#include <vector>
#include <memory>

#include "Comparisons.hh"
#include "FET_DBC.hh"

namespace FET
{

//---------------------------------------------------------------------------//
/*!
 * \brief Evaluates the polynomial for all orders specified for a domain point
 *
 * Evaluates the polynomial to the requested order at a given simulation domain
 * value.
 *
 * The Chebyshev polynomials are calculated using the recurrence relation, shown
 * below:
 * \f[
 * T_{n}(x) = 2 x T_{n-1}(x) - T_{n-2}(x)
 * \f]
 *
 * With \f$ T_{0} = 1 \f$ and \f$ T_{1} = x \f$. A direct calculation of
 * the Chebyshev polynomials is done for orders \f$ T_{n=6} \f$ and lower
 * for efficiency, with the above recursive relationship used otherwise.
 */
template<typename T>
typename Chebyshev_Poly<T>::set_type
Chebyshev_Poly<T>::evaluate(
        const Chebyshev_Poly<T>::order_type order,
        const Chebyshev_Poly<T>::data_type      x) const
{
    auto poly_vals = Chebyshev_Poly<T>::Base::evaluate(order, x);

    // Obtain preliminary domain values
    const Chebyshev_Poly<T>::data_type x2 = x * x;

    // Set non-recusive polynomials
    switch (order)
    {
        default:
        case 10:
            poly_vals[10] = ((((512.0 * x2 - 1280.0) * x2 + 1120.0)
                        * x2 - 400.0) * x2 + 50.0) * x2 - 1.0;
        case  9:
            poly_vals[ 9] = ((((256.0 * x2 - 576.0) * x2 + 432.0)
                    * x2 - 120.0) * x2 + 9.0) * x;
        case  8:
            poly_vals[ 8] = (((128.0 * x2 - 256.0 ) * x2 + 160.0)
                    * x2 - 32.0) * x2 + 1.0;
        case  7:
            poly_vals[ 7] = (((64.0 * x2 - 112.0) * x2 + 56.0)
                    * x2 - 7.0) * x;
        case  6:
            poly_vals[ 6] = ((32.0 * x2 - 48.0) * x2 + 18.0) * x2 - 1.0;
        case  5:
            poly_vals[ 5] = ((16.0 * x2 - 20.0) * x2 + 5.0) * x;
        case  4:
            poly_vals[ 4] = (8.0 * x2 - 8.0) * x2 + 1.0;
        case  3:
            poly_vals[ 3] = (4.0 * x2 - 3.0) * x;
        case  2:
            poly_vals[ 2] = 2.0 * x2 - 1.0;
        case  1:
            poly_vals[ 1] = x;
        case  0:
            poly_vals[ 0] = 1.0;
    }

    // Recursively determine higher order terms
    for (Chebyshev_Poly<T>::order_type n = 11; n <= order; ++n)
    {
        poly_vals[n] = 2.0 * x * poly_vals[n - 1] - poly_vals[n - 2];
    }

    // Normalize, is requested
    if (Chebyshev_Poly<T>::attr()->normalize())
    {
        normalize(poly_vals);
    }

    // Return the polynomial set
    return poly_vals;
}


//---------------------------------------------------------------------------//
/*!
 * \brief Normalizes the provided data on the simulation domain
 *
 * The normalization coefficient, on the orthogonal domain, for a given order
 * is calculated as follows:
 * \f[
 * c_{\psi_{n}} = (\int \psi_{n}(z)^{2} \rho(z) dz)^{-1} = \frac{k}{\pi}
 * \f]
 *
 * Where \f$ k = 1 \f$ for \f$ n = 0 \f$ and \f$ k = 2 \f$ for
 * \f$ n \neq 0 \f$.
 * \todo Implement normalize
 */
 //---------------------------------------------------------------------------//
template<typename T>
void Chebyshev_Poly<T>::normalize(
        Chebyshev_Poly<T>::set_type& poly_vals) const noexcept
{
    Chebyshev_Poly<T>::Base::normalize(poly_vals);
    if (poly_vals.size() <= 0) return;

    Not_Implemented("normalize");
}


//---------------------------------------------------------------------------//
/*!
 * \brief Normalizes the provided vector by order according to its index
 *
 * The Chebyshev polynomial order's normalization constant is determined as the
 * inverse of the l-2 norm (i.e. integration of the square of the basis
 * function of that order over the orthonormal domain). Normalization is done
 * on the given vector by multiplying it by the inverse of the l-2 norm.
 *
 * The normalization coefficient for a given order is calculated as follows:
 * \f[
 * c_{\psi_{n}} = (\int \psi_{n}(z)^{2} \rho(z) dz)^{-1} = \frac{k}{\pi}
 * \f]
 *
 * Where \f$ k = 1 \f$ for \f$ n = 0 \f$ and \f$ k = 2 \f$ for
 * \f$ n \neq 0 \f$.
 */
 //---------------------------------------------------------------------------//
template<typename T>
void Chebyshev_Poly<T>::ortho_normalize(
        Chebyshev_Poly<T>::set_type& poly_vals) const noexcept
{
    Chebyshev_Poly<T>::Base::normalize(poly_vals);
    if (poly_vals.size() == 0) return;

    const Chebyshev_Poly<T>::data_type coeff = 2.0 * pi;
    poly_vals[0] /= pi;
    for (Chebyshev_Poly<T>::order_type i = 1; i < poly_vals.size(); ++i)
    {
        poly_vals[i] *= coeff;
    }
}


//---------------------------------------------------------------------------//
/*!
 * \brief Integrates the basis set on a given standardized domain
 *
 * Integrates the Chebyshev basis set on a given standardized domain. The
 * returned vector is notated as integrated_value[order], i.e. the element
 * index in the vector is representative of the integrated value at that order.
 *
 * The Chebyshev polynomial integration is implemented using the following
 * formula for \f$ n \geq 2 \f$:
 * \f[
 * \int_{a}^{b} T_{n}(z) \rho(z) dz =
 *     \frac{1}{n - 1} (
 *     \frac{n}{n + 1} (T_{n + 1}(b) - T_{n+1}(a))
 *     - (b T_{n}(b) - a T_{n}(a))
 *     )
 * \f]
 *
 * The integration for order \f$ n = 0 \f$ is calculated as \f$ b - a \f$, and
 * that for order \f$ n = 1 \f$ is calculated as \f$ (b^{2} - a^{2}) / 2 \f$.
 */
 //---------------------------------------------------------------------------//
template<typename T>
typename Chebyshev_Poly<T>::set_type
Chebyshev_Poly<T>::integrate(
        const Chebyshev_Poly<T>::data_type lower_b,
        const Chebyshev_Poly<T>::data_type upper_b,
        const Chebyshev_Poly<T>::order_type order) const
{
    // Create the vector
    auto result = Chebyshev_Poly<T>::Base::integrate(lower_b, upper_b, order);
    if (is_approximate(upper_b, lower_b)) return result;

    // Obtain polynomial values at the specified bounds
    const auto lower_vals = evaluate(order + 1, lower_b);
    const auto upper_vals = evaluate(order + 1, upper_b);

    // Set integrated results
    result[0] = upper_b - lower_b;
    result[1] = (pow(upper_b, 2.0) - pow(lower_b, 2)) / 2.0;
    for (Chebyshev_Poly<T>::order_type i = 2; i <= order; ++i)
    {
        Chebyshev_Poly<T>::data_type n =
            static_cast<Chebyshev_Poly<T>::data_type>(i);
        result[i] = (1.0 / (n - 1.0)) * (
                (order / (order + 1.0)) *
                    (upper_vals[i + 1] - lower_vals[i + 1])
                - (upper_b * upper_vals[i] - lower_b * lower_vals[i])
                );
    }

    // Return the vector
    return result;
}


//---------------------------------------------------------------------------//
} // namespace FET

//---------------------------------------------------------------------------//
#endif // src_Polynomials_Chebyshev_Poly_i_hh
//---------------------------------------------------------------------------//
// end of src/Polynomials/Chebyshev_Poly.i.hh
//---------------------------------------------------------------------------//
