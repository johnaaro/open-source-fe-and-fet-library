//---------------------------------*-C++-*-----------------------------------//
/*!
 * \file   src/Polynomials/Legendre_Poly.hh
 * \brief  Legendre_Poly inline definitions.
 */
//---------------------------------------------------------------------------//
#ifndef src_Polynomials_Legendre_Poly_hh
#define src_Polynomials_Legendre_Poly_hh

#include <memory>
#include <vector>

#include "Basis_Poly.hh"
#include "Legendre_Attr.hh"

#include "FET_DBC.hh"

namespace FET
{
//===========================================================================//
/*!
 * \class Legendre_Poly
 * \brief Legendre Polynomial implementation
 *
 * Class for the implementation of Legendre polynomials. Based on the
 * Basis_Poly class and utilizes the Legendre_Attr class for its associated
 * attributes.
 *
 * \todo Use \c cmath and the associated legendre() polynomials when compiled
 * with c++17 if more efficient
 */
/*!
 * \example src/Polynomials/test/tstLegendre_Poly.cc
 *
 * Test of the Legendre polynomial.
 */
//===========================================================================//
template<typename T = double>
class Legendre_Poly : public Basis_Poly<T>
{

  public:
    //@{
    //! Public type aliases
    using Base        = Basis_Poly<T>;
    using data_type   = typename Base::data_type;
    using order_type  = typename Base::order_type;
    using set_type    = typename Base::set_type;
    //@}


  public:
    // >>> CONSTRUCTOR/DESTRUCTORS

    //! \brief Constructor
    Legendre_Poly(const Legendre_Attr& attr)
        : Base(attr)
    {
        Insist(attr.type().compare("Legendre") == 0,
                "Legendre basis sets may only accept Legendre attributes ("
                << attr.type() << " received)");
    }

    //@{
    //! Copy control (default, copy, move, and destructor)
    Legendre_Poly() = default;
    Legendre_Poly(const Legendre_Poly&) = default;
    Legendre_Poly& operator=(const Legendre_Poly&) = default;
    Legendre_Poly(Legendre_Poly&&) = default;
    Legendre_Poly& operator=(Legendre_Poly&&) = default;
    virtual ~Legendre_Poly() = default;
    //@}


  public:
    // >>> GETTERS
    // N/A presently


  public:
    // >>> SETTERS
    // N/A presently


  public:
    // >>> INTROSPECTION

    // Evaluates the polynomials for the basis set at a domain value
    set_type evaluate(const order_type order, const data_type domain_val)
        const override;

    // Returns a normalized set of evaluated Legendre polynomials
    void normalize(set_type& poly_values) const noexcept override;

    // Normalizes the polynomials on the orthogonal domain
    void ortho_normalize(set_type& poly_val) const noexcept override;

    // Returns a vector of integrated values on a given std. domain
    set_type integrate(const data_type lower_b, const data_type upper_b,
            const order_type order)
        const override;

    double orthonorm_const(const int order) const noexcept override;

    //! \brief Uses default order to evaluate the polynomial
    set_type evaluate(const data_type domain_val) const override
    { return evaluate(Base::order(), domain_val); }

    //! \brief Uses the default order to evaluate the integration
    set_type integrate(const data_type lower_b, const data_type upper_b)
        const override
    { return integrate(lower_b, upper_b, Base::order()); }

}; // end class Legendre_Poly

//---------------------------------------------------------------------------//
} // end namespace FET

//---------------------------------------------------------------------------//
// INLINE AND TEMPLATE DEFINITIONS
//---------------------------------------------------------------------------//
#include "Legendre_Poly.i.hh"

//---------------------------------------------------------------------------//
#endif // src_Polynomials_Legendre_Poly_hh
//---------------------------------------------------------------------------//
// end of src/Polynomials/Legendre_Poly.hh
//---------------------------------------------------------------------------//
