//---------------------------------*-C++-*-----------------------------------//
/*!
 * \file   src/gPolynomials/Hermite_Poly.i.hh
 * \brief  Hermite_Poly inline definitions.
 */
//---------------------------------------------------------------------------//
#ifndef src_Polynomials_Hermite_Poly_i_hh
#define src_Polynomials_Hermite_Poly_i_hh

#include "Hermite_Poly.hh"

#include <cmath>

#include "Comparisons.hh"
#include "Constants.hh"
#include "FET_DBC.hh"

namespace FET
{

//---------------------------------------------------------------------------//
/*!
 * \brief Evaluates the polynomial for all orders specified for a domain point
 *
 * Evaluates the polynomial to the requested order at a given simulation domain
 * value.
 *
 * The Hermite polynomials are calculated using the recurrence relation, shown
 * below:
 * \f[
 * H_{n + 1}(x) = 2 x H_{n}(x) - 2 n H_{n - 1}(x)
 * \f]
 *
 * With \f$ H_{0} = 1 \f$ and \f$ H_{1} = 2 x \f$. A direct calculation of
 * the Hermite polynomials is done for orders \f$ H_{n=10} \f$ and lower
 * for efficiency, with the above recursive relationship used otherwise.
 */
template<typename T>
typename Hermite_Poly<T>::set_type
Hermite_Poly<T>::evaluate(
        const Hermite_Poly<T>::order_type order,
        const Hermite_Poly<T>::data_type    val) const
{
    // Pre-process and obtain results
    auto poly_vals = Hermite_Poly<T>::Base::evaluate(order, val);

    // Obtain preliminary domain values
    const Hermite_Poly<T>::data_type x  = 2.0 * val;
    const Hermite_Poly<T>::data_type x2 = x * x;

    // Set non-recusive polynomials
    switch (order)
    {
        default:
        case 10:
            poly_vals[10] = ((((x2 - 90.) * x2 + 2520.) * x2 - 25200.)
                    * x2 + 75600.) * x2 - 30240.;
        case  9:
            poly_vals[ 9] = ((((x2 - 72.) * x2 + 1512.) * x2 - 10080.)
                    * x2 + 15120.) * x;
        case  8:
            poly_vals[ 8] = (((x2 - 56.) * x2 + 840.) * x2 - 6720.) * x2
                + 1680.;
        case  7:
            poly_vals[ 7] = (((x2 - 42.) * x2 + 420.) * x2 - 840.) * x;
        case  6:
            poly_vals[ 6] = ((x2 - 30.) * x2 + 180.) * x2 - 120.;
        case  5:
            poly_vals[ 5] = ((x2 - 20.) * x2 + 60.) * x;
        case  4:
            poly_vals[ 4] = (x2 - 12.) * x2 + 12.;
        case  3:
            poly_vals[ 3] = (x2 - 6.) * x;
        case  2:
            poly_vals[ 2] = x2 - 2.;
        case  1:
            poly_vals[ 1] = x;
        case  0:
            poly_vals[ 0] = 1.;
    }

    // Recursively determine higher order terms
    for (Hermite_Poly<T>::order_type n = 11; n <= order; ++n)
    {
        poly_vals[n] = x * poly_vals[n - 1] -
            2.0 * static_cast<Hermite_Poly<T>::data_type>(n - 1) *
            poly_vals[n - 2];
    }

    // Normalize, is requested
    if (Hermite_Poly<T>::attr()->normalize())
    {
        normalize(poly_vals);
    }

    // Return the polynomial set
    return poly_vals;
}


//---------------------------------------------------------------------------//
/*!
 * \brief Normalizes the provided vector by order on simulation domain
 *
 * The Hermite polynomial order's normalization constant is determined as the
 * inverse of the l-2 norm (i.e. integration of the square of the basis
 * function of that order over the orthonormal domain). Normalization is done
 * on the given vector by multiplying it by the inverse of the l-2 norm.
 *
 * The normalization coefficient, on the simulation domain, for a given order
 * is calculated as follows:
 * \f[
 * c_{\psi_{n}} = (\int H_{n}(z)^{2} dz)^{-1} =
 *    (2^{n} n! \sqrt{\pi})^{-1}
 * \f]
 *
 * \todo Implement \c normalize
 */
 //---------------------------------------------------------------------------//
template<typename T>
void Hermite_Poly<T>::normalize(
        Hermite_Poly<T>::set_type& poly_vals) const noexcept
{
    // Pre-process
    Hermite_Poly<T>::Base::normalize(poly_vals);
    if (poly_vals.size() == 0) return;

    // Perform normalization
    Not_Implemented("normalize");
}


//---------------------------------------------------------------------------//
/*!
 * \brief Normalizes the provided vector by order according to its index
 *
 * The Hermite polynomial order's normalization constant is determined as the
 * inverse of the l-2 norm (i.e. integration of the square of the basis
 * function of that order over the orthonormal domain). Normalization is done
 * on the given vector by multiplying it by the inverse of the l-2 norm.
 *
 * The normalization coefficient for a given order is calculated as follows:
 * \f[
 * c_{\psi_{n}} = (\int H_{n}(z)^{2} dz)^{-1} =
 *    (2^{n} n! \sqrt{\pi})^{-1}
 * \f]
 */
 //---------------------------------------------------------------------------//
template<typename T>
void Hermite_Poly<T>::ortho_normalize(
        Hermite_Poly<T>::set_type& poly_vals) const noexcept
{
    // Pre-process
    Hermite_Poly<T>::Base::normalize(poly_vals);
    if (poly_vals.size() == 0) return;

    // Perform normalization
    Hermite_Poly<T>::data_type coeff = 1.0 / std::sqrt(pi);
    poly_vals[0] *= coeff;
    coeff /= 2.0;
    if (poly_vals.size() > 1)
        { poly_vals[1] *= coeff; }

    // Modify coefficient and normalize
    for (Hermite_Poly<T>::order_type i = 2; i < poly_vals.size(); ++i)
    {
        coeff /= 2.0 * static_cast<Hermite_Poly<T>::data_type>(i);
        poly_vals[i] *= coeff;
    }
}


//---------------------------------------------------------------------------//
/*!
 * \brief Integrates the basis set on a given standardized domain
 *
 * Integrates the Hermite basis set on a given standardized domain. The
 * returned vector is notated as integrated_value[order], i.e. the element
 * index in the vector is representative of the integrated value at that order.
 *
 * The Hermite polynomial integration is implemented using the following
 * formula:
 * \f[
 * \int H_{n}(z) \rho(z) dz =
 *   2^{n} \sqrt{\pi} (
 *   \frac{
 *      z _{p}F_{q}(\frac{n + 1}{2}, \frac{3}{2}, -z^{2})
 *   }{
 *      \Gamma(\frac{1 - n}{2})
 *   } +
 *   \frac{
 *      _{p}F_{q}(\frac{n}{2}, \frac{1}{2}, -z^{2}) - 1
 *   }{
 *      n \Gamma(\frac{n}{2})
 *   }
 *   )  | \limits_{a}^{b}
 * \f]
 *
 * For integration of \f$ H_{0}(z) \f$, the formula \f$ \frac{\sqrt{\pi}}{2}
 * (\Gamma(b) - \Gamma(a)) \f$ is used.
 */
 //---------------------------------------------------------------------------//
template<typename T>
typename Hermite_Poly<T>::set_type
Hermite_Poly<T>::integrate(
        const Hermite_Poly<T>::data_type lower_b,
        const Hermite_Poly<T>::data_type upper_b,
        const Hermite_Poly<T>::order_type  order) const
{
    // Pre-process and get default
    auto result = Hermite_Poly<T>::Base::integrate(lower_b, upper_b, order);
    if (is_approximate(upper_b, lower_b)) return result;

    // Obtain relative constants/coefficients
    const Hermite_Poly<T>::data_type lower2 = pow(lower_b, 2);
    const Hermite_Poly<T>::data_type upper2 = pow(upper_b, 2);
    Hermite_Poly<T>::data_type coeff = std::sqrt(pi);

    // Fill integration result
    result[0] = (coeff / 2.) * (std::erf(upper_b) - std::erf(lower_b));
    for (Hermite_Poly<T>::order_type i = 1; i <= order; ++i)
    {
        coeff *= 2.0;
        Hermite_Poly<T>::data_type half_order =
            static_cast<Hermite_Poly<T>::data_type>(i) / 2.0;
        result[i] = coeff * (
                partial_integral(upper_b, upper2, half_order) -
                partial_integral(lower_b, lower2, half_order));
    }

    // Return the vector
    return result;
}


//---------------------------------------------------------------------------//
/*
 * \brief Performs a portion of the integral for Hermite polynomials
 *
 * Performs the following portion involved in integrating Hermite polynomials
 * with their weight function.
 * \f[
 *   \frac{
 *      z _{p}F_{q}(\frac{n + 1}{2}, \frac{3}{2}, -z^{2})
 *   }{
 *      \Gamma(\frac{1 - n}{2}
 *   } +
 *   \frac{
 *      _{p}F_{q}(\frac{n}{2}, \frac{1}{2}, -z^{2}) - 1
 *   }{
 *      n \Gamma(\frac{n}{2})
 *   }
 *   )
 * \f]
 *
 * See documentation on \c integrate for the full integration formula.
 */
 //---------------------------------------------------------------------------//
template<typename T>
typename Hermite_Poly<T>::data_type
Hermite_Poly<T>::partial_integral(
        const Hermite_Poly<T>::data_type z,
        const Hermite_Poly<T>::data_type z2,
        const Hermite_Poly<T>::data_type n_over_2) const noexcept
{
    return (
            z * pFq(n_over_2 + 0.5, 1.5, -z2) / std::erf(0.5 - n_over_2) +
            (pFq(n_over_2, 0.5, -z2) - 1) / (2.0 * n_over_2 *
                std::erf(n_over_2)));
}


//---------------------------------------------------------------------------//
/*!
 * \brief A simple generalized hypergeometric function for Hermite integration
 *
 * Provides a simple implementation of the generalized hypergeometric function
 * \f$ _{p}F_{q} \f$, where \f$ p = 1 \f$ and \f$ q = 1 \f$ as follows:
 * \f[
 * _{p=1}F_{q=1}(a, b, z) = 1 + \frac{a}{b} z
 * \f]
 *
 */
template<typename T>
typename Hermite_Poly<T>::data_type
Hermite_Poly<T>::pFq(
        const Hermite_Poly<T>::data_type a,
        const Hermite_Poly<T>::data_type b,
        const Hermite_Poly<T>::data_type z) const noexcept
{
    return (1.0 + a * z / b);
}


//---------------------------------------------------------------------------//
} // namespace FET

#endif // src_Polynomials_Hermite_Poly_i_hh
//---------------------------------------------------------------------------//
// end of src/Polynomials/Hermite_Poly.i.hh
//---------------------------------------------------------------------------//
