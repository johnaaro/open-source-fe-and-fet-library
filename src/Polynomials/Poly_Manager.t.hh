//---------------------------------------------------------------------------//
/*!
 * \file  src/Polynomials/Poly_Manager.t.hh
 * \brief Basis FET class declaration.
 */
//---------------------------------------------------------------------------//
#ifndef src_Polynomials_Poly_Manager_t_hh
#define src_Polynomials_Poly_Manager_t_hh

#include <algorithm>
#include <functional>
#include <iostream>
#include <utility>

#include "FET_DBC.hh"
#include "Logger.hh"
#include "Poly_Manager.hh"

namespace FET
{

//---------------------------------------------------------------------------//
/*!
 * \brief Indicates if a polynomial has a valid constrained domain label
 *
 * Returns a boolean flag indicating if a polynomial has a domain that lies
 * within the constrained list of valid domain labels
 */
template<class T>
bool Poly_Manager<T>::is_valid(const Poly_Manager<T>::key_type& key)
    const noexcept
{
    for (auto& it : valid_domains)
    { if (key == it) return true; }
    return false;
}


//---------------------------------------------------------------------------//
/*!
 * \brief Inserts a single polynomial into the manager
 *
 * \note When \c validate() is not used, insertion may not work.
 */
template<class T>
void Poly_Manager<T>::insert(Poly_Manager<T>::SP_Base item)
{
    // Validate if requested
    const auto key = item->attr()->domain().label();

    if (validate())
    {
        Insist (is_valid(key),
                "Polynomials on the (" << key << ") domain are not supported."
                );
        Insist (!contains(key),
                "A polynomial on the (" << key
                    << ") domain already exists in the set."
                );
    }

    // Modify the key associated with the index if the key already exists
    if (!contains(key))
    { d_labels.push_back(key); }

    // Insert (key, item) into the container (replace item if key exists)
    auto success = d_base.insert(std::make_pair(key, item));
    if (!success.second)
    {
        log(WARNING) << "The " << item->type() << " polynomial "
                  << "could not be inserted into the manager.\n";
    }
}


//---------------------------------------------------------------------------//
/*!
 * \brief Inserts a vector of polynomials into the manager via shared pointers
 */
template<class T>
void Poly_Manager<T>::insert(
        const std::vector<Poly_Manager<T>::SP_Base>& items)
{
    for (auto& it : items)
    { insert(it); }
}


//---------------------------------------------------------------------------//
} // end namespace FET

#endif // src_Polynomials_Poly_Manager_t_hh
//---------------------------------------------------------------------------//
// end of src/Polynomials/Poly_Manager_t_hh
//---------------------------------------------------------------------------//
