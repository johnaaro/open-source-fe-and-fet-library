##---------------------------------------------------------------------------##
##
## src/Polynomials/CMakeLists.txt
##
##---------------------------------------------------------------------------##
## CMAKE for the FET polynomials and their sub-libraries
##---------------------------------------------------------------------------##
cmake_minimum_required (VERSION 3.12.4)
PreventInSourceBuild()

##---------------------------------------------------------------------------##
## SUB-LIBRARIES
##---------------------------------------------------------------------------##
add_subdirectories(
  Attr
  )


##---------------------------------------------------------------------------##
## LIBRARY
##---------------------------------------------------------------------------##
library("Polynomials")

set(
  SOURCES
  Basis_Poly.cc
  # 1-D polynomials
  Chebyshev_Poly.cc
  Cosine_Poly.cc
  Hermite_Poly.cc
  Legendre_Poly.cc
  Sine_Poly.cc
  # 2-D polynomials
  # Zernike_Poly.cc
  # Spherical_Harmonics.cc
  # 3-D polynomials
  # Volumetric_Spherical_Polynomials.cc
  Poly_Manager.cc
  )

set(
  DEPENDENCIES
  harness
  Poly_Attrs
  )

# NOTE: This works for GNU/Clang, but probably not Intel.
#       For now, we'll leave this in the global project flags for GNU/Clang
set(
  FLAGS
  # "-Wno-implicit-fallthrough"   # Disable implicit fall-through warnings
  )


add_library_wrapper(
  SOURCES ${SOURCES}
  DEPENDENCIES ${DEPENDENCIES}
  FLAGS ${FLAGS}
  )


##---------------------------------------------------------------------------##
##                   end of src/Polynomials/CMakeLists.txt
##---------------------------------------------------------------------------##
