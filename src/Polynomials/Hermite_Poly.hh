//---------------------------------*-C++-*-----------------------------------//
/*!
 * \file   src/Polynomials/Hermite_Poly.hh
 * \brief  Hermite_Poly class declaration.
 */
//---------------------------------------------------------------------------//
#ifndef src_Polynomials_Hermite_Poly_hh
#define src_Polynomials_Hermite_Poly_hh

#include "FET_Macros.hh"
#include "FET_DBC.hh"
#include "Basis_Poly.hh"
#include "Hermite_Attr.hh"

namespace FET
{
//===========================================================================//
/*!
 * \class Hermite_Poly
 * \brief Hermite Polynomial implementation
 *
 * Class for the implementation of Hermite polynomials. Based on the
 * Basis_Poly class and utilizes the Hermite_Attr class for its associated
 * attributes.
 *
 * \todo Use \c cmath and the associated hermite() polynomials when compiled
 * with c++17
 */
/*!
 * \example src/Polynomials/test/tstHermite_Poly.cc
 *
 * Test of the Hermite polynomial.
 */
//===========================================================================//
template<typename T = double>
class Hermite_Poly : public Basis_Poly<T>
{

  public:
    //@{
    //! Public type aliases
    using Base        = Basis_Poly<T>;
    using data_type   = typename Base::data_type;
    using order_type  = typename Base::order_type;
    using set_type    = typename Base::set_type;
    //@}

  public:
    // >>> CONSTRUCTOR/DESTRUCTORS

    //! \brief Constructor
    Hermite_Poly(const Hermite_Attr& attr)
        : Base(attr)
    {
        Insist(attr.type().compare("Hermite") == 0,
                "Hermite basis sets may only accept Hermite attributes ("
                << attr.type() << " received)");
    }

    //@{
    //! Copy control (default, copy, move, and destructor)
    Hermite_Poly() = default;
    Hermite_Poly(const Hermite_Poly&) = default;
    Hermite_Poly& operator=(const Hermite_Poly&) = default;
    Hermite_Poly(Hermite_Poly&&) = default;
    Hermite_Poly& operator=(Hermite_Poly&&) = default;
    virtual ~Hermite_Poly() = default;
    //@}


  public:
    // >>> GETTERS
    // N/A presently


  public:
    // >>> SETTERS
    // N/A presently


  public:
    // >>> INTROSPECTION

    /*
     * Note the below should ONLY be used if the limits are infinity!
    //! \brief Performs special standardization for Hermite polynomials
    data_type standardize(const data_type domain_value) const noexcept override
    { return domain_value; }
    */

    // Evaluates the polynomials for the basis set at a domain value
    set_type evaluate(const order_type order, const data_type domain_val)
        const override;

    // Returns a normalized set of evaluated Hermite polynomials
    void normalize(set_type& poly_values) const noexcept override;

    // Returns a ortho-normalized set of data
    void ortho_normalize(set_type& poly_values) const noexcept override;

    // Returns the weighting function for Hermite polynomials
    data_type weight_fct(const data_type val) const noexcept override
    { return exp(-val * val); }

    // Returns a vector of integrated values on a given std. domain
    set_type integrate(const data_type lower_b, const data_type upper_b,
            const order_type order)
        const override;

    //! \brief Uses default order to evaluate the polynomial
    set_type evaluate(const data_type domain_val) const override
    { return evaluate(Base::order(), domain_val); }

    //! \brief Uses the default order to evaluate the integration
    set_type integrate(const data_type lower_b, const data_type upper_b)
        const override
    { return integrate(lower_b, upper_b, Base::order()); }

  private:

    // Helper function for the \c integrate function
    data_type partial_integral(const data_type, const data_type,
            const data_type) const noexcept;

    // Simple generalized hypergeometric function for \f$ p = q = 1\f$
    data_type pFq(const data_type, const data_type, const data_type)
        const noexcept;


}; // end class Hermite_Poly

//---------------------------------------------------------------------------//
} // end namespace FET

//---------------------------------------------------------------------------//
// INLINE AND TEMPLATE DEFINITIONS
//---------------------------------------------------------------------------//
#include "Hermite_Poly.i.hh"

//---------------------------------------------------------------------------//
#endif // src_Polynomials_Hermite_Poly_hh
//---------------------------------------------------------------------------//
// end of src/Polynomials/Hermite_Poly.hh
//---------------------------------------------------------------------------//
