//---------------------------------------------------------------------------//
/*!
 * \file  src/Polynomials/Poly_Manager.i.hh
 * \brief Poly_Manager class declaration.
 */
//---------------------------------------------------------------------------//
#ifndef src_Polynomials_Poly_Manager_i_hh
#define src_Polynomials_Poly_Manager_i_hh

#include "FET_DBC.hh"
#include "Poly_Manager.hh"

namespace FET
{

//---------------------------------------------------------------------------//
/*!
 * \brief Removes all polynomial objects stored
 */
template<class T>
void Poly_Manager<T>::reset() noexcept
{
    d_base.clear();
    d_labels.clear();
}


//---------------------------------------------------------------------------//
/*!
 * \brief Erases the element with a specified key if it exists
 */
template<class T>
void Poly_Manager<T>::erase(const Poly_Manager<T>::key_type& key)
{
    if (contains(key))
    {
        // Remove all instances from the indexed container
        unsigned int vec_removed = 0;
        for (auto it = d_labels.begin(); it != d_labels.end(); )
        {
            if (*it == key)
            {
                it = d_labels.erase(it);
                vec_removed++;
            }
            else { ++it; }
        }

        // Remove all instances from the polynomial container
        auto container_removed = d_base.erase(key);
        Require(container_removed == vec_removed);
    }
}


//---------------------------------------------------------------------------//
/*!
 * \brief Erases the element with a specified index if it exists
 */
template<class T>
void Poly_Manager<T>::erase(const Poly_Manager<T>::index_type index)
{
    if (contains(index))
    { erase(labels()[index]); }
}


//---------------------------------------------------------------------------//
/*!
 * \brief Returns the total volume of the polynomials
 *
 * Calculates the total volume occupied by the polynomial's simulation domain.
 * Note that this combines the polynomial volumes assuming they are independent
 * of one another (i.e. orthogonal) and that they do not overlap (i.e. 'x' and
 * 'r').
 */
template<class T>
double Poly_Manager<T>::volume() const noexcept
{
    if (empty()) return 0.00;

    double vol = 1.00;
    for (const auto &polynomial : polynomials())
    { vol *= polynomial.second->volume(); }
    return vol;
}


//---------------------------------------------------------------------------//
/*!
 * \brief Describes the various polynomial objects contained
 *
 * Iterates over the polynomial objects to describe them
 */
template<class T>
void Poly_Manager<T>::describe(std::ostream& os) const noexcept
{
    for (const auto& it : polynomials())
    { it.second->describe(os); }
}


//---------------------------------------------------------------------------//
} // end namespace FET

#endif // src_Polynomials_Poly_Manager_i_hh
//---------------------------------------------------------------------------//
// end of src/Polynomials/Poly_Manager.i.hh
//---------------------------------------------------------------------------//
