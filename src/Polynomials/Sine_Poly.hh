//---------------------------------*-C++-*-----------------------------------//
/*!
 * \file   src/Polynomials/Sine_Poly.hh
 * \brief  Sine_Poly class declaration.
 */
//---------------------------------------------------------------------------//
#ifndef src_Polynomials_Sine_Poly_hh
#define src_Polynomials_Sine_Poly_hh

#include <memory>
#include <vector>

#include "FET_Macros.hh"
#include "FET_DBC.hh"
#include "Basis_Poly.hh"
#include "Sine_Attr.hh"

namespace FET
{
//===========================================================================//
/*!
 * \class Sine_Poly
 * \brief Sine Polynomial implementation
 *
 * Class for the implementation of Sine polynomials. Based on the
 * Basis_Poly class and utilizes the Sine_Attr class for its associated
 * attributes.
 *
 */
/*!
 * \example src/Polynomials/test/tstSine_Poly.cc
 *
 * Test of the Sine polynomial.
 */
//===========================================================================//
template<typename T = double>
class Sine_Poly : public Basis_Poly<T>
{

  public:
    //@{
    //! Public type aliases
    using Base        = Basis_Poly<T>;
    using data_type   = typename Base::data_type;
    using order_type  = typename Base::order_type;
    using set_type    = typename Base::set_type;
    //@}

  public:
    // >>> CONSTRUCTOR/DESTRUCTORS

    //! \brief Constructor
    Sine_Poly(const Sine_Attr& attr)
        : Base(attr)
    {
        Insist(attr.type().compare("Sine") == 0,
                "Sine basis sets may only accept Sine attributes ("
                << attr.type() << " received)");
    }

    //@{
    //! Copy control (default, copy, move, and destructor)
    Sine_Poly() = default;
    Sine_Poly(const Sine_Poly&) = default;
    Sine_Poly& operator=(const Sine_Poly&) = default;
    Sine_Poly(Sine_Poly&&) = default;
    Sine_Poly& operator=(Sine_Poly&&) = default;
    virtual ~Sine_Poly() = default;
    //@}


  public:
    // >>> GETTERS
    // N/A presently


  public:
    // >>> SETTERS
    // N/A presently


  public:
    // >>> INTROSPECTION

    // Evaluates the polynomials for the basis set at a domain value
    set_type evaluate(const order_type order, const data_type domain_val)
        const override;

    // Returns a normalized set of evaluated Sine polynomials
    void normalize(set_type& poly_values) const noexcept override;

    // Normalizes the polynomials on the orthogonal domain
    void ortho_normalize(set_type& poly_val) const noexcept override;

    // Returns a vector of integrated values on a given std. domain
    set_type integrate(const data_type lower_b, const data_type upper_b,
            const order_type order)
        const override;

    //! \brief Uses default order to evaluate the polynomial
    set_type evaluate(const data_type domain_val) const override
    { return evaluate(Base::order(), domain_val); }

    //! \brief Uses the default order to evaluate the integration
    set_type integrate(const data_type lower_b, const data_type upper_b)
        const override
    { return integrate(lower_b, upper_b, Base::order()); }

}; // end class Sine_Poly

//---------------------------------------------------------------------------//
} // end namespace FET

//---------------------------------------------------------------------------//
// INLINE AND TEMPLATE DEFINITIONS
//---------------------------------------------------------------------------//
#include "Sine_Poly.i.hh"

//---------------------------------------------------------------------------//
#endif // src_Polynomials_Sine_Poly_hh
//---------------------------------------------------------------------------//
// end of src/Polynomials/Sine_Poly.hh
//---------------------------------------------------------------------------//
