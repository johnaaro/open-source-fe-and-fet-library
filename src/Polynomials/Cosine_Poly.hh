//---------------------------------*-C++-*-----------------------------------//
/*!
 * \file   src/Polynomials/Cosine_Poly.hh
 * \brief  Cosine_Poly class declaration.
 */
//---------------------------------------------------------------------------//
#ifndef src_Polynomials_Cosine_Poly_hh
#define src_Polynomials_Cosine_Poly_hh

#include <memory>
#include <vector>

#include "FET_Macros.hh"
#include "FET_DBC.hh"
#include "Basis_Poly.hh"
#include "Cosine_Attr.hh"

namespace FET
{
//===========================================================================//
/*!
 * \class Cosine_Poly
 * \brief Cosine Polynomial implementation
 *
 * Class for the implementation of Cosine polynomials. Based on the
 * Basis_Poly class and utilizes the Cosine_Attr class for its associated
 * attributes.
 *
 */
/*!
 * \example src/Polynomials/test/tstCosine_Poly.cc
 *
 * Test of the Cosine polynomial.
 */
//===========================================================================//
template<typename T = double>
class Cosine_Poly : public Basis_Poly<T>
{

  public:
    //@{
    //! Public type aliases
    using Base        = Basis_Poly<T>;
    using data_type   = typename Base::data_type;
    using order_type  = typename Base::order_type;
    using set_type    = typename Base::set_type;
    //@}

  public:
    // >>> CONSTRUCTOR/DESTRUCTORS

    //! \brief Constructor
    Cosine_Poly(const Cosine_Attr& attr)
        : Base(attr)
    {
        Insist(attr.type().compare("Cosine") == 0,
                "Cosine basis sets may only accept Cosine attributes ("
                << attr.type() << " received)");
    }

    //@{
    //! Copy control (default, copy, move, and destructor)
    Cosine_Poly() = default;
    Cosine_Poly(const Cosine_Poly&) = default;
    Cosine_Poly& operator=(const Cosine_Poly&) = default;
    Cosine_Poly(Cosine_Poly&&) = default;
    Cosine_Poly& operator=(Cosine_Poly&&) = default;
    virtual ~Cosine_Poly() = default;
    //@}


  public:
    // >>> GETTERS
    // N/A presently


  public:
    // >>> SETTERS
    // N/A presently


  public:
    // >>> INTROSPECTION

    // Evaluates the polynomials for the basis set at a domain value
    set_type evaluate(const order_type order, const data_type x)
        const override;

    // Returns a normalized set of evaluated Cosine polynomials
    void normalize(set_type& poly_values) const noexcept override;

    // Normalizes the polynomials on the orthogonal domain
    void ortho_normalize(set_type& poly_val) const noexcept override;

    // Returns a vector of integrated values on a given std. domain
    set_type integrate(const data_type lower_b, const data_type upper_b,
            const order_type order)
        const override;

    //! \brief Uses default order to evaluate the polynomial
    set_type evaluate(const data_type domain_val) const override
    { return evaluate(Base::order(), domain_val); }

    //! \brief Uses the default order to evaluate the integration
    set_type integrate(const data_type lower_b, const data_type upper_b)
        const override
    { return integrate(lower_b, upper_b, Base::order()); }

}; // end class Cosine_Poly

//---------------------------------------------------------------------------//
} // end namespace FET

//---------------------------------------------------------------------------//
// INLINE AND TEMPLATE DEFINITIONS
//---------------------------------------------------------------------------//
#include "Cosine_Poly.i.hh"

//---------------------------------------------------------------------------//
#endif // src_Polynomials_Cosine_Poly_hh
//---------------------------------------------------------------------------//
// end of src/Polynomials/Cosine_Poly.hh
//---------------------------------------------------------------------------//
