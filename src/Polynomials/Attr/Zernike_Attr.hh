//---------------------------------*-C++-*-----------------------------------//
/*!
 * \file   src/Polynomials/Attr/Zernike_Attr.hh
 * \brief  Zernike_Attr class declaration.
 */
//---------------------------------------------------------------------------//
#ifndef src_Polynomials_Attr_Zernike_Attr_hh
#define src_Polynomials_Attr_Zernike_Attr_hh

#include "Basis_Attr.hh"

namespace FET
{
//===========================================================================//
/*!

 */
//===========================================================================//
    class Zernike_Attr : public Basis_Attr
    {
        using Base = Basis_Attr;
    private:
        //>>> IMPLEMENTATION DATA
//        Base::domain_type domain = domain_type("Unspecified", -1, 1);
//        Base::domain_type angle = domain_type("Unspecified", 0, 2 * pi); // NEED IT TO BE A DOUBLE FOR PI

        //! \brief The basis set's orthogonal domain
        const Base::domain_type b_ortho_domain =
                Base::domain_type("Normalized", -1.0, 1.0);
    public:
        Zernike_Attr(
              const std::string& name,
              const Base::order_type order,
//            const Base::order_type radial_order,
//            const Base::order_type azimuthal_order,
//            const Base::order_type zenith_order,
//            const std::vector<Base::order_type>{radial_order, azimuthal_order, zenith_order},
//            const std::vector<Base::order_type> order,
//            const std::vector<Base::domain_type> dom)
              const Base::domain_type& dom
//            const Base::domain_type& radial_dom,
//            const Base::domain_type& azimuthal_dom,
//            const Base::domain_type& zenith_dom)
            )
//        : Base(name, order, dom)
            : Base(name, order, dom)
            {/* * */}


        Zernike_Attr() = default;
        Zernike_Attr(const Zernike_Attr&) = default;
        Zernike_Attr& operator=(const Zernike_Attr&) = default;
        Zernike_Attr(Zernike_Attr&&) = default;
        Zernike_Attr& operator = (Zernike_Attr&&) = default;
        virtual ~Zernike_Attr() = default;

    public:
        // >>> INTROSPECTION/GETTERS
        //const Zernike_Attr::domain_type getRange() const {
           // return range;
        //}


//        const Zernike_Attr::domain_type getDomain() const{
//            return domain;
//        }
//        const Zernike_Attr::domain_type getAngle() const {
//            return angle;
//        }

        //! \brief Return the type description
        const std::string type() const noexcept override
        { return "Zernike"; }

        //! \brief Returns the standardized domain of the basis set
        const Base::domain_type& ortho_domain() const noexcept override
        { return b_ortho_domain; }


    };
// end class Zernike_Attr

//---------------------------------------------------------------------------//
} // end namespace FET

#endif // src_Polynomials_Attr_Zernike_Attr_hh
//---------------------------------------------------------------------------//
// end of src/Polynomials/Attr/Zernike_Attr.hh
//---------------------------------------------------------------------------//
