//---------------------------------*-C++-*-----------------------------------//
/*!
 * \file   src/Polynomials/Attr/Basis_Attr.hh
 * \brief  Basis_Attr class declaration.
 */
//---------------------------------------------------------------------------//
#ifndef src_Polynomials_Attr_Basis_Attr_hh
#define src_Polynomials_Attr_Basis_Attr_hh

#include <iostream>
#include <string>

#include "FET_DBC.hh"
#include "Domain.hh"
#include "Name.hh"
#include "Logger.hh"


namespace FET
{

//===========================================================================//
/*!
 * \class Basis_Attr
 * \brief Generic set of common basis set attributes
 *
 * The Basis_Attr object contains the specification of a basis set that is used
 * in a functional expansion object. This object acts as the "details" of the
 * basis set.
 *
 * \todo Use a template for the integral type of the domain. This may prevent
 * roundoff between datatypes when using polynomials of non-double evaluation
 */
/*!
 * \example test/tstBasis_Attr.cc
 *
 * Tests the Basis_Attr object
 *
 */
//===========================================================================//
class Basis_Attr : public Name
{
  public:
    // >>> PUBLIC TYPE ALIASES AND CONSTANTS

    //@{
    //! \brief Public type aliases
    using Base             = Name;
    using order_type       = unsigned int;
    using normalize_type   = bool;
    using domain_type      = Domain<double>;
    //@}

    /*! \brief The recommended amount to extrapolate the simulation range
     *
     * The recommended amount to scale the simulation domain by. That is, the
     * simulation domain ought to be extrapolated past that of what is used to
     * allow the basis set to behave normally within the domain of interest. It
     * is suggested to extrapolate the simulation domain bounds by scaling
     * according to your application.
    */
    static constexpr double domain_extrapolation = 1.15;

    //@{
    //! \brief Default integral values for class members
    static constexpr order_type     default_order        = 12;
    static constexpr order_type     oversample_order     = 30;
    static constexpr normalize_type default_normalize    = false;
    static constexpr double         default_domain       = 0.0;
    static constexpr double         default_ortho_domain = 1.0;
    //@}


  private:
    // >>> IMPLEMENTATION DATA

    //! \brief Order of the basis set
    order_type b_order = default_order;

    //! \brief The domain of the simulation
    domain_type b_sim_domain =
        domain_type("unspecified", -default_domain, default_domain);

    //! \brief Normalization type
    normalize_type b_normalize = default_normalize;

    //! \brief The basis set's standardized domain
    //!    Default: {-default_ortho_domain, ORTHO_default_domain}
    const domain_type b_ortho_domain = domain_type("Normalized",
                -default_ortho_domain, default_ortho_domain);


  public:
    // >>> CONSTRUCTION

    //! \brief Constructor with a name, order, and domain specified
    Basis_Attr(
            const std::string& name,
            const order_type order,
            const domain_type& domain
            )
        : Base(name)
        , b_order(order)
        , b_sim_domain(domain)
    { /* * */ }

    //@{
    //! Copy control (default, copy, move, and destructor)
    Basis_Attr() = default;
    Basis_Attr(const Basis_Attr&) = default;
    Basis_Attr& operator=(const Basis_Attr&) = default;
    Basis_Attr(Basis_Attr&&) = default;
    Basis_Attr& operator=(Basis_Attr&&) = default;
    virtual ~Basis_Attr() = default;
    //@}


  public:
    // >>> SETTERS

    //! \brief Sets the order of the expansion
    void set_order(const order_type order)
    {
        if (order >= oversample_order)
        {
            log(WARNING) << "Oversampling with a " << label()
                         << " polynomial of order " << order
                         << " may occur.";
        }

        b_order = order;
    }

    //! \brief Sets if the owning Basis_Poly will normalize its basis set
    void set_normalize(const normalize_type normalize) noexcept
    { b_normalize = normalize; }

    //! \brief Sets the simulation domain of the owning basis set
    void set_domain(const domain_type& domain) noexcept
    { b_sim_domain = domain; }


  public:
    // >>> GETTERS

    //! \brief Return the Basis set type (e.g. Legendre, cosine, Chebyshev)
    virtual const std::string type() const noexcept
    { return "Basis"; }

    //! \brief Returns the order
    order_type order() const noexcept
    { return b_order; }

    //! \brief Alias for \c order()
    order_type degree() const noexcept
    { return order(); }

    //! \brief Returns if the basis set is normalizing its polynomials
    normalize_type normalize() const noexcept
    { return b_normalize; }

    //! \brief Returns the domain the basis set applies to
    const domain_type& domain() const noexcept
    { return b_sim_domain; }

    //! \brief Returns the domain on which the basis set is normalized
    //! Note that this method does NOT NEED to be overridden for basis sets
    //! that use a orthogonal/normalized domain on [-1.0, 1.0].
    virtual const domain_type& ortho_domain() const noexcept
    { return b_ortho_domain; }


  public:
    // >>> INSTROSPECTION

    //! \brief Write an ReST-formatted description of the attributes
    void describe(std::ostream& os) const noexcept
    {
    os << "\n"
       << "***********************************************************\n"
       << name() << ": " << type() << " basis set\n"
       << "***********************************************************\n";

    if (!description().empty())
    {
        os << "*" << description() << "*\n";
    }

    os << "\n";

    // Print the attributes
    os << "This basis set has the following attributes:"
       << "\n:Order:     " << order()
       << "\n:Normalize: " << normalize()
       << "\n:" << domain().label() << " domain: "
         << "[" << domain().lower() << ", " << domain().upper() << "]"
       << "\n:" << ortho_domain().label() << " domain: "
         << "[" << ortho_domain().lower() << ", "
         <<        ortho_domain().upper() << "]"
       << "\n";
    }

    //! \brief For testing equality
    bool operator==(const Basis_Attr& attr) const noexcept;

}; // end class Basis_Attr

//---------------------------------------------------------------------------//
} // end namespace FET

#endif // src_Polynomials_Attr_Basis_Attr_hh
//---------------------------------------------------------------------------//
// end of src/Polynomials/Attr/Basis_Attr.hh
//---------------------------------------------------------------------------//
