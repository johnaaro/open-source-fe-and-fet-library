//---------------------------------*-C++-*-----------------------------------//
/*!
 * \file   src/Polynomials/Attr/test/tstHermite_Attr.cc
 * \brief  Hermite_Attr class test.
 */
//---------------------------------------------------------------------------//

#include <limits>

#include "FET_DBC.hh"
#include "Hermite_Attr.hh"

namespace FET
{
namespace TEST
{

  //! Tests for Hermite class and derived class functionality
  void TEST_Hermite_Attr()
  {
      // Create a Hermite_Attr object
      FET::Hermite_Attr attr;

      // Test attributes that differ from the Basis_Attr
      Insist(attr.type() == "Hermite",
              "The wrong type was specified for Hermite attributes.");
      Insist(attr.ortho_domain().lower() == -FET::Hermite_Attr::std_bound &&
              attr.ortho_domain().upper() == FET::Hermite_Attr::std_bound &&
              attr.ortho_domain().range() ==
                 2.0 * FET::Hermite_Attr::std_bound,
              "The Hermite series' normalized domain was not properly "
                 << "specified.");
  }

} // End of namespace TEST
} // End of namespace FET


int main()
{
  FET::TEST::TEST_Hermite_Attr();
  return 0;
}


//---------------------------------------------------------------------------//
// end of src/Polynomials/Attr/test/tstHermite_Attr.cc
//---------------------------------------------------------------------------//
