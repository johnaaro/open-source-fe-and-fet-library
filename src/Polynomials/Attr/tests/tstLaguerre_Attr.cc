//---------------------------------*-C++-*-----------------------------------//
/*!
 * \file   src/Polynomials/Attr/test/tstLaguerre_Attr.cc
 * \brief  Laguerre_Attr class test.
 */
//---------------------------------------------------------------------------//

#include "FET_DBC.hh"
#include "Laguerre_Attr.hh"

namespace FET
{
namespace TEST
{

  //! Tests for Laguerre class and derived class functionality
  void TEST_Laguerre_Attr()
  {
      // Create a Laguerre_Attr object
      FET::Laguerre_Attr attr;

      // Test attributes that differ from the Basis_Attr
      Insist(attr.type() == "Laguerre",
              "The wrong type was specified for Laguerre attributes.");
      Insist(attr.ortho_domain().lower() == 0.0 &&
              attr.ortho_domain().upper() == FET::Laguerre_Attr::std_bound &&
              attr.ortho_domain().range() ==
              FET::Laguerre_Attr::std_bound,
              "The Laguerre series' normalized domain was not properly "
                 << "specified.");
  }

} // End of namespace TEST
} // End of namespace FET


int main()
{
  FET::TEST::TEST_Laguerre_Attr();
  return 0;
}


//---------------------------------------------------------------------------//
// end of src/Polynomials/Attr/test/tstLaguerre_Attr.cc
//---------------------------------------------------------------------------//
