//---------------------------------*-C++-*-----------------------------------//
/*!
 * \file   src/Polynomials/Attr/test/tstBasis_Attr.cc
 * \brief  Basis_Attr class test.
 */
//---------------------------------------------------------------------------//

#include "FET_DBC.hh"

#include "Domain.hh"
#include "Basis_Attr.hh"

namespace FET
{
namespace TEST
{

  // Tests for base class and derived class functionality
  void TEST_Attr_Default(Basis_Attr &attr)
  {
      Insist(attr.name() == "untitled", "Default name is wrong");
      Insist(attr.description() == "", "Default description is wrong");
      Insist(attr.order() == 12, "Default order is wrong");
      Insist(attr.domain().lower() == 0.0 &&
              attr.domain().upper() == 0.0, "Default domains are wrong");
      Insist(attr.domain().range() == 0.0, "Default range is wrong");
      Insist(!attr.normalize(), "Default normalization is wrong");
  }

  void TEST_Attr_Constructor()
  {
      // Create a domain
      FET::Domain<double> domain("x", -10.0, 5.0);

      // Create a basis attribute for a 5th order polynomial on [-5, 5]
      FET::Basis_Attr attr("Some basis set", 5, domain);

      // Insist the attribute proporties
      Insist(attr.name() == "Some basis set", "set_name constructor failed");
      Insist(attr.order() == 5, "set_order constructor failed");
      Insist(attr.domain() == domain, "set_domain constructor failed");
  }

  void TEST_Attr_Set_Get_Name(Basis_Attr &attr)
  {
      attr.set_name("Basis Attr");
      Insist(attr.name() == "Basis Attr", "set_name failed");
  }

  void TEST_Attr_Description(Basis_Attr &attr)
  {
      attr.set_description("A longer description here.");
      Insist(attr.description() == "A longer description here.",
              "set_description failed");
  }

  void TEST_Attr_Domain(Basis_Attr &attr)
  {
    auto sim_domain =
        FET::Domain<double>("Some dimension label", -10.0, 10.0);
    attr.set_domain(sim_domain);
    Insist(attr.domain() == sim_domain,
        "Set domain failed at first occurrence");

    sim_domain = Domain<double>("Another label", -1.0, 1.0);
    attr.set_domain(sim_domain);
    Insist(attr.domain() == sim_domain,
        "Set domain failed at second occurrence");
  }

  void TEST_Attr_Ortho_Domain(const Basis_Attr &attr)
  {
      Insist(attr.ortho_domain().label() == "Normalized", "ortho label failed");
      Insist(attr.ortho_domain().lower() == -1.0, "ortho lower bound failed");
      Insist(attr.ortho_domain().upper() ==  1.0, "ortho upper bound failed");
      Insist(attr.ortho_domain().range() ==  2.0, "ortho range failed");
  }

  void TEST_Attr_Order(Basis_Attr &attr)
  {
      attr.set_order(11);
      Insist(attr.order() == 11, "set_order failed");
  }

  void TEST_Attr_Is_Normalized(Basis_Attr &attr)
  {
      attr.set_normalize(true);
      Insist(attr.normalize(), "set_normalize failed");
  }

  void TEST_Attr_Type(const Basis_Attr &attr)
  {
      Insist(attr.type() == "Basis", "Basis type failed to return proper type");
  }

  void TEST_Basis_Attr()
  {
    // Create Basis_Attr object
    FET::Basis_Attr l_attr;

    // Test the API now:
    TEST_Attr_Default(l_attr);
    TEST_Attr_Constructor();
    TEST_Attr_Set_Get_Name(l_attr);
    TEST_Attr_Description(l_attr);
    TEST_Attr_Domain(l_attr);
    TEST_Attr_Ortho_Domain(l_attr);
    TEST_Attr_Is_Normalized(l_attr);
    TEST_Attr_Order(l_attr);
    TEST_Attr_Type(l_attr);
  }

} // End of namespace TEST
} // End of namespace FET


int main()
{
  FET::TEST::TEST_Basis_Attr();
  return 0;
}


//---------------------------------------------------------------------------//
// end of src/Polynomials/Attr/test/tstBasis_Attr.cc
//---------------------------------------------------------------------------//
