//---------------------------------*-C++-*-----------------------------------//
/*!
 * \file   src/Polynomials/Attr/Hermite_Attr.hh
 * \brief  Hermite_Attr class declaration.
 */
//---------------------------------------------------------------------------//
#ifndef src_Polynomials_Attr_Hermite_Attr_hh
#define src_Polynomials_Attr_Hermite_Attr_hh

#include <limits>
#include "Basis_Attr.hh"

namespace FET
{
//===========================================================================//
/*!
 * \class Hermite_Attr
 * \brief Basis set attributes for Hermite polynomials
 *
 * The Hermite_Attr class contains the specific details of a Hermite
 * polynomial basis set.
 */
/*!
 * \example src/Polynomials/Attr/test/tstHermite_Attr.cc
 *
 * Test of Hermite_Attr
 */
//===========================================================================//
class Hermite_Attr : public Basis_Attr
{
    using Base              = Basis_Attr;
  public:

    //! \brief Effective standardized bound for Hermite polynomials
    static constexpr double std_bound =
        2.5;
    //    std::numeric_limits<double>::max() / 2.0;


  private:
    // >>> IMPLEMENTATION DATA

    //! \brief The basis set's orthogonal domain
    const Base::domain_type b_ortho_domain =
        Base::domain_type("Normalized", -std_bound, std_bound);


  public:
    // >>> CONSTRUCTION/SETTERS

    //! \brief Constructor for common arguments
    Hermite_Attr(
            const std::string& name,
            const Base::order_type order,
            const Base::domain_type& dom)
        : Base(name, order, dom)
    { /* * */ }

    //@{
    //! Copy control (default, copy, move, and destructor)
    Hermite_Attr() = default;
    Hermite_Attr(const Hermite_Attr&) = default;
    Hermite_Attr& operator=(const Hermite_Attr&) = default;
    Hermite_Attr(Hermite_Attr&&) = default;
    Hermite_Attr& operator=(Hermite_Attr&&) = default;
    virtual ~Hermite_Attr() = default;
    //@}


  public:
    // >>> INTROSPECTION/GETTERS

    //! \brief Return the type description
    const std::string type() const noexcept override
    { return "Hermite"; }

    //! \brief Returns the standardized domain of the basis set
    const Base::domain_type& ortho_domain() const noexcept override
    { return b_ortho_domain; }


}; // end class Hermite_Attr

//---------------------------------------------------------------------------//
} // end namespace FET

#endif // src_Polynomials_Attr_Hermite_Attr_hh
//---------------------------------------------------------------------------//
// end of src/Polynomials/Attr/Hermite_Attr.hh
//---------------------------------------------------------------------------//
