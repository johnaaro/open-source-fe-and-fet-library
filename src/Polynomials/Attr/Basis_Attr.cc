//---------------------------------*-C++-*-----------------------------------//
/*!
 * \file   src/Polynomials/Attr/Basis_Attr.cc
 * \brief  Basis_Attr class definitions.
 */
//---------------------------------------------------------------------------//

#include <iostream>

#ifdef USE_HDF5
#include "Nemesis/hdf5/core/Writer.hh"
#endif

#include "Basis_Attr.hh"

namespace FET
{

//---------------------------------------------------------------------------//
/*!
 * \brief Checks for equality of attributes
 */
bool Basis_Attr::operator==(const Basis_Attr& attr) const noexcept
{
    // Check each member, return if inequality found
    if (!(type()        == attr.type()))        return false;
    if (!(name()        == attr.name()))        return false;
    if (!(description() == attr.description())) return false;
    if (!(domain()      == attr.domain()))      return false;
    if (!(normalize()   == attr.normalize()))   return false;

    // No inequalities found
    return true;
}

//! \brief Write an ReST-formatted description of the attributes
/*
void Basis_Attr::describe(std::ostream& os) const noexcept
{
os << "\n"
   << "***********************************************************\n"
   << name() << ": " << type() << " basis set\n"
   << "***********************************************************\n";

if (!description().empty())
{
    os << "*" << description() << "*\n";
}

os << "\n";

// Print the attributes
os << "This basis set has the following attributes:"
   << "\n:Order:     " << order()
   << "\n:Normalize: " << normalize()
   << "\n:" << domain().label() << " domain: "
     << "[" << domain().lower() << ", " << domain().upper() << "]"
   << "\n:" << ortho_domain().label() << " domain: "
     << "[" << ortho_domain().lower() << ", "
     <<        ortho_domain().upper() << "]"
   << "\n";
}*/

} // end namespace FET

//---------------------------------------------------------------------------//
// end of src/Polynomials/Attr/Basis_Attr.cc
//---------------------------------------------------------------------------//
