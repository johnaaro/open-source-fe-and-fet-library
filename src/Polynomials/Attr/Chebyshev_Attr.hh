//---------------------------------*-C++-*-----------------------------------//
/*!
 * \file   src/Polynomials/Attr/Chebyshev_Attr.hh
 * \brief  Chebyshev_Attr class declaration.
 */
//---------------------------------------------------------------------------//
#ifndef src__Polynomials_Attr_Chebyshev_Attr_hh
#define src__Polynomials_Attr_Chebyshev_Attr_hh

#include "Basis_Attr.hh"

namespace FET
{
//===========================================================================//
/*!
 * \class Chebyshev_Attr
 * \brief Basis set attributes for Chebyshev polynomials
 *
 * The Chebyshev_Attr class contains the specific details of a Chebyshev
 * polynomial basis set.
 */
/*!
 * \example src/Polynomials/Attr/test/tstChebyshev_Attr.cc
 *
 * Test of Chebyshev_Attr
 */
//===========================================================================//
class Chebyshev_Attr : public Basis_Attr
{
    using Base              = Basis_Attr;

  private:
    // >>> IMPLEMENTATION DATA

    //! \brief The basis set's orthogonal domain
    const Base::domain_type b_ortho_domain =
        Base::domain_type("Normalized", -1.0, 1.0);

  public:
    // >>> CONSTRUCTION/SETTERS

    //! \brief Constructor for common arguments
    Chebyshev_Attr(
            const std::string& name,
            const Base::order_type order,
            const Base::domain_type& dom)
        : Base(name, order, dom)
    { /* * */ }

    //@{
    //! Copy control (default, copy, move, and destructor)
    Chebyshev_Attr() = default;
    Chebyshev_Attr(const Chebyshev_Attr&) = default;
    Chebyshev_Attr& operator=(const Chebyshev_Attr&) = default;
    Chebyshev_Attr(Chebyshev_Attr&&) = default;
    Chebyshev_Attr& operator=(Chebyshev_Attr&&) = default;
    virtual ~Chebyshev_Attr() = default;
    //@}


  public:
    // >>> INTROSPECTION/GETTERS


    //! \brief Return the type description
    const std::string type() const noexcept override
    { return "Chebyshev"; }

    //! \brief Returns the standardized domain of the basis set
    const Base::domain_type& ortho_domain() const noexcept override
    { return b_ortho_domain; }

}; // end class Chebyshev_Attr

//---------------------------------------------------------------------------//
} // end namespace FET

#endif // src_Polynomials_Attr_Chebyshev_Attr_hh
//---------------------------------------------------------------------------//
// end of src/Polynomials/Attr/Chebyshev_Attr.hh
//---------------------------------------------------------------------------//
