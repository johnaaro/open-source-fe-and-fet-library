//---------------------------------*-C++-*-----------------------------------//
/*!
 * \file   src/Polynomials/Attr/Laguerre_Attr.hh
 * \brief  Laguerre_Attr class declaration.
 */
//---------------------------------------------------------------------------//
#ifndef src_Polynomials_Attr_Laguerre_Attr_hh
#define src_Polynomials_Attr_Laguerre_Attr_hh

#include <limits>
#include "Basis_Attr.hh"

namespace FET
{
//===========================================================================//
/*!
 * \class Laguerre_Attr
 * \brief Basis set attributes for Laguerre polynomials
 *
 * The Laguerre_Attr class contains the specific details of a Laguerre
 * polynomial basis set.
 */
/*!
 * \example src/Polynomials/Attr/test/tstLaguerre_Attr.cc
 *
 * Test of Laguerre_Attr
 */
//===========================================================================//
class Laguerre_Attr : public Basis_Attr
{
    using Base              = Basis_Attr;
  public:

    //! \brief Effective standardized bound for Laguerre polynomials
    static constexpr double std_bound =
        5.0;
    //    std::numeric_limits<double>::max() / 2.0;


  private:
    // >>> IMPLEMENTATION DATA

    //! \brief The basis set's orthogonal domain
    const Base::domain_type b_ortho_domain =
        Base::domain_type("Normalized",
                0.0, std_bound);


  public:
    // >>> CONSTRUCTION/SETTERS

    //! \brief Constructor for common arguments
    Laguerre_Attr(
            const Base::string& name,
            const Base::order_type order,
            const Base::domain_type& dom)
        : Base(name, order, dom)
    { /* * */ }

    //@{
    //! Copy control (default, copy, move, and destructor)
    Laguerre_Attr() = default;
    Laguerre_Attr(const Laguerre_Attr&) = default;
    Laguerre_Attr& operator=(const Laguerre_Attr&) = default;
    Laguerre_Attr(Laguerre_Attr&&) = default;
    Laguerre_Attr& operator=(Laguerre_Attr&&) = default;
    virtual ~Laguerre_Attr() = default;
    //@}


  public:
    // >>> INTROSPECTION/GETTERS

    //! \brief Return the type description
    const std::string type() const noexcept override
    { return "Laguerre"; }

    //! \brief Returns the standardized domain of the basis set
    const Base::domain_type& ortho_domain() const noexcept override
    { return b_ortho_domain; }

}; // end class Laguerre_Attr

//---------------------------------------------------------------------------//
} // end namespace FET

#endif // src_Polynomials_Attr_Laguerre_Attr_hh
//---------------------------------------------------------------------------//
// end of src/Polynomials/Attr/Laguerre_Attr.hh
//---------------------------------------------------------------------------//
