//---------------------------------*-C++-*-----------------------------------//
/*!
 * \file   src/Polynomials/Attr/test/tstZernike_Attr.cc
 * \brief  Zernike_Attr class test.
 */
//---------------------------------------------------------------------------//

#include "FET_DBC.hh"
#include "Zernike_Attr.hh"

namespace FET {
    namespace TEST {

        //! Tests for Zernike class and derived class functionality
        void TEST_Zernike_Attr() {
            // Create a Zernike_Attr object
            FET::Zernike_Attr attr;

            // Test attributes that differ from the Basis_Attr
            Insist(attr.type() == "Zernike",
                   "The wrong type was specified for Zernike attributes.");
            Insist(attr.ortho_domain().lower() == -1.0 &&
                   attr.ortho_domain().upper() == 1.0 &&
                   attr.ortho_domain().range() == 2.0,
                   "The Zernike series' normalized domain was not properly "
                           << "specified.");
        }

    } // End of namespace TEST
} // End of namespace FET


int main() {
    FET::TEST::TEST_Zernike_Attr();
    return 0;
}


//---------------------------------------------------------------------------//
// end of src/Polynomials/Attr/test/tstZernike_Attr.cc
//---------------------------------------------------------------------------//
