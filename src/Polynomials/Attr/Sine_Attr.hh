//---------------------------------*-C++-*-----------------------------------//
/*!
 * \file   src/Polynomials/Attr/Sine_Attr.hh
 * \brief  Sine_Attr class declaration.
 */
//---------------------------------------------------------------------------//
#ifndef src_Polynomials_Attr_Sine_Attr_hh
#define src_Polynomials_Attr_Sine_Attr_hh

#include "Constants.hh"
#include "Basis_Attr.hh"

namespace FET
{
//===========================================================================//
/*!
 * \class Sine_Attr
 * \brief Basis set attributes for Sine polynomials
 *
 * The Sine_Attr class contains the specific details of a Sine
 * polynomial basis set.
 */
/*!
 * \example src/Polynomials/Attr/test/tstSine_Attr.cc
 *
 * Test of Sine_Attr
 */
//===========================================================================//
class Sine_Attr : public Basis_Attr
{
    using Base              = Basis_Attr;

  private:
    // >>> IMPLEMENTATION DATA

    //! \brief The basis set's orthogonal domain
    const Base::domain_type b_ortho_domain =
        Base::domain_type("Normalized", -pi, pi);


  public:
    // >>> CONSTRUCTION/SETTERS

    //! \brief Constructor for common arguments
    Sine_Attr(
            const Base::string& name,
            const Base::order_type order,
            const Base::domain_type& dom)
        : Base(name, order, dom)
    { /* * */ }

    //@{
    //! Copy control (default, copy, move, and destructor)
    Sine_Attr() = default;
    Sine_Attr(const Sine_Attr&) = default;
    Sine_Attr& operator=(const Sine_Attr&) = default;
    Sine_Attr(Sine_Attr&&) = default;
    Sine_Attr& operator=(Sine_Attr&&) = default;
    virtual ~Sine_Attr() = default;
    //@}


  public:
    // >>> INTROSPECTION/GETTERS

    //! \brief Return the type description
    const std::string type() const noexcept override
    { return "Sine"; }

    //! \brief Returns the standardized domain of the basis set
    const Base::domain_type& ortho_domain() const noexcept override
    { return b_ortho_domain; }


}; // end class Sine_Attr

//---------------------------------------------------------------------------//
} // end namespace FET

#endif // src_Polynomials_Attr_Sine_Attr_hh
//---------------------------------------------------------------------------//
// end of src/Polynomials/Attr/Sine_Attr.hh
//---------------------------------------------------------------------------//
