//---------------------------------*-C++-*-----------------------------------//
/*!
 * \file   src/Polynomials/Sine_Poly.i.hh
 * \brief  Sine_Poly inline definitions.
 */
//---------------------------------------------------------------------------//
#ifndef src_Polynomials_Sine_Poly_i_hh
#define src_Polynomials_Sine_Poly_i_hh

#include "Sine_Poly.hh"

#include <cmath>

#include "Comparisons.hh"
#include "Constants.hh"

namespace FET
{

//---------------------------------------------------------------------------//
/*!
 * \brief Evaluates the polynomial for all orders specified for a domain point
 *
 * Evaluates the polynomial to the requested order at a given simulation domain
 * value.
 *
 * The Sine polynomials are calculated using a direct calculation, \f$
 * \psi_{n}(x) = sin(n x) \f$.
 */
template<typename T>
typename Sine_Poly<T>::set_type
Sine_Poly<T>::evaluate(
        const Sine_Poly<T>::order_type order,
        const Sine_Poly<T>::data_type x) const
{
    // Pre-process
    auto poly_vals = Sine_Poly<T>::Base::evaluate(order, x);

    // Set non-recusive polynomials
    switch (order)
    {
        default:
        case 20:
            poly_vals[20] = std::sin(20.0 * x);
        case 19:
            poly_vals[19] = std::sin(19.0 * x);
        case 18:
            poly_vals[18] = std::sin(18.0 * x);
        case 17:
            poly_vals[17] = std::sin(17.0 * x);
        case 16:
            poly_vals[16] = std::sin(16.0 * x);
        case 15:
            poly_vals[15] = std::sin(15.0 * x);
        case 14:
            poly_vals[14] = std::sin(14.0 * x);
        case 13:
            poly_vals[13] = std::sin(13.0 * x);
        case 12:
            poly_vals[12] = std::sin(12.0 * x);
        case 11:
            poly_vals[11] = std::sin(11.0 * x);
        case 10:
            poly_vals[10] = std::sin(10.0 * x);
        case  9:
            poly_vals[ 9] = std::sin( 9.0 * x);
        case  8:
            poly_vals[ 8] = std::sin( 8.0 * x);
        case  7:
            poly_vals[ 7] = std::sin( 7.0 * x);
        case  6:
            poly_vals[ 6] = std::sin( 6.0 * x);
        case  5:
            poly_vals[ 5] = std::sin( 5.0 * x);
        case  4:
            poly_vals[ 4] = std::sin( 4.0 * x);
        case  3:
            poly_vals[ 3] = std::sin( 3.0 * x);
        case  2:
            poly_vals[ 2] = std::sin( 2.0 * x);
        case  1:
            poly_vals[ 1] = std::sin(x);
        case  0:
            poly_vals[ 0] = 0.0;
    }

    // Recursively determine higher order terms
    for (Sine_Poly<T>::order_type n = 2; n <= order; ++n)
    {
        poly_vals[n] = std::sin(static_cast<Sine_Poly<T>::data_type>(n) * x);
    }

    // Normalize, is requested
    if (Sine_Poly<T>::attr()->normalize())
    {
        normalize(poly_vals);
    }

    // Return the polynomial set
    return poly_vals;
}


//---------------------------------------------------------------------------//
/*!
 * \brief Normalizes the provided vector by order according to its index
 *
 * The Sine polynomial order's normalization constant is determined as the
 * inverse of the l-2 norm (i.e. integration of the square of the basis
 * function of that order over the orthonormal domain). Normalization is done
 * on the given vector by multiplying it by the inverse of the l-2 norm.
 *
 * The normalization coefficient for a given order is calculated as follows:
 * \f[
 * c_{\psi_{n}}
 *      = (\int_{a}^{b} \psi_{n}(z)^{2} dz)^{-1}
 *      = (\frac{b - a}{2} - \frac{sin(2 n b) - sin(2 n a)}{4 n})^{-1}
 * \f]
 *
 * With a special case of \f$ c_{\psi_{0}} = \infty \f$, a value of \f$
 * \psi_{0}(z) = 0 \f$ is assigned.
 *
 * Applied over the domain \f$ [-\pi, \pi] \f$, the above simplifies to
 * \f$ 1 / \pi \f$.
 */
 //---------------------------------------------------------------------------//
template<typename T>
void Sine_Poly<T>::normalize(
        Sine_Poly<T>::set_type& poly_vals) const noexcept
{
    // Pre-process
    Sine_Poly<T>::Base::normalize(poly_vals);
    if (poly_vals.size() == 0) return;

    const auto dom = Sine_Poly<T>::attr()->domain();
    const Sine_Poly<T>::data_type coeff = dom.range() / 2.0;
    poly_vals[0] = 0.0;
    for (Sine_Poly<T>::order_type i = 1; i < poly_vals.size(); ++i)
    {
        const auto order = static_cast<Sine_Poly<T>::data_type>(i);
        poly_vals[i] /= coeff -
                (sin(2.0 * order * dom.upper())
                    - sin(2.0 * order * dom.lower())) / (4.0 * order);
    }
}


//---------------------------------------------------------------------------//
/*!
 * \brief Normalizes the polynomials on the polynomial's orthogonal  domain
 *
 * Normalizes the argument, .i.hhording to the respective order, based on the \f$
 * l^2 \f$ norm of the polynomial on the orthogonal domain.
 *
 * \see normalize
 */
template<typename T>
void Sine_Poly<T>::ortho_normalize(
        Sine_Poly<T>::set_type& poly_vals) const noexcept
{
    // Pre-process
    Sine_Poly<T>::Base::ortho_normalize(poly_vals);
    if (poly_vals.size() == 0) return;


    poly_vals[0]  = 0.0;
    for (Sine_Poly<T>::order_type i = 1; i < poly_vals.size(); ++i)
    {
        poly_vals[i] /= pi;
    }
}


//---------------------------------------------------------------------------//
/*!
 * \brief Integrates the basis set on a given standardized domain
 *
 * Integrates the Sine basis set on a given standardized domain. The
 * returned vector is notated as integrated_value[order], i.e. the element
 * index in the vector is representative of the integrated value at that order.
 *
 * The Sine polynomial integration is implemented using the following
 * formula:
 * \f[
 * \int_{a}^{b} \psi_{n}(r) \rho(r) dr =
 *    = \frac{1}{n} [cos(n a) - cos(n b)]
 * \f]
 *
 * The special case of \f$ n = 0 \f$ is treated separately, having an integral
 * \f$ = 0 \f$.
 */
 //---------------------------------------------------------------------------//
template<typename T>
typename Sine_Poly<T>::set_type
Sine_Poly<T>::integrate(
        const Sine_Poly<T>::data_type lower_b,
        const Sine_Poly<T>::data_type upper_b,
        const Sine_Poly<T>::order_type  order) const
{
    // Pre-process and obtain default result
    auto result = Sine_Poly<T>::Base::integrate(lower_b, upper_b, order);
    if (is_approximate(upper_b, lower_b)) return result;

    // Set integrated results
    result[0] = 0.0;
    for (Sine_Poly<T>::order_type i = 1; i <= order; ++i)
    {
        const auto n = static_cast<Sine_Poly<T>::data_type>(i);
        result[i] = (std::cos(n * lower_b) - std::cos(n * upper_b)) /
            n;
    }

    // Return the vector
    return result;
}


//---------------------------------------------------------------------------//
} // namespace FET

//---------------------------------------------------------------------------//
#endif // src_Polynomials_Sine_Poly_i_hh
//---------------------------------------------------------------------------//
// end of src/Polynomials/Sine_Poly.i.hh
//---------------------------------------------------------------------------//
