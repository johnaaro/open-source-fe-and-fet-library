//---------------------------------*-C++-*-----------------------------------//
/*!
 * \file   src/Polynomials/Legendre_Poly.i.hh
 * \brief  Legendre_Poly class definitions.
 */
//---------------------------------------------------------------------------//
#ifndef src_Polynomials_Legendre_Poly_i_hh
#define src_Polynomials_Legendre_Poly_i_hh

#include "Legendre_Poly.hh"

#include <vector>

#include "Comparisons.hh"
#include "FET_DBC.hh"

namespace FET
{

//---------------------------------------------------------------------------//
/*!
 * \brief Evaluates the polynomial for all orders specified for a domain point
 *
 * Evaluates the polynomial to the requested order at a given simulation domain
 * value.
 *
 * The Legendre polynomials are calculated using the recurrence relation, shown
 * below:
 * \f[
 * P_{n}(x) = \frac{(2n - 1) x P_{n-1}(x) - (n - 1) P_{n - 2}(x)}{n}
 * \f]
 *
 * With \f$ P_{0} = 1 \f$ and \f$ P_{1} = x \f$. A direct calculation of
 * the Legendre polynomials is done for orders \f$ P_{n=10} \f$ and lower
 * for efficiency, with the above recursive relationship used otherwise.
 */
template<typename T>
typename Legendre_Poly<T>::set_type
Legendre_Poly<T>::evaluate(
        const Legendre_Poly<T>::order_type order,
        const Legendre_Poly<T>::data_type      x) const
{
    // Pre-process and obtain initial variable
    auto poly_vals = Legendre_Poly<T>::Base::evaluate(order, x);

    // Obtain preliminary domain values
    const Legendre_Poly<T>::data_type x2 = x * x;
//    const float root2 = std::sqrt(2);

    // Set non-recusive polynomials
    switch (order)
    {
//        default:
//        case 10:
//            poly_vals[10] = std::sqrt(21) * (((((46189. * x2 - 109395.) * x2 + 90090.) * x2 -
//                               30030.) * x2 + 3465.) * x2 - 63.) / (256 * root2);
//        case  9:
//            poly_vals[ 9] = std::sqrt(19) * (((((12155. * x2 - 25740.) * x2 + 18018.) * x2 -
//                               4620.) * x2 + 315.) * x)/ (128 * root2);
//        case  8:
//            poly_vals[ 8] = std::sqrt(17) * ((((6435. * x2 - 12012.) * x2 + 6930.) * x2 -
//                              1260.) * x2 + 35.) / (128 * root2);
//        case  7:
//            poly_vals[ 7] = std::sqrt(15) * ((((429. * x2 - 693.) * x2 + 315.) * x2 - 35.) *
//                             x) / (16 * root2);
//        case  6:
//            poly_vals[ 6] = std::sqrt(13) * (((231. * x2 - 315.) * x2 + 105.) * x2 - 5.) / (16 * root2);
//        case  5:
//            poly_vals[ 5] = std::sqrt(11) * (((63. * x2 - 70.) * x2 + 15.) * x) / (8 * root2);
//        case  4:
//            poly_vals[ 4] = std::sqrt(9) * ((35. * x2- 30.) * x2 + 3.) / (8 * root2);
//        case  3:
//            poly_vals[ 3] = std::sqrt(7) * ((5. * x2- 3.) * x) / (2 * root2);
//        case  2:
//            poly_vals[ 2] = std::sqrt(5) * (3. * x2 - 1.) / (2 * root2);
////            poly_vals[ 2] = std::sqrt(5) * (3. * x2 - 1.) / (2 * std::sqrt(2));
//        case  1:
//            poly_vals[ 1] = std::sqrt(3) * x / root2;
//        case  0:
//            poly_vals[ 0] = 1.0 / root2;
////            poly_vals[ 0] = 1.0 / std::sqrt(2);


        default:
        case 10:
            poly_vals[10] = (((((46189. * x2 - 109395.) * x2 + 90090.) * x2 -
                            30030.) * x2 + 3465.) * x2 - 63.) / 256.;
        case  9:
            poly_vals[ 9] = (((((12155. * x2 - 25740.) * x2 + 18018.) * x2 -
                            4620.) * x2 + 315.) * x)/ 128.;
        case  8:
            poly_vals[ 8] = ((((6435. * x2 - 12012.) * x2 + 6930.) * x2 -
                        1260.) * x2 + 35.) / 128.;
        case  7:
            poly_vals[ 7] = ((((429. * x2 - 693.) * x2 + 315.) * x2 - 35.) *
                    x) / 16.;
        case  6:
            poly_vals[ 6] = (((231. * x2 - 315.) * x2 + 105.) * x2 - 5.) / 16.;
        case  5:
            poly_vals[ 5] = (((63. * x2 - 70.) * x2 + 15.) * x) / 8.;
        case  4:
            poly_vals[ 4] = ((35. * x2- 30.) * x2 + 3.) / 8.;
        case  3:
            poly_vals[ 3] = ((5. * x2- 3.) * x) / 2.;
        case  2:
            poly_vals[ 2] = (3. * x2 - 1.) / 2.;
        case  1:
            poly_vals[ 1] = x;
        case  0:
            poly_vals[ 0] = 1.0;
    }

    // Recursively determine higher order terms
    for (Legendre_Poly<T>::order_type n = 11; n <= order; ++n)
    {
        const auto i = static_cast<Legendre_Poly<T>::data_type>(n);
        poly_vals[n] = ( (2.0 * i - 1.0) * x *
                poly_vals[n - 1] - (i - 1.0) *
                poly_vals[n - 2] ) / i;
    }

    // Normalize, is requested
    if (Legendre_Poly<T>::attr()->normalize())
    {
        normalize(poly_vals);
    }

    // Return the polynomial set
    return poly_vals;
}


//---------------------------------------------------------------------------//
/*!
 * \brief Normalizes the provided vector by order according to its index
 *
 * The Legendre polynomial order's normalization constant is determined as the
 * inverse of the l-2 norm (i.e. integration of the square of the basis
 * function of that order over the orthonormal domain). Normalization is done
 * on the given vector by multiplying it by the inverse of the l-2 norm.
 *
 * The normalization coefficient for a given order is calculated as follows:
 * \f[
 * c_{\psi_{n}} = (\int \psi_{n}(z)^{2} dz)^{-1} = \frac{2 n + 1}{2}
 * \f]
 */
 //---------------------------------------------------------------------------//
template<typename T>
void Legendre_Poly<T>::normalize(
        Legendre_Poly<T>::set_type& poly_vals) const noexcept
{
    // Pre-process
//    Legendre_Poly<T>::Base::ortho_normalize(poly_vals);

    // Normalize
    const auto range = Legendre_Poly<T>::attr()->domain().range();
    for (Legendre_Poly<T>::order_type i = 0; i < poly_vals.size(); ++i)
    {

//        poly_vals[i] *= range / (2.0 * static_cast<Legendre_Poly<T>::data_type>(i) + 1.0);

        poly_vals[i] *=  (2.0 * static_cast<Legendre_Poly<T>::data_type>(i) +
                1.0) / range;
//        poly_vals[i] *=  std::sqrt(2.0 * static_cast<Legendre_Poly<T>::data_type>(i) +
//                          1.0) / range;
    }
}


//---------------------------------------------------------------------------//
/*!
 * \brief Normalizes the polynomials on the polynomial's orthogonal  domain
 *
 * Normalizes the argument, according to the respective order, based on the \f$
 * l^2 \f$ norm of the polynomial on the orthogonal domain.
 *
 * \see normalize
 */
template<typename T>
void Legendre_Poly<T>::ortho_normalize(
        Legendre_Poly<T>::set_type& poly_vals) const noexcept
{
    // Pre-process
    Legendre_Poly<T>::Base::ortho_normalize(poly_vals);

    // Normalize
//    const auto range = Legendre_Poly<T>::attr()->domain().range();
    for (Legendre_Poly<T>::order_type i = 0; i < poly_vals.size(); ++i)
    {
//         poly_vals[i] *= (2.0 * static_cast<Legendre_Poly<T>::data_type>(i)
//                 + 1.0) / attr()->domain().range();
        poly_vals[i] *= static_cast<Legendre_Poly<T>::data_type>(i) + 0.5;

//        poly_vals[i] *= range / (2.0 * static_cast<Legendre_Poly<T>::data_type>(i) + 1.0);
    }
}

template<typename T>
double Legendre_Poly<T>::orthonorm_const(int order) const noexcept
{
    const auto range = Legendre_Poly<T>::attr()->domain().range();
    return sqrt((2.0 * static_cast<Legendre_Poly<T>::data_type>(order) +
                 1.0) / range);
}

//---------------------------------------------------------------------------//
/*!
 * \brief Integrates the basis set on a given standardized domain
 *
 * Integrates the Legendre basis set on a given standardized domain. The
 * returned vector is notated as integrated_value[order], i.e. the element
 * index in the vector is representative of the integrated value at that order.
 *
 * The Legendre polynomial integration is implemented using the following
 * formula:
 * \f[
 * \int_{a}^{b} P_{n}(r) \rho(r) dr =
 *      \frac{ 1 }{ 2 n + 1} (
 *           (P_{n+1}(b) - P_{n+1}(a)) -
 *           (P_{n-1}(b) - P_{n-1}(a))
 *           )
 * \f]
 */
 //---------------------------------------------------------------------------//
template<typename T>
typename Legendre_Poly<T>::set_type
Legendre_Poly<T>::integrate(
        const Legendre_Poly<T>::data_type lower_b,
        const Legendre_Poly<T>::data_type upper_b,
        const Legendre_Poly<T>::order_type  order) const
{
    // Pre-process and obtain vector
    auto result = Legendre_Poly<T>::Base::integrate(lower_b, upper_b, order);
    if (is_approximate(upper_b, lower_b)) return result;

    // Obtain polynomial values at the specified bounds
    const auto lower_vals = evaluate(order + 1, lower_b);
    const auto upper_vals = evaluate(order + 1, upper_b);
//     const auto lower_vals = evaluate(order, lower_b);
//     const auto upper_vals = evaluate(order, upper_b);

    // Set integrated results
    result[0] = upper_b - lower_b;
    for (Legendre_Poly<T>::order_type i = 1; i <= order; ++i)
    {
        result[i] = (
                ( upper_vals[i + 1] - lower_vals[i + 1] ) -
                ( upper_vals[i - 1] - lower_vals[i - 1] )
                ) / (2.0 * static_cast<Legendre_Poly<T>::data_type>(i) + 1.0);
    }

    // Return the vector
    return result;
}


//---------------------------------------------------------------------------//
} // namespace FET

//---------------------------------------------------------------------------//
#endif // src_Polynomials_Legendre_Poly_i_hh
//---------------------------------------------------------------------------//
// end of src/Polynomials/Legendre_Poly.i.hh
//---------------------------------------------------------------------------//
