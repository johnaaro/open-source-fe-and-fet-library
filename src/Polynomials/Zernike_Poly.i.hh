//---------------------------------*-C++-*-----------------------------------//
/*!
 * \file   src/Polynomials/Zernike_Poly.i.hh
 * \brief  Zernike_Poly class definitions.
 */
//---------------------------------------------------------------------------//
#ifndef src_Polynomials_Zernike_Poly_i_hh
#define src_Polynomials_Zernike_Poly_i_hh

#include "Zernike_Poly.hh"

#include <vector>
#include <cmath>

#include "Comparisons.hh"
#include "FET_DBC.hh"

namespace FET {
//---------------------------------------------------------------------------//
/*!
 * \brief Evaluates the polynomial for all orders specified for a domain point
 *
 * Evaluates the polynomial to the requested order at a given simulation domain
 * value.
 */
    template<typename T>
    typename Zernike_Poly<T>::set_type
    Zernike_Poly<T>::evaluate(const Zernike_Poly<T>::order_type j,
                              const Zernike_Poly<T>::data_type x,
                              const Zernike_Poly<T>::data_type y) const {
        auto poly_vals = Zernike_Poly<T>::Base::evaluate(j, x, y);

        const Zernike_Poly<T>::data_type x2 = x * x;
        const Zernike_Poly<T>::data_type y2 = y * y;

        switch (j) {
            case 14:
                // order = 4 and angular = 4
                poly_vals[14] = (x2 * x2) - (6 * x2 * y2) + (y2 * y2);
            case 13:
                // order = 4 and angular = 2
                poly_vals[13] = (3 * x2) - (3 * y2) - (4 * x2 * x2) + (4 * y2 * y2);
            case 12:
                // order = 4 and angular = 0
                poly_vals[12] = 1 - (6 * x2) - (6 * y2) + (6 * x2 * x2) + (12 * x2 * y2) + (6 * y2 * y2);
            case 11:
                // order = 4 and angular = -2
                poly_vals[11] = (-6 * x * y) + (8 * x2 * x * y) + (8 * x * y2 * y);
            case 10:
                // order = 4 and angular = -4
                poly_vals[10] = (-4 * x2 * x * y) + (4 * x * y2 * y);
            case 9:
                // order = 3 and angular = 3
                poly_vals[9] = (y2 * y) - (3 * x2 * y);
            case 8:
                // order = 3 and angular = 1
                poly_vals[8] = (-2 * y) + (3 * y2 * y) + (3 * x2 * y);
            case 7:
                // order = 3 and angular = -1
                poly_vals[7] = (-2 * x) + (3 * x2 * x) + (3 * x * y2);
            case 6:
                // order = 3 and angular = -3
                poly_vals[6] = -(x2 * x) + (3 * x * y2);
            case 5:
                // order = 2 and angular = 2
                poly_vals[5] = -(x2) + y2;
            case 4:
                // order = 2 and angular = 0
                poly_vals[4] = -1 + 2 * x2 + 2 * y2;
            case 3:
                // order = 2 and angular = -2
                poly_vals[3] = 2 * x * y;
            case 2:
                // order = 1 and angular = 1
                poly_vals[2] = y;
            case 1:
                // order = 1 and angular = -1
                poly_vals[1] = x;
            case 0:
                // order = 0 and angular = 0
                poly_vals[0] = 1;
        }

        // Normalize, is requested
        if (Zernike_Poly<T>::attr()->normalize()) {
            normalize(poly_vals);
        }

        // Return the polynomial set
        return poly_vals;
    }


//---------------------------------------------------------------------------//
/*!
 * \brief Normalizes the provided vector by order according to its index
 */
//---------------------------------------------------------------------------//
    template<typename T>
    void Zernike_Poly<T>::normalize(
            Zernike_Poly<T>::set_type &poly_vals) const noexcept {

    }


//---------------------------------------------------------------------------//
/*!
 * \brief Normalizes the polynomials on the polynomial's orthogonal  domain
 */
//---------------------------------------------------------------------------//

    template<typename T>
    void Zernike_Poly<T>::ortho_normalize(
            Zernike_Poly<T>::set_type &poly_vals) const noexcept {

    }

//---------------------------------------------------------------------------//
/*!
 * \brief Integrates the basis set on a given standardized domain
 */
//---------------------------------------------------------------------------//
/*
    template<typename T>
    typename Zernike_Poly<T>::set_type
    Zernike_Poly<T>::integrate(
            const Zernike_Poly<T>::data_type lower_b,
            const Zernike_Poly<T>::data_type upper_b,
            const Zernike_Poly<T>::order_type  order) const
    {
       auto result = Zernike_Poly<T>::Base::integrate(lower_b, upper_b, order);
        return result;
    }

 */
}

#endif
