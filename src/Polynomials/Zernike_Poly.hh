//---------------------------------*-C++-*-----------------------------------//
/*!
 * \file   src/Polynomials/Legendre_Poly.hh
 * \brief  Legendre_Poly inline definitions.
 */
//---------------------------------------------------------------------------//

#ifndef src_Polynomials_Zernike_Poly_hh
#define src_Polynomials_Zernike_Poly_hh

#include <memory>
#include <vector>

#include "Basis_Poly.hh"
#include "Zernike_Attr.hh"
#include "Basis_Attr.hh"


#include "FET_DBC.hh"


namespace FET {

//===========================================================================//
/*!
 * \class Zernike_Poly
 * \brief Zernike Polynomial implementation
 *
 * Class for the implementation of Zernike polynomials. Based on the
 * Basis_Poly class and utilizes the Zernike_Attr class for its associated
 * attributes.
 *
 * \todo Use \c cmath and the associated Zernike() polynomials when compiled
 * with c++17 if more efficient
 */
/*!
 * \example src/Polynomials/test/tstZernike_Poly.cc
 *
 * Test of the Zernike polynomial.
 */
//===========================================================================//

    template<typename T = double>
    class Zernike_Poly : public Basis_Poly<T> {

    public:
        //@{
        //! Public type aliases
        using Base = Basis_Poly<T>;
        using data_type = typename Base::data_type;
        using order_type = typename Base::order_type;
        using set_type = typename Base::set_type;
        using Attr_type = FET::Basis_Attr;
        using Vec_Sp_Attr = typename Base::Vec_SP_Attr;

        //using Attr_type   = FET::Zernike_Attr; //ADDED 2 CHANGED FROM BASIS_ATTR TO ZERNIKE_ATTR
        //using order_type  = Attr_type::order_type;//ADDED2
        //using SP_Attr     = std::shared_ptr<const Attr_type>;//ADDED2
//        using Attr_type = typename FET::Zernike_Attr;// ADDED
//        //using SP_Attr  = typename Base::SP_Attr;
//        //@}
//        using SP_Attr     =  std::shared_ptr<const Attr_type>;//ADDED GOT RID OF RETURN TYPE ERROR

    private:

    public:
        // >>> CONSTRUCTOR/DESTRUCTORS

        //! brief constructor
        Zernike_Poly(const Zernike_Attr &attr)
                : Base(attr) {
            Insist(attr.type().compare("Zernike") == 0,
                   "Zernike basis sets may only accept Zernike Atrributes ("
                           << attr.type() << " received)");
        }

        //@{
        //! Copy control (default, copy, move, and destructor)
        Zernike_Poly() = default;

        Zernike_Poly(const Zernike_Poly &) = default;

        Zernike_Poly &operator=(const Zernike_Poly &) = default;

        Zernike_Poly(Zernike_Poly &&) = default;

        Zernike_Poly &operator=(Zernike_Poly &&) = default;

        virtual ~Zernike_Poly() = default;
        //@}

    public:
        // >>> GETTERS
        // N/A presently


    public:
        // >>> SETTERS
        // N/A presently

    public:
        // >>> INTROSPECTION

        //Adding Zernike Attributes to the Vec_SP ???????????
        Vec_Sp_Attr Zernike_Attrs = Vec_Sp_Attr(Zernike_Attr::getDomain(), Zernike_Attr::getAngle());

        // Evaluates the polynomials for the basis set at a domain value
        set_type evaluate(const order_type order,
                          const data_type x,
                          const data_type y)
        const override;


        // Returns a normalized set of evaluated Zernike polynomials
        void normalize(set_type &poly_values) const noexcept override;

        // Normalizes the polynomials on the orthogonal domain
        void ortho_normalize(set_type &poly_val) const noexcept override;

        // Returns a vector of integrated values on a given std. domain
        set_type integrate(const data_type lower_b, const data_type upper_b,
                           const order_type order)
        const override;

        set_type evaluate(const data_type lower_b) const override;// ADDED

        //! \brief Uses default order to evaluate the polynomial
        set_type evaluate(const data_type domain_val_x, const data_type domain_val_y) const
        { return evaluate(Base::order(), domain_val_x, domain_val_y); }


        //! \brief Uses the default order to evaluate the integration
        set_type integrate(const data_type lower_b, const data_type upper_b)
        const override { return integrate(lower_b, upper_b, Base::order()); }

    };// end class Zernike_Poly

//---------------------------------------------------------------------------//
} // end namespace FET

//---------------------------------------------------------------------------//
// INLINE AND TEMPLATE DEFINITIONS
//---------------------------------------------------------------------------//
#include "Zernike_Poly.i.hh"

//---------------------------------------------------------------------------//
#endif // src_Polynomials_Zernike_Poly_hh
//---------------------------------------------------------------------------//
// end of src/Polynomials/Zernike_Poly.hh
//---------------------------------------------------------------------------//
