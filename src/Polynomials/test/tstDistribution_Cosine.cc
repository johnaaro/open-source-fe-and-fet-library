//---------------------------------*-C++-*-----------------------------------//
/*!
 * \file   src/Polynomials/test/tstDistribution_Cosine.cc
 * \brief  Tests the Cosine series distribution
 */
//---------------------------------------------------------------------------//

#include "Domain.hh"
#include "Cosine_Attr.hh"
#include "Cosine_Poly.hh"

#include "make_distribution.cc"

namespace FET
{
namespace TEST
{

  //! \brief Tests the Cosine series distribution
  void Cosine_Distribution()
  {
    // Create some domain for the basis set
    Domain<double> domain("x", -20.0, 20.0);

    // Create the attributes for the basis set
    Cosine_Attr attrs("A single Cosine polynomial", 15, domain);
    attrs.set_normalize(false);

    // Create the Cosine basis set now
    Cosine_Poly<> poly(attrs);

    // Now print the distribution
    make_distribution(poly);

    return;
  }

} // End of namespace TEST
} // End of namespace FET


int main()
{
    FET::TEST::Cosine_Distribution();
    return 0;
}

//---------------------------------------------------------------------------//
// end of src/Polynomials/test/tstCosine_Orders.cc
//---------------------------------------------------------------------------//
