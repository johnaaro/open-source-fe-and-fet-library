//---------------------------------*-C++-*-----------------------------------//
/*!
 * \file   src/Polynomials/test/tstDistribution_Hermite.cc
 * \brief  Tests the Hermite series distribution
 */
//---------------------------------------------------------------------------//

#include "Domain.hh"
#include "Hermite_Attr.hh"
#include "Hermite_Poly.hh"

#include "make_distribution.cc"

namespace FET
{
namespace TEST
{

  //! \brief Tests the Hermite series distribution
  void Hermite_Distribution()
  {
    // Create some domain for the basis set
    Domain<double> domain("x", -20.0, 20.0);

    // Create the attributes for the basis set
    Hermite_Attr attrs("A single Hermite polynomial", 15, domain);
    attrs.set_normalize(false);

    // Create the Hermite basis set now
    Hermite_Poly<> poly(attrs);

    // Now print the distribution
    make_distribution(poly);

    return;
  }

} // End of namespace TEST
} // End of namespace FET


int main()
{
    FET::TEST::Hermite_Distribution();
    return 0;
}

//---------------------------------------------------------------------------//
// end of src/Polynomials/test/tstHermite_Orders.cc
//---------------------------------------------------------------------------//
