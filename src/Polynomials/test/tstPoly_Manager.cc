//---------------------------------*-C++-*-----------------------------------//
/*!
 * \file   src/Polynomials/test/tstPoly_Manager.cc
 * \brief  Poly_Manager class test.
 */
//---------------------------------------------------------------------------//

#include <cstdlib>
#include <memory>
#include <stdexcept>

#include "FET_Assertions.hh"
#include "FET_DBC.hh"
#include "Constants.hh"

#include "Domain.hh"
#include "Basis_Poly.hh"

#include "Cosine_Attr.hh"
#include "Cosine_Poly.hh"
#include "Legendre_Attr.hh"
#include "Legendre_Poly.hh"

#include "Poly_Manager.hh"

namespace FET
{
namespace TEST
{

    //! \brief Tests the manager's constructors
    Poly_Manager<> TEST_Poly_Manager_Base_Constructor()
    {
        auto foo = Poly_Manager<>();
        Insist(foo.validate(), "Failed to construct with default validation.");
        Insist(foo.size() == 0,
                "The manager failed to have a size of 0 on construction.");

        auto soo = Poly_Manager<>(false);
        Insist(!soo.validate(), "Failed to construct with no validation.");
        Insist(soo.empty(), "The manager failed to be empty on construction.");
        Insist(soo.volume() == 0, "The empty manager has non-zero volume.");

        // Create some polynomials
        auto d1 = Domain<double>("x", -5, 5);
        auto d2 = Domain<double>("y", -10, 0);
        auto a1 = Cosine_Attr("A Cosine series", 12, d1);
        auto a2 = Legendre_Attr("A Legendre polynomial", 12, d2);

        auto polys = std::vector<std::shared_ptr<const Basis_Poly<>>>(0);
        polys.push_back(std::make_shared<Cosine_Poly<>>(a1));
        polys.push_back(std::make_shared<Legendre_Poly<>>(a2));

        // Test construction with list now
        auto bar = Poly_Manager<>(true, polys);
        Insist(bar.size() == 2,
                "The manager failed to construct with two elements.");
        Insist(bar.basis(0)->type() == "Cosine",
                "The type created was '" << bar.basis("x")->type()
                    << "', not 'Cosine'.");
        Insist(bar.basis(1)->type() == "Legendre",
                "The type created was '" << bar.basis("y")->type()
                    << "', not 'Legendre'.");

        // Attempt to insert a repeated polynomial
        try
        {
            bar.insert(std::make_shared<Cosine_Poly<>>(a1));
            throw std::logic_error(
                    "Failed to throw for repeated polynomial."
                    );
        }
        catch (assertion& e)
        {
            // Properly threw error
        }

        return bar;
    }


    //! \brief Tests the manager's basic API, insert, and erase methods
    void TEST_Poly_Manager_API(Poly_Manager<>& bar)
    {
        // Attempt to insert an invalid polynomial (invalid domain)
        try
        {
            auto d3 = Domain<double>("foo", -10, 10);
            auto a3 = Cosine_Attr("Some series", 12, d3);
            bar.insert(std::make_shared<Cosine_Poly<>>(a3));
            throw std::logic_error(
                    "Failed to throw for invalid polynomial."
                    );
        }
        catch (assertion& e)
        {
            // Properly threw error
        }

        // Turn OFF validation check and play around with an invalid
        // polynomial
        bar.set_validate(false);
        Insist(!bar.validate(),
                "The polynomial manager's 'set_validate' method failed.");
        auto d4 = Domain<double>("foo", 0, 0);
        auto a4 = Legendre_Attr("Foo", 0, d4);
        bar.insert(std::make_shared<Legendre_Poly<>>(a4));
        Insist(bar.size() == 3 && !bar.empty(),
                "Failed to insert the 'foo' polynomial.");
        Insist(bar.contains("foo") && bar.contains(2),
                "The manager could not find the 'foo' polynomial.");
        Insist(bar["foo"] == bar[2], "The overloaded '[]' operations failed.");
        bar.erase("foo");
        Insist(!bar.contains("foo") && !bar.contains(2),
                "The manager could not 'erase' the 'foo' polynomial.");

        // Ensure all valid domains are considered
        const std::vector<Poly_Manager<>::key_type> domains = {
            "x", "y", "r", "theta", "phi",
            "r, theta", "theta, phi",
            "r, theta, phi"};
        for (const auto& domain : domains)
        {
            Insist(bar.is_valid(domain),
                    "The " << domain
                           << " was incorrectly found to be invalid.");
        }
        Insist(!bar.is_valid("pizza"),
                "The 'pizza' domain was incorrectly considered valid.");

        // Test volume method
        Insist(std::abs(bar.volume() - 100.0 ) <= tolerance,
                "The polynomial manager's 'volume' method produced "
                    << "an incorrect result.");

        // Empty the manager (just in case)
        bar.reset();
        Insist(bar.size() == 0 && bar.empty(),
                "Failed to reset the polynomial manager.");
    }


    //! \brief Tests the polynomial manager object
    void TEST_Poly_Manager()
    {
        auto foo = TEST_Poly_Manager_Base_Constructor();
        TEST_Poly_Manager_API(foo);
    }

} // End of namespace TEST
} // End of namespace FET


int main()
{
    FET::TEST::TEST_Poly_Manager();
    return 0;
}

//---------------------------------------------------------------------------//
// end of src/Polynomials/test/tstPoly_Manager.cc
//---------------------------------------------------------------------------//
