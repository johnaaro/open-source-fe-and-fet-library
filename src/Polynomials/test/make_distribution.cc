//---------------------------------*-C++-*-----------------------------------//
/*!
 * \file   src/Polynomials/test/make_distribution.cc
 * \brief  Prints a polynomial's distribution to a file and compares.
 */
//---------------------------------------------------------------------------//

#include <iomanip>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>

namespace FET
{
namespace TEST
{

  //! \brief Prints the distribution of a polynomial over it's domain
  template<class T>
  void make_distribution(const T& poly)
  {
    // Create distribution characteristics:
    const unsigned int num_points = 100;
    const double increment_size = poly.attr()->domain().range() /
        double(num_points);

    // Warn user if graphs may not be useful
    if (poly.attr()->normalize())
    {
        std::cout << "Normalization of a " << poly.type()
            << " series will yield non-standard distributions.\n";
    }
    if (poly.order() < 12)
    {
        std::cout << "The distribution will only be shown to order "
            << poly.order() << ".\n"
            << "The resulting plot may not yield much information.\n";
    }

    // Create file
    const auto file_name = poly.type() + "_Distribution.txt";
    std::ofstream file;
    file.open (file_name);

    // Print important polynomial data
    file << "File: " << file_name << "\n"
         << "Purpose: Contains data for a " << poly.type() << " series.\n"
         << num_points << " data points are present.\n"
         << "Data represents the series up to order = "
             << poly.order() << "\n\n";

    // Describe the polynomial
    poly.describe(file);

    // Generate header
    const std::string delimeter = ", ";
    std::string header = "x_sim" + delimeter + "x_std";
    for (unsigned int i = 0; i <= poly.order(); i++)
    {
        header += delimeter + "n = " + std::to_string(i);
    }

    // Print header
    file << std::fixed;
    file << std::setprecision(3);
    file <<  "\n\nThe data is as follows (delimeted by '"
         << delimeter << "'):\n\n";
    file << header << "\n";
    for (unsigned int i = 0; i < header.length(); i++)
    { file << "-"; }

    // Iterate over the domain bounds
    double sim_dom = poly.attr()->domain().lower();
    double std_dom = poly.standardize(sim_dom);
    std::vector<double> results(poly.order() + 1, 0.00);
    for (unsigned int i = 0; i <= num_points; i++)
    {

        // Evalute the polynomial to its default order
        results = poly.evaluate(std_dom);

        // Print results
        file << "\n" << sim_dom << ", " << std_dom;
        for (unsigned int j = 0; j < results.size(); j++)
        {
            file << ", " << results[j];
        }

        // Obtain the new standardized domain value
        sim_dom += increment_size;
        std_dom = poly.standardize(sim_dom);
    }
    file.close();

    // Now compare this data against the "accepted" solution
    //! \todo Validate against "accepted" solution (regression test)


    return;
  }

} // End of namespace TEST
} // End of namespace FET

//---------------------------------------------------------------------------//
// end of src/Polynomials/test/make_distribution.cc
//---------------------------------------------------------------------------//
