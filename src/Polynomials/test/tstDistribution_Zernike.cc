//---------------------------------*-C++-*-----------------------------------//
/*!
 * \file   src/Polynomials/test/tstDistribution_Zernike.cc
 * \brief  Tests the Zernike series distribution
 */
//---------------------------------------------------------------------------//

#include "Domain.hh"
#include "Zernike_Attr.hh"
#include "Zernike_Poly.hh"

#include "make_distribution.cc"

namespace FET
{
    namespace TEST
    {

        //! \brief Tests the Zernike series distribution
        void Zernike_Distribution()
        {
            // Create some domain for the basis set
            Domain<double> domain("x", -20.0, 20.0);

            // Create the attributes for the basis set
            Zernike_Attr attrs("A single Zernike polynomial", 15, domain);
            attrs.set_normalize(false);

            // Create the Legendre basis set now
            Zernike_Poly<> poly(attrs);

            // Now print the distribution
            make_distribution(poly);

            return;
        }

    } // End of namespace TEST
} // End of namespace FET


int main()
{
    FET::TEST::Zernike_Distribution();
    return 0;
}

//---------------------------------------------------------------------------//
// end of src/Polynomials/test/tstLegendre_Orders.cc
//---------------------------------------------------------------------------//
