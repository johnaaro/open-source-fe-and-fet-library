//---------------------------------*-C++-*-----------------------------------//
/*!
 * \file   src/Polynomials/test/tstDistribution_Legendre.cc
 * \brief  Tests the Legendre series distribution
 */
//---------------------------------------------------------------------------//

#include "Domain.hh"
#include "Legendre_Attr.hh"
#include "Legendre_Poly.hh"

#include "make_distribution.cc"

namespace FET
{
namespace TEST
{

  //! \brief Tests the Legendre series distribution
  void Legendre_Distribution()
  {
    // Create some domain for the basis set
    Domain<double> domain("x", -20.0, 20.0);

    // Create the attributes for the basis set
    Legendre_Attr attrs("A single Legendre polynomial", 15, domain);
    attrs.set_normalize(false);

    // Create the Legendre basis set now
    Legendre_Poly<> poly(attrs);

    // Now print the distribution
    make_distribution(poly);

    return;
  }

} // End of namespace TEST
} // End of namespace FET


int main()
{
    FET::TEST::Legendre_Distribution();
    return 0;
}

//---------------------------------------------------------------------------//
// end of src/Polynomials/test/tstLegendre_Orders.cc
//---------------------------------------------------------------------------//
