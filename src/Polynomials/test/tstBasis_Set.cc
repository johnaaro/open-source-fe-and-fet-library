//---------------------------------*-C++-*-----------------------------------//
/*!
 * \file   src/Polynomials/test/tstBasis_Set.cc
 * \brief  Basis_Poly class test.
 */
//---------------------------------------------------------------------------//

#include <stdexcept>

#include "Basis_Poly.hh"

#include "FET_Assertions.hh"
#include "Comparisons.hh"
#include "FET_DBC.hh"

#include "Basis_Attr.hh"
#include "Domain.hh"
#include "Legendre_Attr.hh"

namespace FET
{
namespace TEST
{
    //! \brief Constructs the basis polynomial
    Basis_Poly<> TEST_Basis_Poly_Constructor()
    {
        // Create a domain and some attributes
        const auto dom = Basis_Attr::domain_type("x", 0.0, 25.0);
        const Basis_Attr b_attr{"Some attr", 12, dom};
        const Legendre_Attr l_attr{"Some attr", 12, dom};

        // Construct the object of interest
        Basis_Poly<> poly{};

        // Ensure Basis poly attribute is the shared pointer
        poly = Basis_Poly<>(b_attr);
        Insist(poly.type() == b_attr.type(),
                "The shared pointer was not cast properly");
        poly.set_attribute(l_attr);
        Insist(poly.type() == l_attr.type(),
                "The shared pointer for Legendre was not cast properly");

        // Test attribute properties (tested already in attribute test)
        Insist(poly.order() == l_attr.order(),
                "The basis set order is not correct.");
        Insist(poly.order() + 1 == poly.num_terms(),
                "The basis set didn't calculate the number of terms "
                << "correctly.");

        return poly;
    }

    //! \brief Tests the evaluation interface of the basis polynomial
    void TEST_Basis_Poly_Evaluations(const Basis_Poly<>& poly)
    {
        // Standardization test
        const auto dom = poly.attr()->domain();
        const auto lower = poly.standardize(dom.lower());
        const auto upper = poly.standardize(dom.upper());
        Insist(lower == poly.attr()->ortho_domain().lower() &&
                upper == poly.attr()->ortho_domain().upper(),
                "Linear domain standardization failed.");

        // Try evaluate 1:
        auto foo = poly(lower);
        Insist(foo.size() == poly.degree() + 1,
                "Size of pre-processed data is not correct.");
        auto soo = poly(5, upper);
        Insist(soo.size() == 6,
                "Size of pre-processed data is not as specified.");
        try
        {
            auto boo = poly(1000.0);
            throw std::logic_error(
                    "Invalid valuation did not throw out of bounds error.");
        }
        catch (const assertion& e)
        { /* Worked properly... */ }

        // Try normalize:
        std::vector<double> coeffs(3, 1.0);
        const auto backup = coeffs;
        poly.normalize(coeffs);
        poly.ortho_normalize(coeffs);
        for (unsigned int i = 0; i < backup.size(); ++i)
        { Insist(is_approximate(backup[i], coeffs[i]),
                "Normalization pre-processing failed with element "
                << i << ".");
        }

        // Weight function
        Insist(is_approximate(poly.weight_fct(lower), 1.0),
                "Default weight function is not = 1.0.");

        // Integration (should return all zero-valued entries
        Insist(is_approximate(poly.integrate(lower, upper)[0]),
                "Integration pre-processing failed.");
        try
        {
            poly.integrate(lower, 1000.0);
            throw std::logic_error(
                    "Integration did not throw lower out of bounds error.");
        }
        catch (const assertion& e)
        { /* Worked properly... */ }
        try
        {
            poly.integrate(-1000.0, upper);
            throw std::logic_error(
                    "Integration did not throw upper out of bounds error.");
        }
        catch (const assertion& e)
        { /* Worked properly... */ }
    }


    //! \brief Tests the basis polynomial
    void TEST_Basis_Poly()
    {
        auto basis = TEST_Basis_Poly_Constructor();
        TEST_Basis_Poly_Evaluations(basis);
    }
} // End of namespace TEST
} // End of namespace FET


int main()
{
    FET::TEST::TEST_Basis_Poly();
    return 0;
}

//---------------------------------------------------------------------------//
// end of src/Polynomials/test/tstBasis_Set.cc
//---------------------------------------------------------------------------//
