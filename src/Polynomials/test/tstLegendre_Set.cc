//---------------------------------*-C++-*-----------------------------------//
/*!
 * \file   src/Polynomials/test/tstLegendre_Set.cc
 * \brief  Legendre_Poly class test.
 */
//---------------------------------------------------------------------------//

#include <assert.h>
#include <cmath>
#include <memory>

#include "Legendre_Poly.hh"

#include "FET_DBC.hh"

#include "Domain.hh"
#include "Legendre_Attr.hh"

namespace FET
{
namespace TEST
{
  //! \brief Tests the Legendre polynomial at 0.0 (centralized value)
  void TEST_Legendre_Poly_CENTRAL(const FET::Legendre_Poly<>& l_poly)
  {
    // Obtain a simulation and standardized domain:
    const double dom_center = (l_poly.attr()->domain().range() / 2.0) +
        l_poly.attr()->domain().lower();
    const double std_dom = l_poly.standardize(dom_center);

    // Ensure it's the center of the Legendre set's standardized domain
    assert((std_dom - 0.00) <= 1.0E-12);

    // Now test for various orders at a few locations
    const std::vector<double> act_res{
        1.0,          0.0, -0.5,          0.0,
        0.375,        0.0, -0.3125,       0.0,
        0.2734375,    0.0, -0.24609375,   0.0,
        0.2255859375, 0.0, -0.2094726562, 0.0
    };
    const std::vector<double> act_norm_res{
        0.5,          0.0, -1.25,         0.0,
        1.6875,       0.0, -2.03125,      0.0,
        2.32422,      0.0, -2.58398438,   0.0,
        2.81982,      0.0, -3.03735342,   0.0
    };
    const auto poly_res = l_poly.evaluate(std_dom);

    // Now let's try normalizing manually
    // NOTE: If attr()->normalize() == true, then the returned vector is
    // already normalized
    std::vector<double> norm_res = poly_res;
    l_poly.normalize(norm_res);

    // Test that values match for both standard and normalized results
    for (unsigned int i = 0; i <= l_poly.order(); i++)
    {
        assert((poly_res[i] - act_res[i]) < 1.0E-4);
        // assert((norm_res[i] - act_norm_res[i]) <= 1.0E-4);
    }
  }


  //! \brief Tests the Legendre polynomial at its bounds
  void TEST_Legendre_Poly_BOUNDS(const FET::Legendre_Poly<>& l_poly)
  {
    const auto lower_bound_res = l_poly.evaluate(-1.0);
    const auto upper_bound_res = l_poly.evaluate( 1.0);
    for (unsigned int i = 0; i <= l_poly.order(); i++)
    {
        assert((lower_bound_res[i] - std::pow(-1.0, i)) < 1.0E-12);
        assert((upper_bound_res[i] - 1.0) < 1.0E-12);
    }
  }


  //! \brief Tests the Legendre polynomial at a random point in the domain
  void TEST_Legendre_Poly_OTHER(const FET::Legendre_Poly<>& l_poly)
  {
    // Verify the values at X = 0.9 (on standardized domain)
    const std::vector<double> act_res{
         1,          0.9,        0.715,     0.4725,
         0.207938,  -0.0411412, -0.241164, -0.367825,
        -0.409686,  -0.36951,   -0.263146, -0.116213,
         0.0407487,  0.177801,   0.270773,  0.305198
    };
    const std::vector<double> act_norm_res{
         0.5,       1.35,      1.7875,   1.65375,
         0.935719, -0.226277, -1.56757, -2.75869,
        -3.48233,  -3.51035,  -2.76303, -1.33645,
         0.509359,  2.40031,   3.92621,  4.73056
    };

    // Evalute the polynomial now:
    const auto poly_res = l_poly.evaluate(0.90);
    auto norm_res = poly_res;
    l_poly.normalize(norm_res);

    // Now ensure valuese match known
    for (unsigned int i = 0; i <= l_poly.order(); i++)
    {
        assert((poly_res[i] - act_res[i]) <= 1.0E-4);
        // assert((norm_res[i] - act_norm_res[i]) <= 1.0E-4);
    }
  }


  //! \brief Tests the first few orders of the Legendre polynomial
  void TEST_Legendre_Poly_LOW_ORDER(const FET::Legendre_Poly<>& l_poly)
  {
    const std::vector<double> x_vals{
        -1.0, -0.9, -0.8, -0.7, -0.6, -0.5, -0.4, -0.3, -0.2, -0.1,
         0.0,  0.1,  0.2,  0.3,  0.4,  0.5,  0.6,  0.7,  0.8,  0.9, 1.0};

    // Check for expected values at each of the x-values analyzed above:
    for (unsigned int i = 0; i < x_vals.size(); i++)
    {
        // Obtain values at this x value
        auto results = l_poly.evaluate(x_vals[i]);

        // Now verify that expected values are obtained
        assert((results[0] - 1.0) < 1.0E-12);       // Order = 0
        assert((results[1] - x_vals[i]) < 1.0E-12); // Order = 1
        assert((results[2] - ((3 * pow(x_vals[i], 2) - 1) / 2.0))
                <= 1.0E-12);                        // Order = 2
        assert((results[3] - ((5 * pow(x_vals[i], 2) - 3) * x_vals[i] / 2.0))
                <= 1.0E-12);                        // Order = 3
    }
  }


  //! \brief Tests the \c integrate method
  void TEST_Legendre_Poly_Integrate(const FET::Legendre_Poly<>& l_poly)
  {
      const auto val = l_poly.integrate(-1.0, 1.0);
      Insist(2.0 == val[0], "0th-order Legendre integration failed");
      for (unsigned int i = 1; i <= 15; i++)
      {
          Insist(val[i] == 0.00,
                  "Legendre integration for order " << i << " failed (="
                  << val[i] << ").");
      }
      //! \todo Verify that order 16 /= 0 for integration
  }


  //! \brief Tests the basis polynomial where possible
  void TEST_Legendre_Poly()
  {
    // Create some domain for the basis set
    FET::Domain<double> domain("x", -20.0, 20.0);

    // Create the attributes for the basis set
    FET::Legendre_Attr attrs("Single Legendre basis", 15, domain);
    attrs.set_normalize(false);

    // Create the Legendre basis set now
    const FET::Legendre_Poly<> l_poly(attrs);
    Insist(l_poly.order() == 15, "Foo");

    // Test the bounds and the central value now:
    TEST_Legendre_Poly_CENTRAL(l_poly);
    TEST_Legendre_Poly_BOUNDS(l_poly);

    // Now test somewhere else
    TEST_Legendre_Poly_OTHER(l_poly);

    // Test first and second order over entire domain now
    TEST_Legendre_Poly_LOW_ORDER(l_poly);

    // Test integration now
    TEST_Legendre_Poly_Integrate(l_poly);
  }
} // End of namespace TEST
} // End of namespace FET


int main()
{
    FET::TEST::TEST_Legendre_Poly();
    return 0;
}

//---------------------------------------------------------------------------//
// end of src/Polynomials/test/tstLegendre_Set.cc
//---------------------------------------------------------------------------//
