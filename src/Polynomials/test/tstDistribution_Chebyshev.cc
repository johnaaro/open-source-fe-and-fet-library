//---------------------------------*-C++-*-----------------------------------//
/*!
 * \file   src/Polynomials/test/tstDistribution_Chebyshev.cc
 * \brief  Tests the Chebyshev series distribution
 */
//---------------------------------------------------------------------------//

#include "Domain.hh"
#include "Chebyshev_Attr.hh"
#include "Chebyshev_Poly.hh"

#include "make_distribution.cc"

namespace FET
{
namespace TEST
{

  //! \brief Tests the Chebyshev series distribution
  void Chebyshev_Distribution()
  {
    // Create some domain for the basis set
    Domain<double> domain("x", -20.0, 20.0);

    // Create the attributes for the basis set
    Chebyshev_Attr attrs("A single Chebyshev polynomial", 15, domain);
    attrs.set_normalize(false);

    // Create the Chebyshev basis set now
    Chebyshev_Poly<> poly(attrs);

    // Now print the distribution
    make_distribution(poly);

    return;
  }

} // End of namespace TEST
} // End of namespace FET


int main()
{
    FET::TEST::Chebyshev_Distribution();
    return 0;
}

//---------------------------------------------------------------------------//
// end of src/Polynomials/test/tstChebyshev_Orders.cc
//---------------------------------------------------------------------------//
