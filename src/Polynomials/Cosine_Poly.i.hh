//---------------------------------*-C++-*-----------------------------------//
/*!
 * \file   src/Polynomials/Cosine_Poly.i.hh
 * \brief  Cosine_Poly inline definitions.
 */
//---------------------------------------------------------------------------//
#ifndef src_Polynomials_Cosine_Poly_i_hh
#define src_Polynomials_Cosine_Poly_i_hh

#include "Cosine_Poly.hh"

#include <cmath>
#include <vector>

#include "Comparisons.hh"
#include "Constants.hh"

namespace FET
{

//---------------------------------------------------------------------------//
/*!
 * \brief Evaluates the series for all orders specified for a domain point
 *
 * Evaluates the polynomial to the requested order at a given simulation domain
 * value.
 *
 * The Cosine polynomials are calculated using a direct calculation, \f$
 * \psi_{n}(x) = cos(n x) \f$.
 */
template<typename T>
typename Cosine_Poly<T>::set_type
Cosine_Poly<T>::evaluate(
        const Cosine_Poly<T>::order_type order,
        const Cosine_Poly<T>::data_type      x) const
{
    // Pre-process and obtain empty set
    auto poly_vals = Cosine_Poly<T>::Base::evaluate(order, x);

    // Set non-recusive polynomials
    // Note: direct calculation is done since recursive tends to take longer,
    // perhaps due to the static_cast<data_type>
    switch (order)
    {
        default:
        case 20:
            poly_vals[20] = std::cos(20.0 * x);
        case 19:
            poly_vals[19] = std::cos(19.0 * x);
        case 18:
            poly_vals[18] = std::cos(18.0 * x);
        case 17:
            poly_vals[17] = std::cos(17.0 * x);
        case 16:
            poly_vals[16] = std::cos(16.0 * x);
        case 15:
            poly_vals[15] = std::cos(15.0 * x);
        case 14:
            poly_vals[14] = std::cos(14.0 * x);
        case 13:
            poly_vals[13] = std::cos(13.0 * x);
        case 12:
            poly_vals[12] = std::cos(12.0 * x);
        case 11:
            poly_vals[11] = std::cos(11.0 * x);
        case 10:
            poly_vals[10] = std::cos(10.0 * x);
        case  9:
            poly_vals[ 9] = std::cos( 9.0 * x);
        case  8:
            poly_vals[ 8] = std::cos( 8.0 * x);
        case  7:
            poly_vals[ 7] = std::cos( 7.0 * x);
        case  6:
            poly_vals[ 6] = std::cos( 6.0 * x);
        case  5:
            poly_vals[ 5] = std::cos( 5.0 * x);
        case  4:
            poly_vals[ 4] = std::cos( 4.0 * x);
        case  3:
            poly_vals[ 3] = std::cos( 3.0 * x);
        case  2:
            poly_vals[ 2] = std::cos( 2.0 * x);
        case  1:
            poly_vals[ 1] = std::cos(x);
        case  0:
            poly_vals[ 0] = 1.0;
    }

    // Recursively determine higher order terms
    for (Cosine_Poly<T>::order_type n = 2; n <= order; ++n)
    {
        poly_vals[n] = std::cos(static_cast<Cosine_Poly<T>::data_type>(n) * x);
    }

    // Normalize, is requested
    if (Cosine_Poly<T>::attr()->normalize())
    {
        normalize(poly_vals);
    }

    // Return the polynomial set
    return poly_vals;
}


//---------------------------------------------------------------------------//
/*!
 * \brief Normalizes the provided vector by order .i.hhording to its index
 *
 * The Cosine polynomial order's normalization constant is determined as the
 * inverse of the l-2 norm (i.e. integration of the square of the basis
 * function of that order over the orthonormal domain). Normalization is done
 * on the given vector by multiplying it by the inverse of the l-2 norm.
 *
 * The normalization coefficient for a given order \f$ n \geq 1 \f$ is
 * calculated as follows:
 * \f[
 * c_{\psi_{n}}
 *      = (\int_{a}^{b} \psi_{n}(z)^{2} dz)^{-1}
 *      = (\frac{b - a}{2} + \frac{sin(2 n b) - sin(2 n a)}{4 n})^{-1}
 * \f]
 *
 * For \f$ n = 0 \f$, \f$ c_{\psi_{n}} = b - a \f$.
 *
 * Applied over the domain \f$ [-\pi, \pi] \f$, the above simplifies to
 * \f$ 1 / \pi \f$ for \f$ n \geq 1 \f$ and \f$ 1 / (2 \pi) \f$ for
 * \f$ n = 0 \f$.
 */
 //---------------------------------------------------------------------------//
template<typename T>
void Cosine_Poly<T>::normalize(Cosine_Poly<T>::set_type& poly_vals)
    const noexcept
{
    Cosine_Poly<T>::Base::normalize(poly_vals);
    if (poly_vals.size() == 0) return;

    const auto dom = Cosine_Poly<T>::attr()->domain();
    poly_vals[0] /= dom.range();
    for (Cosine_Poly<T>::order_type i = 1; i < poly_vals.size(); ++i)
    {
        auto order = static_cast<Cosine_Poly<T>::data_type>(i);
        poly_vals[i] /= (dom.range() / 2.0) +
            (std::sin(2.0 * order * dom.upper()) -
               std::sin(2.0 * order * dom.lower())) /
            (4.0 * order);
    }
}


//---------------------------------------------------------------------------//
/*!
 * \brief Normalizes the polynomials on the polynomial's orthogonal  domain
 *
 * Normalizes the argument, according to the respective order, based on the \f$
 * l^2 \f$ norm of the polynomial on the orthogonal domain.
 * \see normalize
 */
template<typename T>
void Cosine_Poly<T>::ortho_normalize(Cosine_Poly<T>::set_type& poly_vals)
    const noexcept
{
    Cosine_Poly<T>::Base::ortho_normalize(poly_vals);
    if (poly_vals.size() == 0) return;

    poly_vals[0] /= 2.0 * pi;
    for (typename Cosine_Poly<T>::order_type i = 1;
            i < poly_vals.size(); ++i)
    {
        poly_vals[i] /= pi;
    }
}


//---------------------------------------------------------------------------//
/*!
 * \brief Integrates the basis set on a given standardized domain
 *
 * Integrates the Cosine basis set on a given standardized domain. The
 * returned vector is notated as integrated_value[order], i.e. the element
 * index in the vector is representative of the integrated value at that order.
 *
 * The Cosine polynomial integration is implemented using the following
 * formula:
 * \f[
 * \int_{a}^{b} \psi_{n}(r) \rho(r) dr =
 *    = \frac{1}{n} [sin(n b) - sin(n a)]
 * \f]
 *
 * The special case of \f$ n = 0 \f$ is treated separately, having an integral
 * \f$ = b - a \f$.
 */
 //---------------------------------------------------------------------------//
template<typename T>
typename Cosine_Poly<T>::set_type
Cosine_Poly<T>::integrate(
        const Cosine_Poly<T>::data_type lower_b,
        const Cosine_Poly<T>::data_type upper_b,
        const Cosine_Poly<T>::order_type  order) const
{
    // Pre-process and obtain default
    auto result = Cosine_Poly<T>::Base::integrate(lower_b, upper_b, order);
    if (is_approximate(upper_b, lower_b)) return result;

    // Set integrated results
    result[0] = upper_b - lower_b;
    for (Cosine_Poly<T>::order_type i = 1; i <= order; ++i)
    {
        const auto n = static_cast<double>(i);
        result[i] = (std::sin(n * upper_b) - std::sin(n * lower_b)) /
            n;
    }

    // Return the vector
    return result;
}


//---------------------------------------------------------------------------//
} // namespace FET

//---------------------------------------------------------------------------//
#endif // src_Polynomials_Cosine_Poly_i_hh
//---------------------------------------------------------------------------//
// end of src/Polynomials/Cosine_Poly.i.hh
//---------------------------------------------------------------------------//
