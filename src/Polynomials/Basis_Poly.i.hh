//---------------------------------*-C++-*-----------------------------------//
/*!
 * \file   src/Polynomials/Basis_Poly.i.hh
 * \brief  Basis_Poly class specializations
 */
//---------------------------------------------------------------------------//
#ifndef src_Polynomials_Basis_Poly_i_hh
#define src_Polynomials_Basis_Poly_i_hh

#include <iostream>

#include "FET_Macros.hh"
#include "FET_DBC.hh"
#include "Basis_Poly.hh"

namespace FET
{

//---------------------------------------------------------------------------//
/*!
 * \brief Linearly standardizes input to the basis set's domain
 * \param val The evaluation position \f$ x \f$ in the simulation domain
 * \return A linearly scaled value in the polynomial's orthogonal domain.
 *
 * Standardizes a provided simulation domain value onto the domain of the basis
 * set via a simple linear transformation of the simulation domain onto the
 * orthogonal domain. The linear standardization occurs as follows:
 * \f[
 * x' = (\frac{x'_{max} - x'_{min}}{x_{max} - x_{min}}) (x - x_{min}) +
 *      x'_{min}
 * \f]
 *
 * Where primed variables \f$ x' \f$ are those on the standard domain and
 * unprimed variables \f$ x \f$ those on the simulation domain.
 */
template<typename T>
typename Basis_Poly<T>::data_type
Basis_Poly<T>::standardize(Basis_Poly<T>::data_type val) const noexcept
{
    // If outside the domain, use the bound nearest
    if (!attr()->domain().contains(val))
    {
        if (val < attr()->domain().lower())
        {
             val = attr()->domain().lower();
        }
        else
        {
            val = attr()->domain().upper();
        }
    }

//    return (2 * (val - attr()->domain().lower()) / (attr()->domain().upper() - attr()->domain().lower())) - 1.0;

    return (attr()->ortho_domain().range() / attr()->domain().range()) *
        (val - attr()->domain().lower()) + attr()->ortho_domain().lower();
}


//---------------------------------------------------------------------------//
/*!
 * \brief Provides base functionality for all polynomial evaluations
 * \param order The degree, or order, of the polynomial
 * \param x     The evaluation position for \f$ \psi_{n}(x) \f$.
 * \return A list of polynomial values up to degree 'order'
 *
 * The primary function used for evaluating the polynomial at any degree.
 *
 * Here, provides pre-processing of input data and returns a zero-valued vector
 * containing the number of elements respective to the degree of the
 * polynomial. i.e., size  order + 1
 */
template<typename T>
typename Basis_Poly<T>::set_type
Basis_Poly<T>::evaluate(const Basis_Poly<T>::order_type order,
                        const Basis_Poly<T>::data_type      x) const
{
//TODO::DEBUG statement remove later
//std::cout << "polynomial of type " << this->type() << ".evaluate() called successfully...\n";

    // Ensure value is contained in the domain
    Require(contains(x));

    // Ignore the arguments (for DEBUG compilation)
    Basis_Poly<T>::set_type results(order + 1, 0.0);
    return results;
}


//---------------------------------------------------------------------------//
/*!
 * \brief Normalizes the provided data on the simulation domain
 * \param poly_val A set of values that will be normalized
 * \return Nothing; altars the provided dataset in-place.
 *
 * Normalizes the provided dataset according to the l-2 Norm of the polynomial
 * on its simulation domain \f$ s \in [s_{l}, s_{h}] \f$. Mathematically,
 * this is the inner product:
 * \f[
 * c_{\psi_{i}} = \int \limits_{s_{l}}^{s_{h}} \psi_{i}(s)^{2} ds
 * \f]
 *
 * Performs any data pre-processing. Presently, none is required.
 */
template<typename T>
void Basis_Poly<T>::normalize(Basis_Poly<T>::set_type& poly_val) const noexcept
{
    return
    FET_IGNORE(poly_val);
}


//---------------------------------------------------------------------------//
/*!
 * \brief Normalizes the polynomials on the polynomial's orthogonal domain
 * \param poly_val A set of values that will be ortho-normalized
 * \return Nothing; altars the provided dataset in-place.
 *
 * Normalizes the provided dataset according to the l-2 Norm of the polynomial
 * on its orthogonal domain.
 *
 * This method here defines all pre-processing of the data provided.
 * \see normalize
 */
template<typename T>
void Basis_Poly<T>::ortho_normalize(
        Basis_Poly<T>::set_type& poly_val) const noexcept
{
    return;
    FET_IGNORE(poly_val);
}


template<typename T>
double Basis_Poly<T>::orthonorm_const(int order) const noexcept
{
    //placeholder to remove unused parameter warning
    //TODO::return proper orthonorm constant for provided order
    order++;

    return 1;
//    FET_IGNORE(poly_val);
}

//---------------------------------------------------------------------------//
/*!
 * \brief Returns the value of the weighting function for the basis set
 * \param value The evaluation position of \f$ \rho(x) \f$.
 * \return The weighting function for that polynomial, \f$ \rho(x) \f$
 *
 * Evaluates the polynomial's weighting function \f$ \rho_{i}(x) \f$.
 *
 * Many polynomials utilize a value of \f$ \rho(x) = 1 \forall x \f$. This
 * method provides a default implementation for such common polynomials.
 */
template<typename T>
typename Basis_Poly<T>::data_type
Basis_Poly<T>::weight_fct(const Basis_Poly<T>::data_type value) const noexcept
{
    return 1.0;
    FET_IGNORE(value);
}


//---------------------------------------------------------------------------//
/*!
 * \brief Integrates the basis set and weighting function on a given interval
 * \param lower_b A standardized lower bound of the integrand
 * \param upper_b A standardized upper bound of the integrand
 * \return A list of integrated data points on \f$ [lower, upper] \f$ to the
 * prescribed polynomial degree.
 *
 * Performs an integration of the polynomial on the specified range and returns
 * the set of values at all polynomial orders utilized:
 * \f[
 *  I_{n} = \int \limits_{a}^{b} \rho_{n}(x) \psi_{n}(x) dx
 * \f]
 *
 * with
 *
 * \f[
 * I = \{ I_{0}, I_{1}, \ldots, I_{n} \}
 * \f]
 *
 * This implementation simply performs pre-processing checks and returns a
 * zero-valued dataset.
 */
template<typename T>
typename Basis_Poly<T>::set_type
Basis_Poly<T>::integrate(
        const Basis_Poly<T>::data_type lower_b,
        const Basis_Poly<T>::data_type upper_b,
        const Basis_Poly<T>::order_type  order) const
{
//TODO::DEBUG statement remove later
//std::cout << "polynomial of type " << this->type() << ".integrate() called successfully...\n";

    // Show API requirements
    Require(attr()->ortho_domain().contains(lower_b) &&
            attr()->ortho_domain().contains(upper_b));
    return Basis_Poly<T>::set_type(order + 1, 0.0);
}


//---------------------------------------------------------------------------//
/*!
 * \brief Tests for equality of the basis sets via the attributes
 */
template<typename T>
bool Basis_Poly<T>::operator==(const Basis_Poly<T>& poly) const noexcept
{
    // Check if attributes are the same
    if (!(attr() == poly.attr())) return false;
    return true;
}


//---------------------------------------------------------------------------//
} // namespace FET

#endif // src/Polynomials/Basis_Poly_i_hh
//---------------------------------------------------------------------------//
// end of src/Polynomials/Basis_Poly.i.hh
//---------------------------------------------------------------------------//
