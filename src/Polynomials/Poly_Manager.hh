//---------------------------------------------------------------------------//
/*!
 * \file  src/Polynomials/Poly_Manager.hh
 * \brief Poly_Manager class declaration.
 */
//---------------------------------------------------------------------------//
#ifndef src_Polynomials_Poly_Manager_hh
#define src_Polynomials_Poly_Manager_hh

#include <iostream>
#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

#include "FET_DBC.hh"
#include "Basis_Poly.hh"

namespace FET
{
//===========================================================================//
/*!
 * \class Poly_Manager
 * \brief General manager object for a set of polynomials
 *
 * Manages an arbitrary set of polynomial objects. Provides a simple and
 * efficient interface that may be used to fetch polynomials when needed.
 */
/*!
 * \example src/Polynomial/test/tstPoly_Manager.cc
 *
 * Test of the Poly_Manager object
 */
//===========================================================================//
template<class T = Basis_Poly<double>>
class Poly_Manager
{
public:

    //@{
    //! Public type aliases
    using Base             = T;
    using SP_Base          = std::shared_ptr<Base>;
    using key_type         = std::string;
    using index_type       = unsigned int;
    using Cont_SP_Base     = std::unordered_map<key_type, SP_Base>;
    //@}

    //! The default validation state
    static constexpr bool default_validation = true;

    //! Valid domains
    static const std::vector<std::string> valid_domains;


private:
    // >>> IMPLEMENTATION DATA

    //! \brief Container of <key, polynomial> items
    Cont_SP_Base d_base;

    //! \brief Indexed reference for the keys of the polynomials
    std::vector<key_type> d_labels;

    //! \brief Flags if polynomials are to be validated prior to insertion
    bool b_validate = default_validation;


public:
    // >>> CONSTRUCTOR/DESTRUCTORS

    //! Default Constructor
    Poly_Manager(
            const bool validate = default_validation,
            const std::vector<SP_Base>& items = {})
        : b_validate(validate)
    {
        reset();
        insert(items);
    }

    //@{
    //! Copy control (default, copy, move, and destructor)
    Poly_Manager(const Poly_Manager&) = default;
    Poly_Manager& operator=(const Poly_Manager&) = default;
    Poly_Manager(Poly_Manager&&) = default;
    Poly_Manager& operator=(Poly_Manager&&) = default;
    virtual ~Poly_Manager() = default;
    //@}


public:
    // >>> GETTERS

    //! Returns if polynomials are validated prior to insertion
    bool validate() const noexcept
    { return b_validate; }

    //! Returns the container of polynomials
    const Cont_SP_Base& polynomials() const noexcept
    { return d_base; }

    //! Returns the indexed reference vector
    const std::vector<key_type>& labels() const noexcept
    { return d_labels; }

    //! Returns the number of polynomials present
    long unsigned int size() const noexcept
    { return polynomials().size(); }

    //! Returns a boolean flagging if any polynomials are present
    bool empty() const noexcept
    { return polynomials().empty(); }

    //! Returns a polynomial with the associated label
    SP_Base polynomial(const key_type& key) const
    {
        Require(contains(key));
        return polynomials().at(key);
    }

    //! Alias for returning a polynomial with the associated label
    SP_Base basis(const key_type& key) const
    { return polynomial(key); }

    //! Returns a polynomial based on index it was added on
    SP_Base polynomial(const index_type label) const
    {
        Require(contains(label));
        return polynomial(labels()[label]);
    }

    //! Alias for returning a polynomial based on index
    SP_Base basis(const index_type index) const
    { return polynomial(index); }

    //! Alias for returning a polynomial based on key
    SP_Base operator[](const key_type& key) const
    { return polynomial(key); }

    //! Alias for returning a polynomial based on index
    SP_Base operator[](const index_type index) const
    { return polynomial(index); }


public:
    // >>> SETTERS

    //! Removes all polynomial objects stored
    void reset() noexcept;

    //! Sets if the polynomials are to be validated prior to insertion
    void set_validate(const bool validate) noexcept
    { b_validate = validate; }

    //! Inserts a single polynomial
    void insert(SP_Base);

    //! Inserts a list of polynomials
    void insert(const std::vector<SP_Base>&);

    //! Erases an element with a specified key if it exists
    void erase(const key_type&);

    //! Erases the element with a specified index if it exists
    void erase(const index_type);


public:
    // >>> INTROSPECTION

    //! \brief Indicates if a polynomial has a valid constrained domain label
    bool is_valid(const key_type&) const noexcept;

    //! Returns true/false regarding if a polynomial label exists
    bool contains(const key_type& key) const noexcept
    { return (polynomials().find(key) != polynomials().end()); }

    //! Returns true/false if a polynomial with specific index exists
    bool contains(const index_type index) const noexcept
    { return (index < labels().size()); }

    //! \brief Returns the volume occupied by the polynomial domains
    double volume() const noexcept;

    //! \brief Describes the object
    virtual void describe(std::ostream& = std::cout) const noexcept;

}; // end class Poly_Manager


//! Valid domains
template<typename T>
const std::vector<std::string> Poly_Manager<T>::valid_domains = {
    "x", "y", "z", "r", "theta", "phi",   // 1-D domains
    "r, theta", "theta, phi",             // 2-D domains
    "r, theta, phi"                       // 3-D domains
};

//---------------------------------------------------------------------------//
} // end namespace FET

//---------------------------------------------------------------------------//
// Inline and Template instantiations
//---------------------------------------------------------------------------//

#include "Poly_Manager.i.hh"
#include "Poly_Manager.t.hh"

//---------------------------------------------------------------------------//
#endif // src_Polynomials_Poly_Manager_hh
//---------------------------------------------------------------------------//
// end of src/Polynomials/Poly_Manager.hh
//---------------------------------------------------------------------------//
