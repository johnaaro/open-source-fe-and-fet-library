//---------------------------------------------------------------------------//
/*!
 * \file  src/core/Domain.hh
 * \brief Domain class declaration.
 */
//---------------------------------------------------------------------------//
#ifndef src_core_Domain_hh
#define src_core_Domain_hh

#include <iostream>
#include <string>
#include <cmath>

#include "Constants.hh"
#include "FET_DBC.hh"
#include "Name.hh"

namespace FET
{
//===========================================================================//
/*!
 * \class Domain
 * \brief Specifies a sub-set of a desired dimension
 *
 * The Domain class is used by the FET library to notate the extent that a
 * Basis_Poly object, or any of its child objects, applies within a given
 * dimension. The Domain specifies the lower and upper bounds of a stated
 * dimension that an object applies over.
 *
 * Within the FET library, this applies mainly to basis sets, specifying the
 * range over which the basis set applies.
 *
 * Note: If using non-standard data types (int, float, double), the object used
 * requires a division operator (/), a subtraction operator (-), an equality
 * operator (==), and a comparison operator (<=, >=).
 *
 */
/*!
 * \example test/tstDomain.cc
 *
 * Test of the Domain object
 */
//===========================================================================//
template<class T = double>
class Domain : public Name
{

public:
    // >>> PUBLIC TYPE ALIASES
    //@{
    //! Public type aliases
    using Base       = Name;
    using value_type = T;
    //@}


private:
    // >>> IMPLEMENTATION DATA

    //! \brief Beginning of the range
    T d_lower;

    //! \brief End of the domain
    T d_upper;


public:
    // >>> CONSTRUCTORS

    //! \brief Primary templated Domain constructor
    template<typename U>
    Domain(const std::string& label, const U lower, const U upper)
        : Base(label)
    {
        Insist(lower <= upper,
               "The Domain's lower bound must be less than " <<
               "or equal to the upper bound.");
        set_lower(lower);
        set_upper(upper);
    }

    //@{
    //! Copy control (default, copy, move, and destructor)
    Domain() = default;
    Domain(const Domain&) = default;
    Domain& operator=(const Domain&) = default;
    Domain(Domain&&) = default;
    Domain& operator=(Domain&&) = default;
    virtual ~Domain() = default;
    //@}


private:
    // >>> SETTERS

    //! \brief Sets the lower value of the Domain
    template<typename U>
    void set_lower(const U lower) noexcept
    { d_lower = static_cast<value_type>(lower); }

    //! \brief Sets the upper value of the Domain
    template<typename U>
    void set_upper(const U upper) noexcept
    { d_upper = static_cast<value_type>(upper); }


public:
    // >>> GETTTERS

    //! \brief Returns the lower limit
    const T lower() const noexcept
    { return d_lower; }

    //! \brief Returns the upper limit
    const T upper() const noexcept
    { return d_upper; }

    //! \brief Returns the lower (<=0) or upper bound (>0) in a vector-like way
    const T operator()(const int index) const noexcept
    { return (index <= 0 ? lower() : upper()); }

    bool operator==(const Domain<T>& dom) const noexcept
    {
        return (label() == dom.label() &&
         std::abs(lower() - dom.lower()) < tolerance &&
         std::abs(upper() - dom.upper()) < tolerance
         ? true : false);
    }


public:
    // >>> INTROSPECTION

    //! \brief Returns the range of the values
    const T range() const noexcept
    { //return (upper() - lower());
      return (d_upper-d_lower);
    }

    //! \brief Returns the central value of the domain
    const T center() const noexcept
    { return (range() / T(2.0) + lower()); }

    //! \brief Returns if a provided value is contained by the Domain
    bool contains(const T val) const noexcept
    { return ((lower() <= val && val <= upper()) ? true : false); }

    //! \brief Describes the object to an output stream
    void describe(std::ostream os) const noexcept;

    //! \brief Describes the object inline (<<)
    void describe_inline(std::ostream os) const noexcept;


}; // end class Domain

//---------------------------------------------------------------------------//
} // end namespace FET

#include "Domain.i.hh"

//---------------------------------------------------------------------------//
#endif // src_core_Domain_hh
//---------------------------------------------------------------------------//
// end of src/core/Domain.hh
//---------------------------------------------------------------------------//
