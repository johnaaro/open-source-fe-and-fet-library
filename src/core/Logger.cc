//---------------------------------*-C++-*-----------------------------------//
/*!
 * \file   src/core/Logger.cc
 * \author Seth R Johnson
 * \date   Tue Feb 03 08:39:27 2015
 * \brief  Logger class definitions.
 */
//---------------------------------------------------------------------------//

#include <iostream>
#include <string>
#include <utility>

#include "Logger.hh"
#include "FET_DBC.hh"

//---------------------------------------------------------------------------//
// ANONYMOUS HELPER FUNCTIONS
//---------------------------------------------------------------------------//
/*!
 * \brief Allows any namespace to use the \c Logger easily
 */
//namespace
//{
//! \brief Custom deleter so that a regular pointer can be wrapped in an SP
//struct null_deleter
//{
//    void operator()(const void*)
//    { /* * */ }
//};
//} // end anonymous namespace


namespace FET
{
constexpr const char* const FET::Logger::s_log_prefix[END_LOG_LEVEL];
//---------------------------------------------------------------------------//
// STATIC DATA
//---------------------------------------------------------------------------//
/*const char* Logger::s_log_prefix[END_LOG_LEVEL] = {
    "      ", // DEBUG
    "      ", // DIAGNOSTIC
    ":::   ", // STATUS
    ">>>   ", // INFO
    "***   ", // WARNING
    "\n!!!   ", // ERROR
    "\n!*!*! ", // CRITICAL
};
*/
//---------------------------------------------------------------------------//
// LOGGER
//---------------------------------------------------------------------------//
/*!
 * \brief Constructor.
 */
/*Logger::Logger()
    : d_local_level(DIAGNOSTIC)
    , d_global_level(DIAGNOSTIC)
    // , d_node(FET::node())
    , d_node(0)
    , d_screen_output("screen", DEBUG)
    , d_file_output("file",     END_LOG_LEVEL)
{
    // Default screen output is cerr
    d_screen_output.stream_ptr.reset(&std::cerr, null_deleter());
}
*/

//---------------------------------------------------------------------------//
/*!
 * \brief Set verbosity level for local log calls.
 */
/*void Logger::set_local_level(Log_Level level)
{
    Require(0 <= level && level < END_LOG_LEVEL);
    d_local_level = level;
}
*/

//---------------------------------------------------------------------------//
/*!
 * \brief Set verbosity level for global log calls.
 */
/*void Logger::set_global_level(Log_Level level)
{
    Require(0 <= level && level < END_LOG_LEVEL);
    d_global_level = level;
}
*/

//---------------------------------------------------------------------------//
/*!
 * \brief Set an output handle
 *
 * \warning This is UNSAFE except with global ostreams such as cout!
 *
 * Since the Logger is global, it will almost certainly exceed the scope of any
 * local stream, leading to dereferencing of deallocated data.
 *
 * If you absolutely must give a raw pointer here, make *sure* to call
 * \c remove() on it when the pointer's reference is destroyed.
 */
/*void Logger::set(const std::string &key,
                 ostream_t         *stream_ptr,
                 Log_Level          min_level)
{
    Require(stream_ptr);
    Require(0 <= min_level && min_level < END_LOG_LEVEL);

    Sink& sink = find(key);

    sink.name = key;
    sink.level = min_level;
    sink.stream_ptr = SP_ostream(stream_ptr, null_deleter());
}
*/

//---------------------------------------------------------------------------//
/*!
 * \brief Set an output handle from a shared pointer.
 *
 * This is the preferred way to set a logger handle because it has
 * reference-counted semantics.
 *
 * Note that because of static initialization order, it's generally bad to
 * have logger output during destructors of other static objects -- this logger
 * instance could be deleted before those other objects.
 */
/*void Logger::set(const std::string &key,
                 SP_ostream         stream_sp,
                 Log_Level          min_level)
{
    Require(stream_sp);
    Require(0 <= min_level && min_level < END_LOG_LEVEL);

    Sink& sink = find(key);

    sink.name = key;
    sink.level = min_level;
    sink.stream_ptr = stream_sp;
}
*/

//---------------------------------------------------------------------------//
/*!
 * \brief Remove an output handle.
 */
/*void Logger::remove(const std::string &key)
{
    Sink& sink = find(key);
    sink.stream_ptr.reset();
}
*/

//---------------------------------------------------------------------------//
// ACCESSORS
//---------------------------------------------------------------------------//
/*!
 * \brief Return a stream appropriate to the level for node-zero output
 */
/*Logger_Statement Logger::global_stream(Log_Level level)
{
    Require(0 <= level && level < END_LOG_LEVEL);

    Logger_Statement::Vec_Ostream streams;

    // Only add streams on node zero
    if (d_node == 0 && level >= d_global_level)
    {
        streams = build_streams(level);
    }

    // Create the logger statement (moving the vec streams for efficiency)
    Logger_Statement result(std::move(streams));

    // Pipe prefix to the stream before returning
    result << s_log_prefix[level];

    // Return the expiring logger_statement, implicit move
    return result;
}
*/

//---------------------------------------------------------------------------//
/*!
 * \brief Return a stream appropriate to the level for local-node output
 */
/*Logger_Statement Logger::local_stream(Log_Level level)
{
    Require(0 <= level && level < END_LOG_LEVEL);

    Logger_Statement::Vec_Ostream streams;

    if (level >= d_local_level)
    {
        streams = build_streams(level);
    }

    // Create the logger statement (moving the vec streams for efficiency)
    Logger_Statement result(std::move(streams));

    // Pipe prefix to the stream before returning
    result << s_log_prefix[level];

    // Return the expiring logger_statement, implicit move
    return result;
}
*/

//---------------------------------------------------------------------------//
/*!
 * \brief Remove an output handle.
 */
/*Logger::Sink& Logger::find(const std::string &key)
{
    if (key == "screen")
    {
        return d_screen_output;
    }
    else if (key == "file")
    {
        return d_file_output;
    }
    else
    {
        Validate(false, "Currently only screen and file are supported "
                 "log keys; '" << key << "' is invalid.");
    }

    // Squelch compiler errors
    return find(key);
}
*/

//---------------------------------------------------------------------------//
/*!
 * \brief Build output streams based on the given level
 */
/*Logger::Vec_Ostream Logger::build_streams(Log_Level level) const
{
    Require(0 <= level && level < END_LOG_LEVEL);
    Vec_Ostream streams;

    for (const Sink* s : {&d_screen_output, &d_file_output})
    {
        Check(s);
        if (s->stream_ptr && (level >= s->level))
        {
            streams.push_back(s->stream_ptr.get());
        }
    }
    return streams;
}
*/

//---------------------------------------------------------------------------//
// STATIC METHODS
//---------------------------------------------------------------------------//
/*!
 * \brief Access global logging instance.
 *
 * \warning This should be accessed after MPI is initialized because we call
 * MPI_rank.
 */
/*Logger& Logger::get()
{
    static Logger s_instance;
    Check(std::end(s_log_prefix) - std::begin(s_log_prefix)
          == END_LOG_LEVEL);
    return s_instance;
}
*/

//---------------------------------------------------------------------------//
} // end namespace FET

//---------------------------------------------------------------------------//
// end of src/core/Logger.cc
//---------------------------------------------------------------------------//
