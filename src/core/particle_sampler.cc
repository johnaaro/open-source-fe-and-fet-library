//---------------------------------------------------------------------------//
/*!
 * \file  src/core/particle_sampler.cc
 * \brief Implementation of sampling a characteristic of a particle object
 */
//---------------------------------------------------------------------------//

#include <math.h>

#include <chrono>
#include <functional>
#include <random>
#include <vector>

#include "Particle.hh"

namespace FET
{

// enum is to create switchable values for expected string inputs
enum string_code
{
  eINVALID,
  eX_POS,
  eY_POS,
  eZ_POS,
  eX_DIR,
  eY_DIR,
  eZ_DIR,
  eSTEP,
  eENERGY,
  eWEIGHT,
  eCROSS_SECTION
};

// convert_string function converts string inputs to switchable enum value
string_code convert_string(std::string const& str)
{
  if(str == "x_pos")  return eX_POS;
  if(str == "y_pos")  return eY_POS;
  if(str == "z_pos")  return eZ_POS;
  if(str == "x_dir")  return eX_DIR;
  if(str == "y_dir")  return eY_DIR;
  if(str == "z_dir")  return eZ_DIR;
  if(str == "step")   return eSTEP;
  if(str == "energy") return eENERGY;
  if(str == "weight") return eWEIGHT;
  if(str == "cross_section") return eCROSS_SECTION;
  else return eINVALID;
}

//---------------------------------------------------------------------------//
/*!
 * \brief Randomly samples a given particle characteristic
 *
 * Samples a particle characteristic randomly given a function handle to a
 * function that samples a known distribution.
 *
 * An example of a valid function handler is:
 *    For f(x) = sin(x), the fct_handler is:
 *       sampled_value = asin(2 * rndm - 1);
 *
 * \todo Let Aaron doctor it up :)
 * \todo Make a sampler class to more easily sample a function..?
 *       (contains RNG, fct_handler, num RNs required, description, etc.)
 */
void sample_particles(
    std::vector<Particle<>>& particles,
    const std::string& item,
    std::function<double(std::vector<double>)> fct_handler)
{
    // Initialize the random number generator
    // \todo Make RNG initialize randomly so multiple samplings do NOT use
    //       the same RNG sequence (perhaps base the initial seed on the time)
    auto SEED = std::chrono::duration_cast<std::chrono::milliseconds>(
            std::chrono::system_clock::now().time_since_epoch()).count();
    std::default_random_engine generator(SEED);
    std::uniform_real_distribution<double> distribution(0,1);

    // Sample the characteristic
    for (unsigned int i = 0; i < particles.size(); i++)
    {
        // Generate some random numbers (the number needed for the CDF handler)
        // ...
        std::vector<double> rndms;
        //TODO:: j<1 temporary, only using while explicitly sampling x over sin(x)
        for(int j = 0; j < 1; j++)
        {
          rndms.emplace_back(distribution(generator));
        }

        // Sample the CDF now (give std::vector<double>(random numbers required}))
        // e.g.
        // double sampled_value = fct_handler({one_random_number})
        //    w/
        // fct_handler(std::vector<double> rndms) = asin(2 * rndms(0) - 1);
        double sampled_value = fct_handler(rndms);

        // Now modify the current particle with the modified value
        // \todo This does NOT work since C++ does switch/case with integers, NOT strings - needs adjusting
        switch (convert_string(item))
        {
            case eX_POS:
                particles[i].set_position({sampled_value, particles[i].position()[1], particles[i].position()[2]});
                break;
            case eY_POS:
                particles[i].set_position({particles[i].position()[0], sampled_value, particles[i].position()[2]});
                break;
            case eZ_POS:
                particles[i].set_position({particles[i].position()[0], particles[i].position()[1], sampled_value});
                break;
            case eX_DIR:
                particles[i].set_direction({sampled_value, particles[i].dir()[1], particles[i].dir()[2]});
                break;
            case eY_DIR:
                particles[i].set_direction({particles[i].dir()[0], sampled_value, particles[i].dir()[2]});
                break;
            case eZ_DIR:
                particles[i].set_direction({particles[i].dir()[0], particles[i].dir()[1], sampled_value});
                break;
            case eSTEP:
                particles[i].set_step(sampled_value);
                break;
            case eENERGY:
                particles[i].set_energy(sampled_value);
                break;
            case eWEIGHT:
                particles[i].set_weight(sampled_value);
                break;
            case eCROSS_SECTION:
                particles[i].set_total_cross_section(sampled_value);
                break;
            default:
                throw std::invalid_argument("Cannot sample an invalid particle characteristic.");
                break;
        }
    }
}

//---------------------------------------------------------------------------//
} // end namespace FET

//---------------------------------------------------------------------------//
// end of src/core/Particle/Particle.cc
//---------------------------------------------------------------------------//
