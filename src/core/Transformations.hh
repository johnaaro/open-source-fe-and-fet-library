//---------------------------------*-C++-*-----------------------------------//
/*!
 * \file   src/core/Transformations.hh
 * \brief  Declares a set of transformations
 */
//---------------------------------------------------------------------------//
#ifndef src_core_Transformations_hh
#define src_core_Transformations_hh

#include <cmath>
#include <utility>

#include "Constants.hh"
#include "FET_DBC.hh"

namespace FET
{

namespace Geometry
{


//===========================================================================//
/*!
 * \brief Obtains the Euclidean distance of (x, y)
 *
 * Obtains and returns a radial distance, from \f$(0, 0[, 0])\f$, to that
 * provided \f$(x, y)\f$. This is useful for polar coordinate transformations
 * and other purposes.
 *
 * This function uses the following formula:
 * \f$ \rho = \sqrt{x^{2} + y^{2}} \f$
 */
template<typename T>
inline T radial(const T x, const T y)
{ return std::sqrt(x * x + y * y); }


//===========================================================================//
/*!
 * \brief Obtains the Euclidean radial distance of (x, y, z)
 *
 * Obtains and returns a radial distance, from \f$(0, 0, 0)\f$, to that
 * provided \f$(x, y, z)\f$. This is useful for spherical coordinate
 * transformations or obtaining the mangitude of some unit vector in both
 * planar and phase spaces.
 *
 * This function uses the following formula:
 * \f$ r = \sqrt{x^{2} + y^{2} + z^{2}} \f$
 */
template<typename T>
inline T radial(const T x, const T y, const T z)
{ return std::sqrt(x * x + y * y + z * z); }


//===========================================================================//
/*!
 * \brief Obtains the resulting angle from \f$(primary, secondary)\f$
 *
 * Obtains and returns the angle between a primary and secondary axis. Useful
 * for polar coordinate transformations.
 *
 * This is done using the following:
 * \f$ \phi = tan^{-1}(\frac{secondary}{primary}) \f$
 *
 * In standard Cartesian geometry, the primary axis will be the \f$x\f$-axis.
 */
template<typename T>
inline T angle(const T x, const T y)
{
    // Require(std::abs(x) > tolerance); // Handled by std::atan
    return std::atan(y / x);
}


//===========================================================================//
/*!
 * \brief Obtains the angles \f$ (\phi, \theta) \f$ from \f$ (x, y, z)\f$
 *
 * Obtains and returns the angles \f$ (\phi, \theta) \f$ between a set of
 * orthogonal Cartesian axes.
 *
 * \f$ \phi \f$ represents the azimuthal angle, being between the
 * primary and secondary axes, and \f$ \theta \f$ represents the polar angle,
 * being that between the tertiary and other dimensions.
 *
 * The following equation is used, given the coordinate of \f$ (x, y, z) \f$,
 * and is generalizeable to any such orthogonal axis set.
 * \f[
 *     \phi = tan^{-1}(\frac{y}{x}),
 *     \theta = tan^{-1}(\frac{\sqrt{x^{2} + y^{2}}}{z})
 * \f]
 */
template<typename T>
inline std::pair<T, T> angles(const T x, const T y, const T z)
{
    return std::make_pair(angle(x, y), angle(radial(x, y), z));
}


//===========================================================================//
/*!
 * \brief Transforms coordinates from 2-D Cartesian to 2-D polar
 *
 * Transforms a set of 2-D coordinates, being \f$(primary, secondary)\f$,
 * e.g. \f$(x, y)\f$, to the standard 2-D polar form, being \f$(r, \theta) \f$.
 */
template<typename T>
inline std::pair<T, T> cartesian_to_polar(
        const T primary_axis, const T secondary_axis) noexcept
{
    return std::make_pair(
            radial(primary_axis, secondary_axis),
            angle(primary_axis, secondary_axis));
}


//===========================================================================//
/*!
 * \brief Transforms coordinates from 3-D Cartesian to spherical
 *
 * Transforms a set of 3-D Cartesian coordinates, being
 * \f$ (primary, secondary, tertiary) \f$, e.g. \f$ (x, y, z) \f$, to spherical
 * coordinates, being \f$ (r, \phi, theta) \f$. Here, \f$ \theta \f$ is the
 * polar angle between the tertiary and other axes, and \f$ \phi \f$ the
 * azimuthal angle between the primary and secondary axes.
 */
template<typename T>
inline std::pair<T, std::pair<T, T>> cartesian_to_spherical(
        const T primary_axis, const T secondary_axis, const T tertiary_axis)
    noexcept
{
    return std::make_pair(
            radial(primary_axis, secondary_axis, tertiary_axis),
            angles(primary_axis, secondary_axis, tertiary_axis));
}


//---------------------------------------------------------------------------//
} // end namespace Geometry
} // end namespace FET

#endif // src_core_Transformations_hh
//---------------------------------------------------------------------------//
// end of src/core/Transformations.hh
//---------------------------------------------------------------------------//
