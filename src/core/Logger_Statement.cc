//---------------------------------*-C++-*-----------------------------------//
/*!
 * \file   src/core/Logger_Statement.cc
 * \author Seth R Johnson
 * \date   Sat Feb 21 00:12:15 2015
 * \brief  Logger_Statement class definitions.
 */
//---------------------------------------------------------------------------//

#include <utility>

#include "Logger_Statement.hh"
#include "FET_DBC.hh"

namespace FET
{

//---------------------------------------------------------------------------//
/*!
 * \brief Constructs the statement via a list of streams
 */
/*Logger_Statement::Logger_Statement(Vec_Ostream streams)
    : d_sinks(std::move(streams))
{
    // Require streams to point somewhere
#if defined(REQUIRE_ON)
    for (auto sink : d_sinks)
    {
        Require(sink);
    }
#endif

    if (!d_sinks.empty())
    {
        // Allocate a message stream if we're actually doing output
        d_message.reset(new osstream_t());
    }
    Ensure(!d_sinks.empty() == static_cast<bool>(d_message));
}
*/

//---------------------------------------------------------------------------//
/*!
 * \brief Flushes the output to the stream
 */
/*void Logger_Statement::flush()
{
    if (!d_message)
        return;

    try
    {
        // Add a trailing newline
        *d_message << "\n";

        // Get the string output
        const auto& message = d_message->str();

        // Write it to all the streams
        for (auto* stream_ptr : d_sinks)
        {
            *stream_ptr << message << std::flush;
        }
    }
    catch (const std::exception& e)
    {
        std::cerr << "An error occurred writing a log message: "
                  << e.what()
                  << std::endl;
    }
}
*/
//---------------------------------------------------------------------------//
} // end namespace FET

//---------------------------------------------------------------------------//
// end of src/core/Logger_Statement.cc
//---------------------------------------------------------------------------//
