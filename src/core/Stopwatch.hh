//---------------------------------*-C++-*-----------------------------------//
/*!
 * \file   src/core/Stopwatch.hh
 * \brief  Stopwatch class declaration.
 */
//---------------------------------------------------------------------------//
#ifndef src_core_Stopwatch_hh
#define src_core_Stopwatch_hh

#include <ctime>
#include <type_traits>
#include <vector>

#include "FET_DBC.hh"

namespace FET
{

//===========================================================================//
/*!
 * \class Stopwatch
 * \brief Simple Stopwatch class for estimating processor time
 *
 * Uses the simple \c ctime header utilities to estimate processor time. The
 * stopwatch supports standard features, namely \c start, \c stop, \c reset,
 * and \c lap. Note that the timer is not very precise and thus cannot measure
 * durations of small operations.
 *
 * The timer supports pausing, so clients may stop and re-start the timer at
 * will to avoid timing of irrelevant operations. E.g.,
 * \code
 * Stopwatch timer();
 *
 * // Important operations
 * timer.start();
 * ...
 * timer.stop();
 *
 * // Irrelevant operations
 * std::cout << timer.elapsed() << " (s) have elapsed.\n";
 *
 * timer.start();
 * ...
 * \endcode
 *
 * Further, laps may be created while the timer is running to easily create
 * new time stamps, e.g. when timing the duration of a loop many times.
 * \code
 * Stopwatch timer();
 * timer.start()
 * for (unsigned int i = 0; i < 10; i++)
 * {
 *     // Do work here...
 *     timer.lap();
 * }
 * timer.stop();
 * std::cout << "The loop averaged " << timer.average_lap() << " (s).\n";
 * \endcode
 */
/*!
 * \test src/core/test/tstStopwatch.cc
 *
 * Test of the Stopwatch object
 */
template<typename T = double>
class Stopwatch
{
  public:
    //@{
    //! \brief Public namespace aliases
    using clock_type      = clock_t;
    using set_type        = std::vector<clock_type>;
    using size_type       = set_type::size_type;
    using result_type     = T;
    //@}


  private:
    // >>> IMPLEMENTATION DATA

    //! \brief Logic flag indicating if stopwatch is running
    bool b_running = false;

    /*!
     * \brief Set of time stamps
     *
     * Contains the list of time stamps/laps of the watch. The 0th element
     * is the time of the last start, while all others are time differences.
     * e.g.,
     * [0] = reference
     * [1] = lap 1
     * .
     * .
     * .
     * [n] = lap[n]
     *
     * Note that this supports starting and stopping without creating new laps,
     * so you may use it as a standard stopwatch or with laps to obtain
     * averages.
     */
    set_type d_times{1};


  public:
    // >>> CONSTRUCTORS/DESTRUCTORS

    //! \brief Default constructor
    Stopwatch();

    //@{
    //! Copy control (default, copy, move, and destructor)
    Stopwatch(const Stopwatch&) = default;
    Stopwatch& operator=(const Stopwatch&) = default;
    Stopwatch(Stopwatch&&) = default;
    Stopwatch& operator=(Stopwatch&&) = default;
    virtual ~Stopwatch() = default;
    //@}


  private:
    // >>> SETTER METHODS

    //! \brief Sets the state of the runner
    void set_running(const bool running) noexcept
    { b_running = running; }

    //! \brief Adds a new time stamp to the list
    void add_time(const clock_type time) noexcept
    { d_times.push_back(time - reference_time()); }

    //! \brief Increments the current time elapsed record (for pausing)
    void increment_time(const clock_type time) noexcept;


  private:
    // >>> GETTER METHODS

    //! \brief Returns the current reference time
    clock_type reference_time() const noexcept
    { return d_times[0]; }

  public:

    //! \brief Indicates if the stopwatch is on (running)
    bool running() const noexcept
    { return b_running; }

    //! \brief Alias for \c running
    bool on() const noexcept
    { return running(); }

    //! \brief Returns the number of times stored
    size_type size() const noexcept
    { return d_times.size(); }

    //! \brief Returns the number of laps made for the stopwatch
    size_type num_laps() const noexcept
    { return size() - 1; }

    //! \brief Returns the elapsed time, in seconds, of the existing timer
    result_type time() const noexcept;

    //! \brief Returns the elapsed time, in seconds, of a single lap
    result_type time(const size_type index) const noexcept;

    //! \brief Alias for \c time()
    result_type elapsed() const noexcept
    { return time(); }

    //! \brief Alias for \c time(lap ID)
    result_type elapsed(const size_type index) const noexcept
    { return time(index); }

    //! \brief Returns the average lap time
    result_type average_lap() const noexcept;


  public:
    // >>> INTROSPECTION METHODS

    //! \brief Starts the timer
    void start()
    {
        Require(!running());
        set_running(true);
        d_times[0] = clock();
    }

    //! \brief Stops the timer
    void stop()
    {
        Require(running());
        increment_time(clock());
        set_running(false);
    }

    //! \brief Resets the timer
    void reset() noexcept
    {
        d_times.clear();
        d_times.push_back(0.0);
        set_running(false);
    }

    //! \brief Makes a lap (saves the current time and starts a new time)
    void lap()
    {
        Require(running());
        stop();
        d_times.push_back(0);
        start();
    }

}; // end class Stopwatch

//---------------------------------------------------------------------------//
} // end namespace FET

//---------------------------------------------------------------------------//
// INLINE AND TEMPLATE DEFINITIONS
//---------------------------------------------------------------------------//
#include "Stopwatch.i.hh"


#endif // src_core_Stopwatch_hh
//---------------------------------------------------------------------------//
// end of src/core/Stopwatch.hh
//---------------------------------------------------------------------------//
