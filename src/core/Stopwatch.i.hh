//---------------------------------*-C++-*-----------------------------------//
/*!
 * \file   src/core/Stopwatch.i.hh
 * \brief  Stopwatch class method implementations.
 */
//---------------------------------------------------------------------------//
#ifndef src_core_Stopwatch_i_hh
#define src_core_Stopwatch_i_hh

#include <ctime>
#include <type_traits>

#include "FET_DBC.hh"

namespace FET
{

//===========================================================================//
/*!
 * \brief Simple constructor
 */
template<typename T> Stopwatch<T>::Stopwatch()
{
    Insist(std::is_floating_point<result_type>::value,
            "The Stopwatch requires a floating-point type.");
}


//===========================================================================//
/*!
 * \brief Increments the current time elapsed record
 *
 * Increments the last time stored in the time stamp set. If no data was
 * stored, then it creates new data to store.
 */
template<typename T> void Stopwatch<T>::increment_time(
        const Stopwatch<T>::clock_type time) noexcept
{
    if (size() == 1)
    { add_time(time); }
    else
    { d_times[size() - 1] += time - reference_time(); }
}


//===========================================================================//
/*!
 * \brief Returns the elapsed time (seconds)
 *
 * Returns the elapsed time, in seconds, of the last active timer. If a timer
 * is currently active (i.e. on), the time elapsed for the timer is returned.
 */
template<typename T> typename Stopwatch<T>::result_type
Stopwatch<T>::time() const noexcept
{
    // If not started, no time elapsed
    if (size() < 1) return 0.0;

    // Calculate time
    Stopwatch<T>::clock_type current;
    if (running())
    {
        current = clock() - reference_time();
    }
    else
    {
        current = d_times[size() - 1];
    }
    return (Stopwatch<T>::result_type) current / CLOCKS_PER_SEC;
}



//===========================================================================//
/*!
 * \brief Returns the elasped time of a lap (seconds)
 *
 * Returns the elapsed time, in seconds, of the lap specified by the given lap
 * ID.
 */
template<typename T> typename Stopwatch<T>::result_type
Stopwatch<T>::time(const Stopwatch<T>::size_type index) const noexcept
{
    Require(index > 0);
    if (size() <= index)
    {
        // Give the last timer value
        return time();
    }
    else
    {
         // Return the requested timer value
         return (Stopwatch<T>::result_type)
                 d_times[index] / CLOCKS_PER_SEC;
    }
}


//===========================================================================//
/*!
 * \brief Returns the average time of a lap (seconds)
 *
 * Returns the average lap time for all laps performed, in seconds. If no
 * laps performed, simply returns the elapsed time.
 */
template<typename T> typename Stopwatch<T>::result_type
Stopwatch<T>::average_lap() const noexcept
{
    // If no timers, an average lap does not exist
    if (size() == 0) return 0.0;

    // Calculate the average time elapsed (peel off last time found first)
    Stopwatch<T>::result_type average = elapsed();
    for (Stopwatch<T>::size_type i = 1; i < size() - 1; ++i)
    {
        average += elapsed(i);
    }
    return num_laps() <= 1 ? average :
        average / static_cast<Stopwatch<T>::result_type>(num_laps());
}


//---------------------------------------------------------------------------//
} // end namespace FET

#endif // src_core_Stopwatch_i_hh
//---------------------------------------------------------------------------//
// end of src/core/Stopwatch.i.hh
//---------------------------------------------------------------------------//
