//---------------------------------------------------------------------------//
/*!
 * \file  src/core/test/tstName.cc
 * \brief Test of the Name object
 */
//---------------------------------------------------------------------------//

#include <stdexcept>

#include "FET_Assertions.hh"
#include "FET_DBC.hh"
#include "Name.hh"

namespace FET
{
namespace TEST
{
    //! \brief Tests the constructors and functions of the \c Name object
    void TEST_Name_Functions()
    {
        // Invalid construction
        auto foo = Name();
        Insist(foo.name().compare("untitled") == 0,
                "The default object name failed.");

        // Setter/getter methods
        foo.set_name("Foo");
        foo.set_description("Some object description");
        Insist(foo.name().compare("Foo") == 0,
                "Setting the object name failed.");
        Insist(foo.description().compare("Some object description") == 0,
                "Setting the object description failed.");

        // Construction with name only
        Name bar("Bar");
        Insist(bar.label().compare("Bar") == 0,
                "Constructor with name argument failed.");

        // Construction with name and description
        Name soo("Soo", "Another object");
        Insist(soo.name().compare("Soo") == 0,
                "Constructor with name and description for 'name' failed.");
        Insist(soo.description().compare("Another object") == 0,
                "Constructor with name and description for 'description' failed.");

        soo.set_label("Soo's real name");
        Insist(soo.label().compare("Soo's real name") == 0,
                "Setting the object label failed.");

        return;
    }

    //! \brief Tests the \c Name bounds
    void TEST_Name_Bounds()
    {
        auto foo = Name();
        try
        {
            foo.set_name("");
            throw std::length_error(
                    "Usage of invalid name failed to throw an error.");
        }
        catch (assertion& e)
        {
            // Correct functionality
        }

        return;
    }

    //! \brief Fully test the \c Name object
    void TEST_Name()
    {
        TEST_Name_Functions();
        TEST_Name_Bounds();
    }

} // End of namespace TEST
} // End of namespace FET


int main()
{

    FET::TEST::TEST_Name();
    return 0;
}

//---------------------------------------------------------------------------//
// end of src/core/test/tstName.cc
//---------------------------------------------------------------------------//
