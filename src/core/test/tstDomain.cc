//---------------------------------------------------------------------------//
/*!
 * \file  src/core/test/tstDomain.cc
 * \brief Test of the Domain object
 */
//---------------------------------------------------------------------------//

#include <stdexcept>

#include "FET_DBC.hh"
#include "Domain.hh"

namespace FET
{
namespace TEST
{
    //! \brief Tests different \c Domain instantiations (int, float, double)
    void TEST_Domain_Types()
    {
        // Check for <int>
        FET::Domain<int> x_dom("x", -1, 1);
        Insist(x_dom.lower() == -1 && x_dom.upper() == 1,
                "Lower and upper values for Domain<int> failed.");
        Insist(x_dom.contains(0), "Internal range query of Domain<int> failed.");
        Insist(x_dom.range() == 2, "Range query of Domain<int> failed.");
        Insist(x_dom(-4) == -1, "Simplified bound query of Domain<int> failed.");
        Insist(x_dom.center() == 0, "Query of Domain<int> center failed.");

        // Check for <float>
        FET::Domain<float> y_dom("y", -5.0, 5.0);
        Insist(y_dom.lower() == -5.0 && y_dom.upper() == 5.0,
                "Lower and upper values for Domain<float> failed.");
        Insist(y_dom.contains(0), "Internal range query of Domain<float> failed.");
        Insist(y_dom.range() == 10.0, "Range query of Domain<float> failed.");
        Insist(y_dom(5) == 5.0, "Simplified bound query of Domain<float> failed.");
        Insist(y_dom.center() == 0.0, "Query of Domain<float> center failed.");

        // Check for <double>
        FET::Domain<double> z_dom("z", 0.0, 10.0);
        Insist(z_dom.lower() == 0.0 && z_dom.upper() == 10.0,
                "Lower and upper values for Domain<double> failed.");
        Insist(z_dom.contains(0), "Internal range query of Domain<double> failed.");
        Insist(z_dom.range() == 10.0, "Range query of Domain<double> failed.");
        Insist(z_dom(0) == 0.0, "Simplified bound query of Domain<double> failed.");
        Insist(z_dom.center() == 5.0, "Query of Domain<double> center failed.");

        FET::Domain<double> other_dom = z_dom;
        Insist (other_dom == z_dom, "Simple Domain comparison failed.");
    }

    //! \brief Tests the \c Domain bounds by throwing errors
    void TEST_Domain_Bounds()
    {
        try
        {
            Domain<double> foo("E", 10.0, 0.0);
            throw std::domain_error(
                    "The Domain object failed to throw when the "
                    "range was [upper, lower]");
        }
        catch (assertion& e)
        {
            // Works properly
        }
        return;
    }


    //! \brief Performs all test of the \c Domain object
    void TEST_Domain()
    {
        TEST_Domain_Types();
        TEST_Domain_Bounds();
    }

} // End of namespace TEST
} // End of namespace FET


int main()
{

    FET::TEST::TEST_Domain();
    return 0;
}

//---------------------------------------------------------------------------//
// end of src/core/test/tstDomain.cc
//---------------------------------------------------------------------------//
