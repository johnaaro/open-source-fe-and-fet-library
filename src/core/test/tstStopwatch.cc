//---------------------------------------------------------------------------//
/*!
 * \file  src/core/test/tstStopwatch.cc
 * \brief Test of the Stopwatch object
 */
//---------------------------------------------------------------------------//

#include <cmath>
#include <iostream>
#include <random>
#include <stdexcept>

#include "Stopwatch.hh"

#include "FET_Assertions.hh"
#include "Comparisons.hh"
#include "FET_DBC.hh"
#include "FET_Macros.hh"

namespace FET
{
namespace TEST
{
    //! \brief Simply takes time (does no real work)
    void waste_time(const unsigned int max = 100000000)
    {
        std::minstd_rand0 generator{};
        for (unsigned int i = 0; i < max; ++i)
        {
            for (unsigned int j = 0; j < max; ++j)
            {
                // Make a conditional that will never occur (keep test files
                // small)
                if (generator.min() > generator()) std::cout << ".";
            }
        }
    }


    //! \brief Tests the \c Stopwatch start/stop feature for timer pausing
    void TEST_Stopwatch_Continuous()
    {
        auto foo = Stopwatch<>{};

        // Ensure construction is proper
        Insist(!foo.on(), "Stopwatch was 'on' upon construction.");
        Insist(is_equivalent(foo.size(), 1),
                "Stopwatch failed to initialize to size = 1.");
        Insist(is_approximate(foo.elapsed(), 0.0, 1.0E-02),
                "Elapsed time (in s) was not near-zero at construction.");
        Insist(is_approximate(foo.average_lap(), 0.0, 1.0E-02),
                "Average lap (in s) was not near-zero at construction.");

        // Perform some timings...
        foo.start();
        waste_time(10000);
        foo.stop();
        auto elapsed = foo.elapsed();
        std::cout.precision(3);
        std::cout << std::fixed << "Random loop cost is "
                  << elapsed << " seconds.\n";

        // Try re-starting it now (waste less time)
        foo.start();
        waste_time(3000);
        foo.stop();
        std::cout << "Random loop cost (again) is "
                  << foo.elapsed() << " seconds.\n";
        Insist(foo.elapsed() > elapsed,
                "Elapsed time failed to accumulate as prescribed.");
        return;
    }


    //! \brief Tests the lap feature of the stopwatch object
    void TEST_Stopwatch_Laps()
    {
        auto foo = Stopwatch<>{};

        // Obtain a rough estimate
        foo.start();
        waste_time(3000);
        foo.stop();
        auto elapsed = foo.elapsed();

        // Reset and find an average
        foo.reset();
        foo.start();
        for (unsigned int i = 0; i < 10; ++i)
        {
            // Do some work here...
            waste_time(3000);

            // Make the lap, if applicable
            if (i < 9)
            { foo.lap(); }
        }
        foo.stop();
        std::cout.precision(3);
        std::cout << "Average lap cost was "
                  << std::fixed << foo.average_lap() << " s.\n";
        auto delta = std::abs(foo.average_lap() - elapsed);
        Insist(is_approximate(delta, 0.0, 1.0E-02),
                "Average is not within the specified tolerance ("
                << delta << ").");
    }


    //! \brief Fully test the \c Stopwatch object
    void TEST_Stopwatch()
    {
        TEST_Stopwatch_Continuous();
        TEST_Stopwatch_Laps();
    }

} // End of namespace TEST
} // End of namespace FET


int main()
{

    FET::TEST::TEST_Stopwatch();
    return 0;
}

//---------------------------------------------------------------------------//
// end of src/core/test/tstStopwatch.cc
//---------------------------------------------------------------------------//
