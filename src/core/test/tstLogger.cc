//---------------------------------*-C++-*-----------------------------------//
/*!
 * \file   src/core/test/tstLogger.cc
 * \author Seth R Johnson
 * \date   Tue Feb 03 08:39:41 2015
 * \brief  Logger class definitions.
 */
//---------------------------------------------------------------------------//

#include "Logger.hh"

namespace FET
{
namespace TEST
{

//! \brief Some expensive operation for testing the Logger
struct Expensive
{
    int a;
    explicit Expensive(const int start_a) : a(start_a)
    { Require(start_a >= 0); }
};

    //! \brief Helper function to allow \c << Expensive() to occur
    std::ostream& operator<<(std::ostream& os, const Expensive& rhs)
    {
        std::cout << "--- Calling expensive ostream function ---"
                  << std::endl;
        for (int i = 0; i < rhs.a; ++i)
        {
            os << i;
        }
        return os;
    }


    //! \brief Tests the setup of the Logger
    void TEST_Logger_setup()
    {
        logger().set_global_level(DEBUG);
        logger().set_local_level(DIAGNOSTIC);
        logger().set("screen", &std::cerr, DEBUG);
        logger().remove("file");

        Validate(logger().local_level() == DIAGNOSTIC,
                "Failed to set local level to DIAGNOSTIC.");
        Validate(logger().global_level() == DEBUG,
                "Failed to set global level to DEBUG.");
    }


    //! \brief Tests general usage of the Logger
    void TEST_Logger_general()
    {
        // Global logging usage (primary type for FETs)
        log(WARNING) << "Some warning";
        log(INFO) << "Some information";
        log(ERROR) << "An error occurred";
        log(CRITICAL) << "Critical error, time to crash!";

        // For locally logging
        log_local() << "Some local information";

        // Change local level and check logging again
        logger().set_local_level(WARNING);
        log_local(INFO) << "Local info shouldn't show up";
        log(INFO) << "Global info should show up";
        log(DEBUG) << "Global debug should show up";
        log_local(DEBUG) << "Local debug should show up";
        log(DIAGNOSTIC) << "Diagnostic should show up";

        // Try a manipulator
        log() << std::endl << "^ this was a blank line";

        // Test multi-line messages
        log(INFO) << "This multi-line text message should"
                    << flush_next(INFO)
                    << "be flush with itself.";

        // Debug statements shouldn't have to call expensive functions
        // if logging level omits it
        log_local(DEBUG) << "This expensive print statement... "
            << Expensive(20)
            << "... shouldn't have been called.";
        log(DEBUG) << "Called an expensive statement... "
            << Expensive(10);
    }


    //! \brief Tests Logger setting functionality
    void TEST_Logger_setting()
    {
        // The "screen" output destination and minimum level can be changed at
        // will.
        log(DEBUG) << "This should show up";
        logger().set("screen", &std::cerr, WARNING);
        log(STATUS) << "This should NOT show up!";
    }


    //! \brief Tests logging functionality
    void TEST_Logger_logging()
    {
        auto stream_sp = std::make_shared<std::ostringstream>();

        // Additional loggers can be set with different "minimum" log levels.
        // For example, you could set an ofstream on node zero that only writes
        // warnings and higher.
        logger().set_global_level(INFO);
        logger().set("file", stream_sp, WARNING);

        log(DEBUG) << "Debug: Screen only (shouldn't show up)";
        log(WARNING) << "Warning: Screen and file";
        log(ERROR) << "Error: Screen and file";
        log(INFO) << "Status: Screen only (should show up)";

        // Check that only certain items were written to a file
        const auto& log_stream = stream_sp->str();
        Validate(log_stream ==
                "***   Warning: Screen and file\n\n!!!   Error: Screen and file\n",
                "The file stream didn't work as expected: " << log_stream);
    }


    //! \brief Performs all test of the Logger object
    void TEST_Logger()
    {
        TEST_Logger_setup();
        TEST_Logger_general();
        TEST_Logger_setting();
        TEST_Logger_logging();
    }


} // End of namespace TEST
} // End of namespace FET


int main()
{
    FET::TEST::TEST_Logger();
    return 0;
}

//---------------------------------------------------------------------------//
// end of src/core/test/tstLogger.cc
//---------------------------------------------------------------------------//
