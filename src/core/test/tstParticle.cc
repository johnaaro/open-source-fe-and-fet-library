//---------------------------------------------------------------------------//
/*!
 * \file  src/core/test/tstParticle.cc
 * \brief Test of the Particle object
 */
//---------------------------------------------------------------------------//

#include <vector>

#include "FET_Assertions.hh"
#include "FET_DBC.hh"
#include "Particle.hh"

namespace FET
{
namespace TEST
{

  //! \brief Tests the \c Particle object
  void TEST_Particle()
  {
    // Set values for testing
    const std::vector<double> pos = {1.0, 1.0, 1.0};
    const std::vector<double> dir = {0.0, 0.0, 1.0};
    const double step   = 2.4;
    const double weight = 0.5;
    const double energy = 14.0;
    const double tot_cross_section = 996.2;

    // Create the particle
    FET::Particle<double> particle = FET::Particle<double>();

    // Test position
    particle.set_position(pos);
    Insist(particle.position() == pos, "Positions don't match");

    // Test direction
    particle.set_direction(dir);
    Insist(particle.dir() == dir, "Directions don't match");

    // Test step
    particle.set_step(step);
    Insist(particle.step() == step, "The step doesn't match");

    // Test weight
    particle.set_weight(weight); // Set to valid value
    try {
        particle.set_weight(-0.5); // Try an invalid value now
    } catch (assertion& e) {
        // Caught the failure
    }
    Insist(particle.weight() == weight, "The weight doesn't match");

    // Test energy
    particle.set_energy(energy);
    Insist(particle.energy() == energy, "The energy doesn't match");

    // Test cross section
    particle.set_total_cross_section(tot_cross_section);
    Insist(particle.total_cross_section() == tot_cross_section,
            "The total cross section values don't match");

    // Test next position
    const std::vector<double> next_pos = particle.next_position();
    particle.update_position();
    Insist(particle.position() == next_pos, "The next position doesn't match");
  }

} // End of namespace TEST
} // End of namespace FET


int main()
{
    FET::TEST::TEST_Particle();
    return 0;
}

//---------------------------------------------------------------------------//
// end of src/core/test/tstParticle.cc
//---------------------------------------------------------------------------//
