//---------------------------------*-C++-*-----------------------------------//
/*!
 * \file   src/core/Name.hh
 * \brief  Name class declaration.
 */
//---------------------------------------------------------------------------//
#ifndef src_core_Name_hh
#define src_core_Name_hh

#include <string>

#include "FET_DBC.hh"

namespace FET
{
//===========================================================================//
/*!
 * \class Name
 * \brief Provides a simple name/label and brief description
 *
 * A simple object that provides a name or label and a description to an object
 * that derives from this.
 */
/*!
 * \example test/tstName.cc
 *
 * Tests the Name object
 *
 */
//===========================================================================//
class Name
{
  public:
    //@{
    //! Public type aliases
    using string         = std::string;
    //@}

    //@{ Default values
    static constexpr auto default_name = "untitled";
    static constexpr auto default_desc = "";
    //@}


  private:
    // >>> IMPLEMENTATION DATA

    //! \brief Name of basis set attribute
    string b_name = default_name;

    //! \brief Description of basis set attribute
    string b_description = default_desc;


  public:
    // >>> CONSTRUCTION

    //! \brief Simple constructor using both arguments
    Name(const string& name = default_name,
            const string& desc = default_desc)
        : b_description(desc)
    {
        set_name(name);
    }

    //@{
    //! Copy control (default, copy, move, and destructor)
    Name(const Name&) = default;
    Name& operator=(const Name&) = default;
    Name(Name&&) = default;
    Name& operator=(Name&&) = default;
    virtual ~Name() = default;
    //@}


  public:
    // >>> SETTERS

    //! \brief Set object label/name
    void set_name(const string& name)
    {
        Insist(!name.empty(), "A name cannot be empty.");
        b_name = name;
    }

    //! \brief Sets the object label/name
    void set_label(const string& label)
    { set_name(label); }

    //! \brief Set longer object description
    void set_description(const string& description) noexcept
    { b_description = description; }


  public:
    // >>> GETTERS

    //! \brief Returns the name/label
    const string& name() const noexcept
    { return b_name; }

    //! \brief Returns the name/label
    const string& label() const noexcept
    { return name(); }

    //! \brief Returns the description
    const string& description() const noexcept
    { return b_description; }


  public:
    // >>> INSTROSPECTION


}; // end class Name

//---------------------------------------------------------------------------//
} // end namespace FET

#endif // src_core_Name_hh
//---------------------------------------------------------------------------//
// end of src/core/Name.hh
//---------------------------------------------------------------------------//
