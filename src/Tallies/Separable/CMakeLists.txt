##---------------------------------------------------------------------------##
##
## src/Tallies/Separable/CMakeLists.txt
##
##---------------------------------------------------------------------------##
## CMAKE for the separable FET objects
##---------------------------------------------------------------------------##
cmake_minimum_required (VERSION 3.12.4)
PreventInSourceBuild()


##---------------------------------------------------------------------------##
## LIBRARY
##---------------------------------------------------------------------------##
library("Separable")

set(
  SOURCES
  Track_Length_S_FET.cc
  )

set(
  DEPENDENCIES
  harness
  core
  Poly_Attrs
  Polynomials
  Tally_Base
  )

add_library_wrapper(
  SOURCES ${SOURCES}
  DEPENDENCIES ${DEPENDENCIES}
  )


##---------------------------------------------------------------------------##
##                   end of src/Tallies/Separable/CMakeLists.txt
##---------------------------------------------------------------------------##
