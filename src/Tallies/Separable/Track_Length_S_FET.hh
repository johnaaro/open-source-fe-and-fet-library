//---------------------------------------------------------------------------//
/*!
 * \file  src/FET/Manager_FET/Tallies/Separable/Track_Length_S_FET.hh
 * \brief Separable/Track_Length_S_FET class declaration.
 */
//---------------------------------------------------------------------------//
#ifndef src_FET_Manager_FET_Tallies_Separable_Track_Length_S_FET_hh
#define src_FET_Manager_FET_Tallies_Separable_Track_Length_S_FET_hh

#include <memory>
#include <string>
#include <vector>

#include "FET_DBC.hh"
#include "Basis_Poly.hh"
#include "Separable_FET.hh"
#include "Particle.hh"

namespace FET
{
//===========================================================================//
/*!
 * \class Track_Length_Flux_SFET
 * \brief Separable track-length FET
 *
 * A separable FET for track-length flux
 */
/*!
 * \example test/tstSeparable/Track_Length_S_FET
 *
 * Test of the Track_Length_Flux_SFET object
 */
//===========================================================================//
class Track_Length_Flux_SFET : public Separable_FET
{
public:

    //@{
    //! Public type aliases
    using Base                    = FET::Separable_FET;
    using Vec_Dbl                 = std::vector<double>;
    using Vec_Vec_Dbl             = std::vector<Vec_Dbl>;
    //@}

public:
    // >>> CONSTRUCTOR/DESTRUCTORS

    //! \brief Basic Constructor
    Track_Length_Flux_SFET(const std::string &name,
              const std::vector<std::shared_ptr<Basis_Poly<>>> &basis_sets)
         : Base(name, basis_sets)
    { /* * */ }

    //! \brief Destructor
    virtual ~Track_Length_Flux_SFET() = default;


public:
    // >>> SETTERS
    // N/A presently


public:
    // >>> GETTTERS

    //! \brief Returns the FET object type
    const std::string type() const noexcept override
    { return "Track-length flux (separable FET)"; }


public:
    // >>> EVALUATION

    //! \brief Scores the coefficients at the specified location
    void score(const Particle<>& p)
    {
        // Obtain position at start/end of step
        const auto lower = p.position();
        const auto upper = p.next_position();

        // Set common constants for each scoring (reduce computations)
        d_num_scores++;
        const double coeff = p.weight() * p.step();
        if (std::abs(coeff) <= tolerance) return;

        // Score each domain individually:
        for (unsigned int i = 0; i < d_basis.size(); i++)
        {
            // Standardize the positions
            const auto lower_b = d_basis[i]->standardize(lower[i]);
            const auto upper_b = d_basis[i]->standardize(upper[i]);

            // Determine step size in this direction and create vector for
            // integration
            const double domain_step = std::abs(upper_b - lower_b);

            // Set values based on domain step size
            // (i.e. use lower bound values or perform integration)
            double weight_fct = 1.0;
            Vec_Dbl values(d_basis[i]->order() + 1, 0.0);

            if (domain_step < tolerance)
            {
                weight_fct = d_basis[i]->weight_fct(lower_b);
                values = d_basis[i]->evaluate(lower_b);
            }
            else
            {
                values = d_basis[i]->integrate(lower_b, upper_b);
            }

            // Add the scoring function for each order to the respective
            // coefficient
            for (unsigned int order = 0; order <= d_basis[i]->order(); order++)
            {
                if (domain_step < tolerance)
                {
                    // Set value for if domain_step ~ 0
                    d_coeff[i][order] += coeff * values[order] * weight_fct;
                }
                else
                {
                    // Score for non-negligible step sizes
                    d_coeff[i][order] += (coeff / (upper_b - lower_b))
                        * values[order];
                }
            }
        }
    }


public:
    // >>> INTROSPECTION
    // N/A presently


}; // end class Separable/Track_Length_S_FET

 //---------------------------------------------------------------------------//
} // end namespace FET

//---------------------------------------------------------------------------//
#endif // src_FET_Manager_FET_Tallies_Separable/Track_Length_S_FET_hh
//---------------------------------------------------------------------------//
// end of src/FET/Manager_FET/Tallies/Separable/Track_Length_S_FET.hh
//---------------------------------------------------------------------------//
