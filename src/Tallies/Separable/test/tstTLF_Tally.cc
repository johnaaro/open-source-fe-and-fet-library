//---------------------------------*-C++-*-----------------------------------//
/*!
 * \file   src/FET/Manager_FET/Tallies/Separable/test/tstTLF_Tally.cc
 * \brief  Track_Length_Flux_SFET class test
 */
//---------------------------------------------------------------------------//

#include <fstream>
#include <iostream>
#include <math.h>
#include <memory>
#include <random>

#include "FET_DBC.hh"

#include "Constants.hh"
#include "Domain.hh"

#include "Chebyshev_Attr.hh"
#include "Chebyshev_Poly.hh"
#include "Cosine_Attr.hh"
#include "Cosine_Poly.hh"
#include "Hermite_Attr.hh"
#include "Hermite_Poly.hh"
#include "Legendre_Attr.hh"
#include "Legendre_Poly.hh"
#include "Sine_Attr.hh"
#include "Sine_Poly.hh"

#include "Track_Length_S_FET.hh"

#include "Particle.hh"

namespace FET
{
namespace TEST
{
    //! \brief Constructs a TLF FET object with one dimension
    std::vector<Track_Length_Flux_SFET> TEST_TLF_1D()
    {
        // Make the domain
        const double bound = 1.0; //pi;
        const double shift = 0;
        const Domain<double> dom("x", -bound + shift, bound + shift);
//        const Domain<double> dom("x", -5, 2);

        // Create attributes
        const Chebyshev_Attr attr1{"Chebyshev basis set in (x)", 12, dom};
        const Cosine_Attr    attr2{"Cosine basis set in (x)", 12, dom};
        const Hermite_Attr   attr3{"Hermite basis set in (x)", 12, dom};
        const Legendre_Attr  attr4{"Legendre basis set in (x)", 8, dom};
        const Sine_Attr      attr5{"Sine basis set in (x)", 12, dom};
        const Legendre_Attr  attr6{"Legendre basis set in (x)", 8, dom};
        const Legendre_Attr  attr7{"Legendre basis set in (x)", 8, dom};

        // Create basis sets
        const Chebyshev_Poly<> poly1{attr1};
        const Cosine_Poly<>    poly2{attr2};
        const Hermite_Poly<>   poly3{attr3};
        const Legendre_Poly<>  poly4{attr4};
        const Sine_Poly<>      poly5{attr5};
//        const Legendre_Poly<>  poly6{attr6};
//        const Legendre_Poly<>  poly7{attr7};


        // Construct
//        Track_Length_Flux_SFET fet1("Chebyshev TLF",
//                {std::make_shared<Chebyshev_Poly<>>(poly1)});
//        Track_Length_Flux_SFET fet2("Cosine TLF",
//                {std::make_shared<Cosine_Poly<>>(poly2)});
//        Track_Length_Flux_SFET fet3("Hermite TLF",
//                {std::make_shared<Hermite_Poly<>>(poly3)});
        Track_Length_Flux_SFET fet4("Legendre TLF",
                {std::make_shared<Legendre_Poly<>>(poly4)});
//                std::make_shared<Legendre_Poly>(poly6)});
//        Track_Length_Flux_SFET fet5("Sine TLF",
//                {std::make_shared<Sine_Poly<>>(poly5)});
//        Track_Length_Flux_SFET fet6("Legendre TLF",
//                {std::make_shared<Legendre_Poly<>>(poly6)});
//        Track_Length_Flux_SFET fet7("Legendre TLF",
//                {std::make_shared<Legendre_Poly<>>(poly7)});

        std::vector<Track_Length_Flux_SFET> fets(
                {fet4});//, fet6, fet7});
                //{fet1, fet2, fet3, fet4, fet5, fet6, fet7});

        // Now test the various getters (also ensures proper construction)
        Insist(fet4.type() == "Track-length flux (separable FET)",
                "Object type failed.");

        // Return the object for further testing
        return fets;
    }


    //! \brief Scores a 1-D TLF object
    void TEST_TLF_1D_SCORE(
            std::vector<Track_Length_Flux_SFET> fets)
    {
        // Create the iteration points
        const auto dom = fets[0].basis("x")->attr()->domain();
        const unsigned int num_bins = (int) 100;//5E2;
        const double bin_size = dom.range() / double(num_bins);

        // The sample function
        const double n = 4.0;
        // Simple step functions: (n, x, x^2, etc.)
        // auto wgt_fct = [](double n, double x) { return n * x; };
        // auto wgt_fct = [](double n, double x) { return n * x * x; };
        // auto wgt_fct = [](double n, double x) { return n * x * x * x; };

        // Discontinuous functions
        /*
        auto wgt_fct = [](double n, double x) {
            if (x < -pi) { return 1.0; }
            if (x < 0.0) { return 2.0; }
            if (x < pi ) { return 1.0; }
            return 2.0 + 0 * n;
        };
        */
        /*
        auto wgt_fct = [](double n, double x) {
            return std::abs(n * pow(x, 3) - 6 * pow(x, 2) + 4 * x + 15); };
        */
        // Continuous functions
        auto wgt_fct = [](double n, double x) {
            const auto result = std::pow(x, 1.0);//0.5 + cos(n * x / (1.25 * pi));
//            if (result < 0 ) return 0.0;
            return result; };
        /*
         * Used for the ANS RPSD/ICRP 2020 paper on [0, 2pi]
         */
        /*
        auto wgt_fct = [](double n, double x) {
            const auto shift = 10.0;
            const auto scale = -0.50;
            const auto trans = pi;
            if (x >= trans)
            { return shift + sin (n * x); }
            // const auto a = - pow(n, 3) * cos(n * trans) / (6 * scale);
            const auto a = 1.0;
            const auto b = 3 * a * trans + pow(n, 2) * sin(n * trans) / (2 * scale);
            const auto c = n * cos(n * trans) / scale + 2 * b * trans -
                3 * a * pow(trans, 2);
            const auto d = (sin(n * trans) + shift) / scale - a * pow(trans, 3) +
                b * pow(trans, 2) - c * trans;
            return scale * (a * pow(x, 3) - b * pow(x, 2) + c * x + d);
        };
        */


        // Allocate memory
        std::vector<double> loc_v(int(num_bins), 0);
        for (unsigned int i = 0; i < fets.size(); i++)
        {
            fets[i].reset();
        }

        // Now score
        for (unsigned int i = 0; i < num_bins; i++)
        {
            // Get the location:
            double loc = dom.lower() + (bin_size * double(i));
            loc_v[i] = loc;// + bin_size / 2.0;

            // Create a particle at a point
            Particle<> p;
            p.set_position({loc, 0.0, 0.0});
            p.set_step(bin_size);
            p.set_weight(wgt_fct(n, loc_v[i]));

            // Score the particle now
            for (unsigned int j = 0; j < fets.size(); j++)
            {
                fets[j].score(p);
            }
        }

        // Finalize and evaluate:
        for (unsigned int i = 0; i < fets.size(); i++)
        {
            fets[i].finalize();
        }

        // Summarize file:
        std::ofstream file;
        file.open ("TLF_1D.txt");
        file << "Separable track length flux:\n"
             << "1-D basis set in X on [" << dom.lower() << ", "
             << dom.upper() << "]\n"
             << num_bins << " data points exist.\n\n";

        // Print header
        const std::string delimeter = ", ";
        file << "x_sim";
        for (unsigned int i = 0; i < fets.size(); i++)
        {
            file << delimeter << fets[i].basis("x")->attr()->type() << i + 1;
        }
        file << delimeter << "Actual";

        // Now print evaluation data at each point scored
        const auto normalization = 1.0 / (dom.range());
        for (unsigned int i = 0; i < num_bins; i++)
        {
            file << "\n"   << loc_v[i];
            for (unsigned int j = 0; j < fets.size(); j++)
            {
                file << delimeter << fets[j].evaluate({loc_v[i]});
            }
            file << delimeter
                 << (bin_size * wgt_fct(n, loc_v[i]) * normalization);
        }

        file.close();

        // Compare data agains the "accepted" solution
        //! \todo Validate against accepted solution (regression test)

    }


    //! \brief Performs a 1-D test/scoring of the \c Track_Length_Flux_SFET
    void TEST_TLF_FET()
    {
        // Test construction and get object
        auto fets = TEST_TLF_1D();

        // Score and print results
        TEST_TLF_1D_SCORE(fets);
    }


} // End of namespace TEST
} // End of namespace FET


int main()
{
    try {
        FET::TEST::TEST_TLF_FET();
    } catch (char const* message) {
        std::cerr << message << std::endl;
        return 1;
    }
    return 0;
}

//---------------------------------------------------------------------------//
// end of src/FET/Manager_FET/Tallies/Separable/test/tstTLF_Tally.cc
//---------------------------------------------------------------------------//
