//
// Created by emily on 10/12/2020.
//

//---------------------------------*-C++-*-----------------------------------//
/*!
 * \file   src/FET/Manager_FET/Tallies/Separable/test/tstTLF_2D_Tally.cc
 * \brief  Track_Length_Flux_SFET class test
 */
//---------------------------------------------------------------------------//

#include <cmath>
#include <fstream>
#include <iostream>
#include <math.h>
#include <memory>
#include <random>

#include "FET_DBC.hh"

#include "Constants.hh"
#include "Domain.hh"

#include "Legendre_Attr.hh"
#include "Legendre_Poly.hh"

#include "Cosine_Attr.hh"
#include "Cosine_Poly.hh"
#include "Sine_Attr.hh"
#include "Sine_Poly.hh"

#include "Track_Length_S_FET.hh"

#include "Particle.hh"

namespace FET
{
    namespace TEST
    {
        //! \brief Constructs a TLF FET object with two dimensions
        std::vector<Track_Length_Flux_SFET> TEST_TLF_2D()
        {
            // Make the domain
            const double bound = 1;
            const double shift = 0;
            const Domain<double> dom_x("x", -bound + shift, bound + shift);
            const Domain<double> dom_y("y", -bound - shift, bound - shift);

            // Create attributes
            const Legendre_Attr  l_attrx{"Legendre basis (x)", 12, dom_x};
            const Legendre_Attr  l_attry{"Legendre basis (y)",  7, dom_y};

//            const Cosine_Attr    c_attr{"Cosine basis set in (x)", 15, dom_x};
//            const Sine_Attr      s_attr{"Sine basis set in (y)",    5, dom_y};

            // Create basis sets
            const Legendre_Poly<>  lpoly_x{l_attrx};
            const Legendre_Poly<>  lpoly_y{l_attry};
//            const Cosine_Poly<>    cpoly_x{c_attr};
//            const Sine_Poly<>      spoly_y{s_attr};

            // Construct the FET(s)
            Track_Length_Flux_SFET fet_legendre("Legendre-only TLF", {});
//
            fet_legendre.add_basis_set(std::make_shared<Legendre_Poly<>>(lpoly_x));
            fet_legendre.add_basis_set(std::make_shared<Legendre_Poly<>>(lpoly_y));

//            Track_Length_Flux_SFET fet_angular("Cosine/Sine TLF", {});
//            fet_angular.add_basis_set(std::make_shared<Cosine_Poly<>>(cpoly_x));
//            fet_angular.add_basis_set(std::make_shared<Sine_Poly<>>(spoly_y));

            std::vector<Track_Length_Flux_SFET> fets(
                    {fet_legendre});



            // Now test the various getters (also ensures proper construction)
            Insist(fet_legendre.type() == "Track-length flux (separable FET)",
                   "Object type failed.");

            // Return the object for further testing
            return fets;
        }


        //! \brief Scores a 2-D TLF object
        void TEST_TLF_2D_SCORE(
                std::vector<Track_Length_Flux_SFET> fets)
        {
            // Create the iteration points
            const unsigned int num_bins = (int) 100;


            const auto dom_x = fets[0].basis("x")->attr()->domain();
            const double x_bin_size = dom_x.range() / double(num_bins);

            const auto dom_y = fets[0].basis("y")->attr()->domain();
            const double y_bin_size = dom_y.range() / double(num_bins);


            // The sample function
            // auto wgt_fct = [](double x, double y) { return 5.0 + 0. * x * y; };
            auto wgt_fct = [](double x, double y) { return 1.0; };
            // auto wgt_fct = [](double x, double y) { return y; }
            // auto wgt_fct = [](double x, double y) { return x * y; }


            // Allocate memory
            std::vector<double> loc_x(int(num_bins), 0);
            std::vector<double> loc_y(int(num_bins), 0);
            for (unsigned int i = 0; i < fets.size(); i++)
            {
                fets[i].reset();
            }

            // Score! (iterate over all X- and Y-space)
            const double inv_sqrt2 = 1 / std::sqrt(2.0);
            const std::vector<double> dir {inv_sqrt2, inv_sqrt2, 0.0};
            for (unsigned int i = 0; i < num_bins; i++)
            {
                // Get the location (store for later)
                double pos_x = dom_x.lower() + (x_bin_size * double(i));
                loc_x[i] = pos_x + x_bin_size / 2.0;

                // Iteration over y-space
                for (unsigned int j = 0; j < num_bins; j++)
                {
                    // Get location (store for later)
                    double pos_y = dom_y.lower() + (y_bin_size * double(j));
                    loc_y[j] = pos_y + y_bin_size / 2.0;

                    // Create a particle at a point, moving equally along X/Y
                    // domains (e.g. diagonally across the space)
                    Particle<> p;
                    p.set_direction(dir);
                    p.set_position({pos_x, pos_y, 0.0});
                    p.set_step(std::sqrt(x_bin_size * x_bin_size + y_bin_size * y_bin_size));
                    p.set_weight(wgt_fct(loc_x[i], loc_y[j])); // use weight in middle of particle step

                    // Score the particle now
                    for (unsigned int k = 0; k < fets.size(); k++)
                    {
                        fets[k].score(p);
                    }
                }
            }

            // Finalize and evaluate:
            for (unsigned int i = 0; i < fets.size(); i++)
            {
                fets[i].finalize();
            }

            // Summarize file:
            std::ofstream file;
            file.open ("TLF_2D_CHASE.txt");
            file << "Separable track length flux:\n"
                 << "Basis set in X on [" << dom_x.lower() << ", "
                 << dom_x.upper() << "]\n"
                 << "Basis set in Y on [" << dom_y.lower() << ","
                 << dom_y.upper() << "]\n"
                 << num_bins << " data points exist.\n\n";

            // Print header
            const std::string delimeter = ",";
            file << "x_sim" << delimeter << "y_sim" << delimeter << "Expected";
            for (unsigned int i = 0; i < fets.size(); i++)
            {
                file << delimeter << fets[i].name();
            }

            // Now print evaluation data at each point scored
            const auto normalization = x_bin_size * y_bin_size / (dom_x.range() * dom_y.range());
            for (unsigned int i = 0; i < num_bins; i++)
            {
                for (unsigned int j = 0; j < num_bins; j++)
                {
                    // Print domain values
                    file << "\n" << loc_x[i] << delimeter << loc_y[j];

                    // Print expected result
                    file << delimeter << (wgt_fct(loc_x[i], loc_y[j]) * normalization);

                    for (unsigned int k = 0; k < fets.size(); k++)
                    {
                        file << delimeter << fets[k].evaluate({loc_x[i], loc_y[j]});
                    }
                }
            }
            file.close();

            // Compare data agains the "accepted" solution
            //! \todo Validate against accepted solution (regression test)

        }


        //! \brief Performs a 2-D test/scoring of the \c Track_Length_Flux_SFET
        void TEST_TLF_FET()
        {
            // Test construction and get object
            auto fets = TEST_TLF_2D();

            // Score and print results
            TEST_TLF_2D_SCORE(fets);
        }


    } // End of namespace TEST
} // End of namespace FET


int main()
{
    try {
        FET::TEST::TEST_TLF_FET();
    } catch (char const* message) {
        std::cerr << message << std::endl;
        return 1;
    }
    return 0;
}

//---------------------------------------------------------------------------//
// end of src/FET/Manager_FET/Tallies/Separable/test/tstTLF_2D_Tally.cc
//---------------------------------------------------------------------------//
