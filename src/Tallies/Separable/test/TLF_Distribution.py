import pandas as pd
import matplotlib.pyplot as plt

"""Create a dataframe containing the data in data"""
data = pd.read_csv('../../../../TLF_1D.txt', sep=',', skiprows=range(0,5))
data.columns =  ['x', 'Flux', 'Actual']

"""Plot the distribution of legendre polynomials"""
plt.plot(data['x'], data['Flux'], label='Flux')
plt.plot(data['x'], data['Actual'], label='Actual')

"""Label the plot"""
plt.xlabel('x [units]')
plt.ylabel('Flux')
plt.title('Sample Separable Track Length Flux')
plt.legend(loc='best')
plt.grid()
plt.show()

plt.savefig('TLF_1D.png', dpi=1200)

