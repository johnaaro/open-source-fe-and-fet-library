import math
import numpy
import matplotlib.pyplot as pyplot
from matplotlib.ticker import MaxNLocator
import scipy.integrate as integrate
import matplotlib
import math

import matplotlib
import matplotlib.pyplot as pyplot
import numpy
import scipy.integrate as integrate
from matplotlib.ticker import MaxNLocator

matplotlib.use('TkAgg')


def CrazyFunction(x, y):
    xOffset = -0.4
    yOffset = -0.5
    damping = .4
    frequency = 1 * math.pi
    space = numpy.sin(frequency * numpy.exp(y - yOffset)) * numpy.cos(frequency * 1.5 *

                                                                      ((x + xOffset) * (x - 2 * xOffset) + (
                                                                                  y + yOffset) ** 2)) * numpy.exp(
        - damping * ((x +

                      xOffset) * (x - 2 * xOffset) + (y + yOffset) ** 2))
    return space


dpi = 144
plot3D = True
plotMap = True
plotDiff = True
legendreHigh = 20
mapHigh = 5
# I use rhomberg numerical integration, so the sizes must one greater than a power of 2
# Also note that 2**11 + 1 is as big as I can get for the map plot on a machine with 32 GB RAM
fidelity = 11
xSize = 2 ** fidelity + 1
ySize = 2 ** fidelity + 1
xLin = numpy.linspace(-1, 1, xSize)
yLin = numpy.linspace(-1, 1, ySize)
x, y = numpy.meshgrid(xLin, yLin, indexing="ij")
vmin = -1
vmax = 1
fineness = 4

params = {"legend.fontsize": "small", "legend.framealpha": 0.7, "legend.loc": "upper right",
          "legend.fancybox": True}
pyplot.rcParams.update(params)

truth = CrazyFunction(x, y)

if plot3D:
    print("\ nGenerating 3D representation of the sample distribution ... ")
    figureSplit = pyplot.figure(figsize=(10, 4))

    axes = pyplot.subplot2grid((1, 2), (0, 0), projection='3d')
    axes.set_axis_off()
    axes.view_init(80, -45)
    surf = axes.plot_surface(x, y, truth, cmap=pyplot.cm.gnuplot2, linewidth=0, rcount=

    xSize / fineness, ccount=ySize / fineness, vmin=vmin, vmax=vmax, antialiased=False)

    axes = pyplot.subplot2grid((1, 2), (0, 1))
    axes.xaxis.set_visible(False)
    axes.yaxis.set_visible(False)
    mesh = pyplot.pcolormesh(xLin, yLin, truth.T, cmap=pyplot.cm.gnuplot2, vmin=vmin,
                             vmax=vmax)
    pyplot.colorbar(mesh, format="%%.3 g")
    pyplot.title("Source")

    pyplot.tight_layout()
    # Save
    figureSplit.savefig("3_DRepresentation.png", dpi=144)
    pyplot.close()
    print("3D representation image saved !")

if plotMap or plotDiff:
    print("\ nGenerating expansion coefficients ... ")
    a = numpy.zeros((legendreHigh + 1, legendreHigh + 1))
    b = numpy.zeros((legendreHigh + 1, legendreHigh + 1))
    legendreXLin = numpy.polynomial.legendre.legvander(xLin, legendreHigh)
    legendreYLin = numpy.polynomial.legendre.legvander(yLin, legendreHigh)
    legendreX = numpy.zeros((legendreHigh + 1, xSize, ySize))
    legendreY = numpy.copy(legendreX)
    for i in range(legendreHigh + 1):
        legendreX[i], legendreY[i] = numpy.meshgrid(legendreXLin[:, i] * (i + 0.5),

                                                    legendreYLin[:, i] * (i + 0.5), indexing="ij")
    projectLin = numpy.zeros(ySize)
    for i in range(legendreHigh + 1):
        for j in range(legendreHigh + 1):
            projection = truth * legendreX[i] * legendreY[j]
            for p in range(ySize):
                projectLin[p] = integrate.romb(projection[:, p], xLin[1] - xLin[0])
            a[i, j] = integrate.romb(projectLin, yLin[1] - yLin[0])
    print(" done !")
    for i in range(legendreHigh + 1):
        for j in range(legendreHigh + 1):
            if i == 0 or j == 0:
                b[i, j] = a[i, j]

if plotMap:
    print("\ nGenerating map elements ... ")
    figureMap = pyplot.figure(figsize=(6, 4))

    allLegendre = numpy.zeros((mapHigh, mapHigh, xSize, ySize))
    individualLegendre = numpy.copy(allLegendre)
    for i in range(mapHigh):
        print("\ tComputing order {} of {}".format(i + 1, mapHigh))
        for j in range(mapHigh):
            coef = a[:(i + 1), :(j + 1)]
            axes = pyplot.subplot2grid((mapHigh, mapHigh), (i, j))
            axes.xaxis.set_visible(False)
            axes.yaxis.set_visible(False)
            allLegendre[i, j] = numpy.polynomial.legendre.leggrid2d(xLin, yLin, coef)
            individualLegendre[i, j] = numpy.copy(allLegendre[i, j])
            if i == 0:
                axes.set_title("i ={} ".format(j))
            else:
                individualLegendre[i, j] -= allLegendre[i - 1, j]
            if j == 0:
                axes.yaxis.set_visible(True)
                axes.set_yticks([])
                axes.set_ylabel("j ={} ".format(i), rotation=0, size='large ')
            else:
                individualLegendre[i, j] -= allLegendre[i, j - 1]
            if i > 1 and j > 1:
                individualLegendre[i, j] += allLegendre[i - 1, j - 1]
            plot = pyplot.pcolormesh(xLin, yLin, individualLegendre[i, j].T, cmap=pyplot.

                                     cm.gnuplot2)
    print(" Plotting the data ...")
    pyplot.tight_layout()
    # Save
    figureMap.savefig("ProjectionMap.png", dpi=dpi)
    pyplot.close()
    print("Saved map plot ")

if plotDiff:
    print("\n Generating diff plots ...")
    figureSplit = pyplot.figure(figsize=(10, 4))

    fullSet = numpy.zeros((legendreHigh + 1, xSize, ySize))
    separableSet = numpy.zeros((legendreHigh + 1, xSize, ySize))
    orders = range(legendreHigh + 1)
    variance = numpy.zeros((legendreHigh + 1))
    sepVar = numpy.zeros((legendreHigh + 1))
    meshVariance = numpy.zeros((legendreHigh + 1))
    for i, coefficients in enumerate(range(1, legendreHigh + 2)):
        print("\ tComputing order {: >2} of {: >2}".format(i, legendreHigh))
        # Compute the FE
        if i < mapHigh and plotMap:
            fullSet[i] = allLegendre[i, i]
        else:
            fullSet[i] = numpy.polynomial.legendre.leggrid2d(xLin, yLin, a[: coefficients,

                                                                         : coefficients])
        separableSet[i] = numpy.polynomial.legendre.leggrid2d(xLin, yLin, b[: coefficients

        , : coefficients])

        # Generate the equivalent mesh
        bins = i + 1
        histogram = numpy.zeros((bins, bins))
        histogramError = numpy.zeros((xSize, ySize))
        xSpace = xSize / bins
        ySpace = ySize / bins
        for m in range(bins):
            for n in range(bins):
                mMin = numpy.around(m * xSpace).astype(int)
                mMax = numpy.around((m + 1) * xSpace - 1).astype(int)
                nMin = numpy.around(n * ySpace).astype(int)
                nMax = numpy.around((n + 1) * ySpace - 1).astype(int)
                histogram[m, n] = numpy.mean(truth[mMin: mMax, nMin: nMax])
                for p in range(mMin, mMax + 1):
                    for q in range(nMin, nMax + 1):
                        histogramError[p, q] = histogram[m, n] - truth[p, q]
        xEdges = numpy.linspace(-1, 1, bins + 1)
        yEdges = numpy.linspace(-1, 1, bins + 1)
        dataDiff = fullSet[i] - truth
        variance[i] = dataDiff.var()
        sepDiff = separableSet[i] - truth
        sepVar[i] = sepDiff.var()
        meshVariance[i] = histogramError.var()
        if i == 0:
            diffMax = numpy.max((numpy.absolute(dataDiff).max(), numpy.absolute(histogramError).max()))
            diffMin = - diffMax

        # Clear the graph
        pyplot.clf()

        if i == 0:
            plural = ""
        else:
            plural = "s"

        # Plot the FE vs. FE Error
        # FE
        axes = pyplot.subplot2grid((1, 2), (0, 0))
        axes.xaxis.set_visible(False)
        axes.yaxis.set_visible(False)
        fe = pyplot.pcolormesh(xLin, yLin, fullSet[i].T, cmap=pyplot.cm.gnuplot2, vmin=

        vmin, vmax=vmax)
        pyplot.title("FE: {n} coefficient {p}".format(n=(coefficients ** 2), p=plural))

        pyplot.tight_layout()
        pyplot.colorbar(fe, format="%%.3 g")
        # Error
        axes = pyplot.subplot2grid((1, 2), (0, 1))
        axes.xaxis.set_visible(False)
        axes.yaxis.set_visible(False)
        dif = pyplot.pcolormesh(xLin, yLin, dataDiff.T, cmap=pyplot.cm.gnuplot2, vmin=

        diffMin, vmax=diffMax)
        pyplot.title("Difference with Source ")
        pyplot.colorbar(dif, format="%%.3 g")
        pyplot.tight_layout()
        # Save
        figureSplit.savefig("FE {:02}. png ".format(i), dpi=dpi)

        # Plot the FE vs. Mesh
        # FE
        axes = pyplot.subplot2grid((1, 2), (0, 0))
        axes.xaxis.set_visible(False)
        axes.yaxis.set_visible(False)
        fet = pyplot.pcolormesh(xLin, yLin, fullSet[i].T, cmap=pyplot.cm.gnuplot2, vmin=

        vmin, vmax=vmax)
        pyplot.title("FE: {n} coefficient {p}".format(n=(coefficients ** 2), p=plural))
        pyplot.tight_layout()
        pyplot.colorbar(fet, format="%%.3 g")
        # Mesh
        axes = pyplot.subplot2grid((1, 2), (0, 1))
        axes.xaxis.set_visible(False)
        axes.yaxis.set_visible(False)
        mesh = pyplot.pcolormesh(xEdges, yEdges, histogram.T, cmap=pyplot.cm.gnuplot2,

                                 vmin=vmin, vmax=vmax)
        pyplot.colorbar(mesh, format="%%.3 g")
        pyplot.title("Mesh : {n} coefficient {p}".format(n=(coefficients ** 2), p=plural))
        pyplot.tight_layout()
        # Save
        figureSplit.savefig("Mesh {:02}. png ".format(i), dpi=dpi)

        # Multivariate FE
        axes = pyplot.subplot2grid((1, 2), (0, 0))
        axes.xaxis.set_visible(False)
        axes.yaxis.set_visible(False)
        fet = pyplot.pcolormesh(xLin, yLin, fullSet[i].T, cmap=pyplot.cm.gnuplot2, vmin=

        vmin, vmax=vmax)
        pyplot.title("Multivariate : {n} coefficient {p}".format(n=(coefficients ** 2), p=

        plural))
        pyplot.tight_layout()
        pyplot.colorbar(fet, format="%%.3 g")
        # Separable FE
        axes = pyplot.subplot2grid((1, 2), (0, 1))
        axes.xaxis.set_visible(False)
        axes.yaxis.set_visible(False)
        fet = pyplot.pcolormesh(xLin, yLin, separableSet[i].T, cmap=pyplot.cm.gnuplot2,

                                vmin=vmin, vmax=vmax)
        pyplot.title("Separable : {n} coefficient {p}".format(n=(coefficients ** 2 - (

                coefficients - 1) ** 2), p=plural))
        pyplot.tight_layout()
        pyplot.colorbar(fet, format="%%.3 g")
        # Save
        figureSplit.savefig("SeparableFE {:02}. png ".format(i), dpi=dpi)

    # Multivariate FE
    i = 5
    coefficients = 6
    axes = pyplot.subplot2grid((1, 2), (0, 0))
    axes.xaxis.set_visible(False)
    axes.yaxis.set_visible(False)

    fet = pyplot.pcolormesh(xLin, yLin, fullSet[i].T, cmap=pyplot.cm.gnuplot2, vmin=vmin,

                            vmax=vmax)
    pyplot.title("Multivariate : {n} coefficient {p}".format(n=(coefficients ** 2), p=plural)

                 )
    pyplot.tight_layout()
    pyplot.colorbar(fet, format="%%.3 g")
    # Separable FE
    i = 20
    coefficients = 21
    axes = pyplot.subplot2grid((1, 2), (0, 1))
    axes.xaxis.set_visible(False)
    axes.yaxis.set_visible(False)
    fet = pyplot.pcolormesh(xLin, yLin, separableSet[i].T, cmap=pyplot.cm.gnuplot2, vmin=

    vmin, vmax=vmax)
    pyplot.title("Separable : {n} coefficient {p}".format(n=(coefficients ** 2 - (
            coefficients - 1) ** 2), p=plural))
    pyplot.tight_layout()
    pyplot.colorbar(fet, format="%%.3 g")
    # Save
    figureSplit.savefig("CloseSeparable.png", dpi=dpi)
    # Plot the statistical history
    meanVariancePlot = pyplot.figure(figsize=(8, 3.3))
    variancePlot, = pyplot.semilogy(orders, variance)
    sepVariancePlot, = pyplot.semilogy(orders, sepVar)
    meshVariancePlot, = pyplot.semilogy(orders, meshVariance)
    pyplot.title("Statistical variance as a function of order ")
    pyplot.xlabel("Order ")
    pyplot.ylabel("Variance ")
    meanVariancePlot.gca().xaxis.set_major_locator(MaxNLocator(integer=True))
    pyplot.autoscale(enable=True, axis='x', tight=True)
    legend = pyplot.legend([variancePlot, sepVariancePlot, meshVariancePlot],
                           ["FEMultivariate" "FE Separable" "Mesh "], loc='lower left ')
    legend.get_frame().set_linewidth(0.0)
    pyplot.tight_layout()
    # Save
    meanVariancePlot.savefig("FEStatisticsHistory.pdf ".format(i))
