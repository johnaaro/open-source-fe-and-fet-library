# import pandas as pd
#
# plot = True
#
# if (plot):
#     print(f"Plotting enabled")
#     import matplotlib
#     import matplotlib.pyplot as plt
# else:
#     print(f"Plotting disabled.")
#
# # Create a dataframe containing the data in data
# data = pd.read_csv('../../../../build/src/Tallies/Separable/test/TLF_2D.txt', sep=',', skiprows=range(0,8))
# print(f"Data columns: {data.columns}")
#
# # Plot the distribution of legendre polynomials
# if (plot):
#     fig = plt.figure()
#     ax = plt.axes(projection='3d')
#
# # Simple scale as needed (observe distribution)
# increment = 1
# for label in data.columns[3:]:
#     scalar = data['  Actual'].mean() / data[label].mean()
#
#     # Print scalar to terminal and graph
#     print(f"The {label} was being scaled by {scalar} for the following plot.")
#     if (plot):
#         ax.text(increment, increment, data['  Actual'].mean(),
#                 f"{label} scaled by {scalar}", color = "red")
#
#     # Now scale:
#     data[label] = data[label] * scalar
#
# # Specify data to plot
# print(f"{data.head(1000)}")
# if (plot):
#     for label in data.columns[2:]:
#         ax.plot_trisurf(data['x_sim'], data['  y_sim'], data[label],
#                         linewidth=0, antialiased=False)
#         # ax.plot_surface(data['x_sim'], data['y_sim'], data[label],
#         #                rstride = 1, cstride = 1,
#         #                cmap = 'viridis', edgecolor = 'none')
#     # Label the plot
#     ax.set_xlabel("X (units")
#     ax.set_ylabel("Y (units)")
#     ax.set_zlabel("Flux Value")
#     ax.set_title("2-D Track Length Flux FET Benchmark")
#     plt.legend(loc='best')
#     plt.grid()
#
#     plt.show()
#     plt.savefig('TLF_2D.png', dpi=1200)
#     plt.close(fig)




import pandas as pd
import matplotlib.pyplot as plt

# data = pd.read_csv('../../../../build/src/Tallies/Separable/test/TLF_2D.txt', sep=',', skiprows=range(0, 9))

data = pd.read_csv('../../../../TLF_2D.txt', sep=',', skiprows=range(0, 9))

fig = plt.figure()
ax = plt.axes(projection='3d')

ax.set_xlabel('x')
ax.set_ylabel('y')
ax.set_zlabel('FET')

data.columns = ['x', 'y', 'Actual', 'FET']
x = data['x']
y = data['y']
z = data['FET']

ax.plot_trisurf(x, y, z, linewidth=0, antialiased=False)

# ax.scatter(x, y, z, color='red')

ax.set_title('Estimated FET Surface')

plt.show()
plt.savefig('TLF_2D_Estimated.png', dpi=1200)
