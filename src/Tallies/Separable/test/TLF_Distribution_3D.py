from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

data = pd.read_csv('../../../../TLF_3D.txt', sep=',', skiprows=range(0, 13))

fig = plt.figure()
ax = fig.add_subplot(projection='3d')

data.columns = ['x', 'y', 'z', 'Actual', 'fet']
x = data['x']
y = data['y']
z = data['z']
c = data['fet']

# x = np.random.standard_normal(100)
# y = np.random.standard_normal(100)
# z = np.random.standard_normal(100)
# c = np.random.standard_normal(100)

img = ax.scatter(x, y, z, c=c, cmap=plt.hot())
fig.colorbar(img)
plt.show()


# import pandas as pd
# import matplotlib.pyplot as plt
#
# data = pd.read_csv('../../../../build/src/Tallies/Separable/test/TLF_3D.txt', sep=',', skiprows=range(0, 13))
#
# fig = plt.figure()
# ax = plt.axes(projection='3d')
#
# ax.set_xlabel('x')
# ax.set_ylabel('y')
# ax.set_zlabel('FET')
#
# data.columns = ['x', 'y', 'z', 'fet', 'Actual']
# x = data['x']
# y = data['y']
# z = data['z']
#
# ax.plot_trisurf(x, y, z, linewidth=0, antialiased=False)
#
# # ax.scatter(x, y, z, color='red')
#
# ax.set_title('Estimated FET Surface')
#
# plt.show()
# plt.savefig('TLF_3D_Estimated.png', dpi=1200)
