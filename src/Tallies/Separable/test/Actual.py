
import pandas as pd
import matplotlib.pyplot as plt

data = pd.read_csv('../../../../build/src/Tallies/Separable/test/TLF_2D.txt', sep=',', skiprows=range(0, 9))

fig = plt.figure()
ax = plt.axes(projection='3d')

ax.set_xlabel('x')
ax.set_ylabel('y')
ax.set_zlabel('FET')

data.columns = ['x', 'y', 'fet', 'Actual']
x = data['x']
y = data['y']
z = data['Actual']

ax.plot_trisurf(x, y, z, linewidth=0, antialiased=False)

ax.set_title('Actual Surface')

plt.show()
plt.savefig('TLF_2D_Actual.png', dpi=1200)