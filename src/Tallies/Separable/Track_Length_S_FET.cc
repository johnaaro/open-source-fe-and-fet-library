//---------------------------------------------------------------------------//
/*!
 * \file  src/FET/Manager_FET/Tallies/Separable/Track_Length_Flux_S_FET.cc
 * \brief Separable FET class declaration.
 */
//---------------------------------------------------------------------------//

#include <cmath>

#include "Constants.hh"
#include "FET_DBC.hh"
#include "Track_Length_S_FET.hh"
#include "Particle.hh"

namespace FET
{
//---------------------------------------------------------------------------//
/*!
 * \brief Scores the separable coefficients based on the particle
 *
 * Coefficients for track-length flux uses two different methods for when the
 * particle does not, or does, move a non-negligible amount in the respective
 * domain being analyzed. Coefficients are scored according to the following:
 * \f[
 * a_{n} = \frac{ c_{\psi_{n}}}{N} \sum_{i=1}^{N} \sum_{c=1}^{C_{i}}
 *         S_{n, i, c} (w_{i,c}, d_{i, c}, z_{i, c}, z_{i, c+1})
 * \f]
 *
 * \note The \f$ c_{\psi_{n}} \f$ and \f$ N \f$ terms above are applied in
 * the \c finalize method, with \f$ c_{\psi_{n}} \f$ the order normalization
 * and \f$ N \f$ the number of particles sampled.
 *
 * The scoring method for a negligible step in the respective domain is
 * as follows:
 * \f[
 * S_{n, i, c} (w_{i,c}, d_{i, c}, z_{i, c}, z_{i, c+1}) =
 *      (w_{i,c} d_{i,c}) \psi_{n}(z_{i,c}) \rho(z_{i,c})
 * \f]
 *
 * The scoring method for a non-negligible step in the respective domain is as
 * follows:
 * \f[
 * S_{n, i, c} (w_{i,c}, d_{i, c}, z_{i, c}, z_{i, c+1}) =
 *      \frac{w_{i,c} d_{i,c}}{z_{i,c+1} - z_{i,c}}
 *      \int_{z_{i,c}}^{z_{i,c+1}} \psi_{n} (z) \rho(z) dz
 * \f]
 *
 * \note It is assumed that the particle's position and next position are
 * representative of the order that basis sets were added to the FET object.
 *
 * \todo Set limits on for-loop to use min of location and particle location
 * \todo Parse method into several smaller methods(?)
 * \todo Embed call to (or actual implementation) of variance scoring
 * \todo Look into modifying step size used for particles whose track leaves
 * the domain?
 */
/*
void Track_Length_Flux_SFET::score(const Particle<>& p)
{
    // Obtain position at start/end of step
    const auto lower = p.position();
    const auto upper = p.next_position();

    // Set common constants for each scoring (reduce computations)
    d_num_scores++;
    const double coeff = p.weight() * p.step(); //w_i,c * d_i,c
    if (std::abs(coeff) <= tolerance) return;

    // Score each domain individually:
    for (unsigned int i = 0; i < basis().size(); i++)
    {
        // Standardize the positions
        const auto lower_b = basis(i)->standardize(lower[i]);
        const auto upper_b = basis(i)->standardize(upper[i]);

        // Determine step size in this direction and create vector for
        // integration
        const double domain_step = std::abs(upper_b - lower_b);

        // Set values based on domain step size
        // (i.e. use lower bound values or perform integration)
        double weight_fct = 1.0;
        Vec_Dbl values(basis(i)->order() + 1, 0.0);
        if (domain_step < tolerance)    //x_i,c = x_i,c+1
        {
            weight_fct = basis(i)->weight_fct(lower_b); // rho
            values = basis(i)->evaluate(lower_b);       // psi
        }
        else //x_i,c != x_i,c+1
        {
            values = basis(i)->integrate(lower_b, upper_b); // int_{x_c}^{x+1}
        }

        // Add the scoring function for each order to the respective
        // coefficient
        for (unsigned int order = 0; order <= basis(i)->order(); order++)
        {
            if (domain_step < tolerance) //x_i,c = x_i,c+1
            {
                // Set value for if domain_step ~ 0
                d_coeff[i][order] += coeff * values[order] * weight_fct; //w_i,c * d_i,c * psi * rho
            }
            else //x_i,c != x_i,c+1
            {
                // Score for non-negligible step sizes
//                d_coeff[i][order] += (coeff / (domain_step))
//                                     * values[order];
                d_coeff[i][order] += (coeff / (upper_b - lower_b)) // (w_i,c * d_i,c) / (x_i,c+1 - x_i,c) *
                                    * values[order];
            }
        }
    }
}
*/

//---------------------------------------------------------------------------//
} // end namespace FET

//---------------------------------------------------------------------------//
// end of src/FET/Manager_FET/Tallies/Separable/Track_Length_Flux_S_FET.cc
//---------------------------------------------------------------------------//
