//---------------------------------------------------------------------------//
/*!
 * \file  src/Tallies/Base/Results_Separable.t.hh
 * \brief Results_Separable class declaration.
 */
//---------------------------------------------------------------------------//
#ifndef src_Tallies_Separable_Results_Separable_t_hh
#define src_Tallies_Separable_Results_Separable_t_hh

#include <algorithm>
#include <iostream>
#include <utility>

#include "FET_DBC.hh"
#include "Results_Separable.hh"
#include "States.hh"

namespace FET
{

//===========================================================================//
/*!
 * \brief Sets all data contained by the object to zero and prepares to score
 *
 * Sets all data contained by the object to zero. This method does NOT remove
 * any polynomial representations previously added.
 *
 * \note This method completes the pre-processing step and moves the object to
 * a processing state.
 */
template<typename T>
void Results_Separable<T>::pre_process() noexcept
{
    for (auto& it : d_data)
    { std::fill(it.second.begin(), it.second.end(), 0.); }
    Base::set_state(process_state::processing);
}


//===========================================================================//
/*!
 * \brief Increments a coefficient set
 *
 * Increments a coefficient set by that of the argument, not exceeding the
 * data in the coefficient set.
 */
template<typename T>
void Results_Separable<T>::increment(
        const Results_Separable<T>::key_type& key,
        const Results_Separable<T>::dataset_type& amount)
{
    // Pre-process all data if not in a processing state
    Require(Base::processing());

    // Obtain the coefficient reference and ensure size is valid
    auto data = d_data[key];
    Check(data.size() <= amount.size());

    // Increment the data
    for (typename Results_Separable<T>::dataset_type::size_type i = 0;
            i < data.size(); i++)
    { data[i] += amount[i]; }
}


//===========================================================================//
/*!
 * \brief Scales the data contained within the data structure
 *
 * Scales the data by the scaling factor of specified
 */
template<typename T>
void Results_Separable<T>::scale_data(
        const Results_Separable<T>::scaling_type val) noexcept
{
    auto normalizer = [val](T datum) { return datum / val; };
    for (auto& it : d_data)
    {
        std::transform(it.second.begin(), it.second.end(), it.second.begin(),
                normalizer);
    }
}


//---------------------------------------------------------------------------//
} // end namespace FET

//---------------------------------------------------------------------------//
#endif // src_Tallies_Separable_Results_Separable_t_hh
//---------------------------------------------------------------------------//
// end of src/Tallies/Base/Results_Separable.t.hh
//---------------------------------------------------------------------------//
