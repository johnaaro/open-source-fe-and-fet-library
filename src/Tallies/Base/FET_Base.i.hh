//---------------------------------------------------------------------------//
/*!
 * \file  src/Tallies/Base/FET_Base.i.hh
 * \brief FET_Base class declaration.
 */
//---------------------------------------------------------------------------//
#ifndef src_Tallies_Base_FET_Base_i_hh
#define src_Tallies_Base_FET_Base_i_hh

#include "FET_Base.hh"


namespace FET
{

//---------------------------------------------------------------------------//
/*!
 * \brief Flags if the scoring particle is within the FET's domain
 *
 * Returns a flag for if the location provided lies within the FET's domain.
 * It is assumed that the location has dimensions maching that of the FET.
 * Given this notation, it is best that software clients are careful when
 * establshing the FET objects to ensure valid use cases are given.
 *
 * As an example, a Basis_FET with basis sets having dimensions in x, z, and y
 * will assume the location is given as {x, z, y}.
 *
 * In the event the FET object will use non-Cartesian coordinates, the client
 * is expected to transform the location into one compatible with the location
 * of the FET object.
 * \todo Migrate to use the Site_Handler object to map from an arbitrary
 * particle domain to the arbitrary FET domain.
 */
    template<class T, class particle_type>
    bool FET_Base<T, particle_type>::contains(const particle_type& particle)
    const noexcept
    {
        // Ensure the size is that of the FET
        Require(particle.position().size() >= d_poly.size());

        // Iterate over each domain and verify if within
        auto location = particle.position();
        for (unsigned int i = 0; i < d_poly.size(); ++i)
        {
            if (!d_poly[i]->contains(location[i]))
            {
                return false;
            }
        }
        return true;
    }


//---------------------------------------------------------------------------//
} // end namespace FET

//---------------------------------------------------------------------------//
#endif // src_Tallies_Base_FET_Base.i.hh
//---------------------------------------------------------------------------//
// end of src/Tallies/Base/FET_Base.i.hh
//---------------------------------------------------------------------------//
