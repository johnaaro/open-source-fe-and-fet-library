//---------------------------------------------------------------------------//
/*!
 * \file  src/Tallies/Base/Results_Separable.hh
 * \brief Results_Separable class declaration.
 */
//---------------------------------------------------------------------------//
#ifndef src_Tallies_Separable_Results_Separable_hh
#define src_Tallies_Separable_Results_Separable_hh

#include <iostream>
#include <unordered_map>
#include <utility>
#include <vector>

#include "FET_DBC.hh"
#include "Results_Base.hh"

namespace FET
{

//===========================================================================//
/*!
 * \class Results_Separable
 * \brief Contains the coefficient and variance data for a separable FET
 *
 * Contains the coefficient and variance set for a single FET object. Items may
 * be added to the object at any time. Built-in increment functionality
 * requires the object be in a \c processing state, however the client may
 * utilize the bracket or the \c at() notation to perform scoring outside the
 * object.
 *
 * Linear scaling functionality is provided with this interface. Non-linear
 * scaling or per-element normalization must be implemented by the client using
 * the bracket or the \c at() notation.
 *
 * Pre-processing is provided, effectively setting all data to 0. Using the \c
 * increment functionality further performs a pre-processing if not already
 * done. Lastly, data may be quickly scaled via \c finalize, at a constant
 * reference to said data obtained via \c extract(), extracting either the
 * entire container or a single dataset by key.
 *
 * \note The interface is meant to wrap the underlying container, with
 * additional functionality for the purposes of this library. See online
 * documentation for said container for further details and implementation
 * on those functions.
 */
/*!
 * \example src/Tallies/Base/test/tstResults_Separable.cc
 *
 * Test of the Results_Separable object
 */
//===========================================================================//
template<typename T = double>
class Results_Separable : public Results_Base<T>
{
    using Base     = Results_Base<T>;
public:

    //@{
    //! Public type aliases
    using data_type        = typename Base::data_type;
    using key_type         = typename Base::key_type;
    using dataset_type     = typename std::vector<data_type>;
    using container_type   =
        typename std::unordered_map<key_type, dataset_type>;
    using size_type        = typename container_type::size_type;

    using scaling_type     = typename Base::scaling_type;
    using state_type       = typename Base::state_type;
    //@}


private:
    // >>> IMPLEMENTATION DATA

    //! \brief Contains the data by [label], being of \c key_type
    container_type d_data{};


public:
    // >>> CONSTRUCTOR/DESTRUCTORS

    //! Default Constructor
    Results_Separable(
            const typename Base::scaling_type scaling = Base::default_scaling)
        : Base(scaling)
    { /* * */ }

    //@{
    //! Copy control (copy, move, and destructor)
    Results_Separable(const Results_Separable&) = default;
    Results_Separable& operator=(const Results_Separable&) = default;
    Results_Separable(Results_Separable&&) = default;
    Results_Separable& operator=(Results_Separable&&) = default;
    virtual ~Results_Separable() = default;
    //@}


public:
    // >>> METHODS OF TYPICAL OF STD:: CONTAINERS

    // >>>>>> Capacity

    //! \brief Returns if the data set is empty or not
    bool empty() const noexcept
    { return d_data.empty(); }

    //! \brief Returning the number of items represented in the data set
    size_type size() const noexcept
    { return d_data.size(); }

    // >>>>>> Modifiers

    //! \brief Clears the underlying data structure
    void clear() noexcept
    {
        Base::reset();
        d_data.clear();
    }

    //! \brief Resets the underlying data structure by clearing it's contents
    void reset() noexcept override
    { clear(); }

    //! \brief Inserts a dataset if one doesn't exist
    void insert(const key_type& key, const size_type order)
    { insert(std::make_pair(key, dataset_type(order))); }

    //! \brief Inserts a dataset if one doesn't exist
    void insert(const std::pair<key_type, dataset_type>& item)
    { d_data.insert(item); }

    //! \brief Inserts a dataset if one doesn't exist
    void insert(const std::pair<key_type, dataset_type>&& item)
    { d_data.insert(item); }

    //! \brief Erases a dataset from the data structure
    void erase(const key_type& key)
    { d_data.erase(key); }

    // >>>>>> Lookup

    //! \brief Access specified element with bounds checking
    dataset_type& at(const key_type& key)
    { return d_data.at(key); }

    //! \brief Access specified element with bounds checking
    const dataset_type& at(const key_type& key) const
    { return d_data.at(key); }

    //! \brief Access specified element
    dataset_type& operator[](const key_type& key)
    {
        // Require(contains(key));
        return d_data[key];
    }

    //! \brief Access specified element
    dataset_type& operator[](key_type&& key)
    {
        // Require(contains(key));
        return d_data[key];
    }

    //! Indicates if a representation exists in the data
    bool contains(const key_type& key) const
    { return (d_data.find(key) != d_data.end()); }

    //! \brief Returns the element with specific key (alias for \c at)
    dataset_type& find(const key_type& key)
    { return at(key); }


public:
    // >>> GETTERS

    //! \brief Returns the underlying data structure
    const container_type& extract() const
    {
        Require(Base::finalized());
        return d_data;
    }

    /*!
     * \brief Returns a constant reference to a dataset
     *
     *  Note this method assumes that the data has been finalized
     */
    const dataset_type& extract(const key_type& key) const
    { return extract().at(key); }


public:
    // >>> SETTERS & MODIFIERS

    //! \brief Sets all data contained by the object to zero
    void pre_process() noexcept;

    //! \brief Increments a dataset
    void increment(const key_type&, const dataset_type&);


public:
    // >>> INTROSPECTION

    //! Describes the object
    virtual void describe(std::ostream& = std::cout) const
    { Not_Implemented("describe"); }


private:

    //! Performs the scaling of internal data
    void scale_data(const scaling_type) noexcept override;


}; // end class Results_Separable

 //---------------------------------------------------------------------------//
} // end namespace FET

//---------------------------------------------------------------------------//
// Inline and Template instantiations
//---------------------------------------------------------------------------//

// #include "Results_Separable.i.hh"
#include "Results_Separable.t.hh"

//---------------------------------------------------------------------------//
#endif // src_Tallies_Separable_Results_Separable_hh
//---------------------------------------------------------------------------//
// end of src/Tallies/Base/Results_Separable.hh
//---------------------------------------------------------------------------//
