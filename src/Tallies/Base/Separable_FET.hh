//---------------------------------------------------------------------------//
/*!
 * \file  src/FET/Manager_FET/Tallies/Separable_FET.hh
 * \brief Separable_FET class declaration.
 */
//---------------------------------------------------------------------------//
#ifndef src_FET_Manager_FET_Tallies_Separable_FET_hh
#define src_FET_Manager_FET_Tallies_Separable_FET_hh

#include <algorithm>
#include <iostream>
#include <memory>
#include <vector>
#include <string>

#include "FET_DBC.hh"
#include "Basis_FET.hh"

namespace FET
{
//===========================================================================//
/*!
 * \class Separable_FET
 * \brief General structure and API of separable FET objects
 *
 * The Separable_FET object acts as the general API for each of the separable
 * FET objects. These objects may be used for estimating particle flux,
 * particle current, and potentially many other types of tallies.
 */
/*!
 * \example src/Tallies/Base/test/tstSeparable_FET.cc
 *
 * Test of the Separable_FET object
 */
//===========================================================================//
class Separable_FET : public Basis_FET
{
public:

    //@{
    //! Public type aliases
    using Base                    = Basis_FET;
    using Vec_Dbl                 = std::vector<double>;
    using Vec_Vec_Dbl             = std::vector<Vec_Dbl>;
    //@}

protected:
    // >>> IMPLEMENTATION DATA

    //! \brief Coefficient for each order of each basis set
    //!
    //! \note Reference is d_coeff[dimension][order]
    Vec_Vec_Dbl d_coeff;

    Vec_Vec_Dbl d_locations;
    //! \brief Coefficient variance for each order of each basis set
    //!
    //! \note Reference is d_variance[dimension][order]
    Vec_Vec_Dbl d_variance;

    Vec_Dbl d_flux;

    Vec_Dbl d_energies;

    double zero_order_norm = 1.0;




public:
    // >>> CONSTRUCTOR/DESTRUCTORS

    //! \brief Basic Constructor
    Separable_FET(const std::string &name,
              const std::vector<std::shared_ptr<Basis_Poly<>>> &basis_sets)
         : Base(name, basis_sets)
    {
        reset();
    }

    //! \brief Destructor
    virtual ~Separable_FET() = default;

public:
    // >>> SETTERS

    //! \brief Resets the coefficients and associated variances
    void reset() override
    {
        // Ensure the object was constructed with some basis sets
        Require(basis().size() > 0);

        // Now allocate the coefficient data for each associated basis set
        d_coeff.resize(basis().size());
        d_variance.resize(basis().size());
        //d_flux.resize(3,-1.0);//resize for assumption of 3 dimensions
        for (unsigned int i = 0; i < basis().size(); i++)
        {
            // Set array size for each basis set
            d_coeff[i].resize(basis(i)->order() + 1);
            d_variance[i].resize(basis(i)->order() + 1);

            // Set all values to 0.00 (for repeat uses)
            for (unsigned int order = 0; order <= basis(i)->order(); order++)
            {
                d_coeff[i][order] = 0.00;
                d_variance[i][order] = 0.00;
            }
        }
    }


public:
    // >>> GETTTERS

    //! \brief Returns the FET object type
    virtual const std::string type() const noexcept override
    { return "Separable FET"; }

    Vec_Vec_Dbl locations() const override
    { return d_locations; }

    //! \brief Returns the coefficients
    Vec_Vec_Dbl coefficients() const override
    { return d_coeff; }

    //! \brief Returns the variance coefficients
    Vec_Vec_Dbl variance() const override
    { return d_variance; }

    Vec_Dbl flux() const override
    { return d_flux; }

    Vec_Dbl energies() const override
    { return d_energies; }

public:
    // >>> EVALUATION

    //! \brief Returns the flux of the FET at a single location
    void evaluate_flux(const Particle<>& p) override {

        const auto location = p.position();
        const auto energy = p.energy();


        d_energies.push_back(energy);
        d_locations.push_back(location);

       // if (!contains(location) || basis().size() == 0) return 0.0;
        double val = 1.0;
//    const auto zero_order = d_coeff[0][0];// * d_coeff[1][0];// * d_coeff[2][0];
        double zero_order = 1.0;
        for (unsigned int i = 0; i < (basis().size()); i++) {
            // Evaluate the basis set:
            zero_order *= d_coeff[i][0];

            const auto std_loc = basis(i)->standardize(location[i]);
            const auto wgt_fct = basis(i)->weight_fct(std_loc);
            const auto basis_res = basis(i)->evaluate(std_loc);
            // Evaluate coefficients now for each associated basis value
            double basis_eval = 0; // Sum of [coeff_n * P_n{x}]
            for (unsigned int j = 0; j <= basis(i)->order(); j++) {
                const auto ortho_const = basis(i)->orthonorm_const(j);
                basis_eval += d_coeff[i][j] * ortho_const * wgt_fct * basis_res[j];
            }

            // Now accumulate the multiplication:
            // Evaluation = Multiplication_Product (basis_eval's)
            val *= basis_eval;
        }
        val /= zero_order;
        d_flux.push_back(val);
        // return val;
    }
    //! \brief Returns the variance of the coefficients at a point
    double variance(const Vec_Dbl &location) const override
    {
        // Require that the location is within the domain
        // Note: This is NOT an issue during production, but good for checking
        //       during development (debug)
        if (!contains(location)) return 0.0;

        // Now evaluate the polynomial
        double var = 1.0;
        for (unsigned int i = 0; i < basis().size(); i++)
        {
            // Evaluate the basis set:
            const auto basis_res = basis(i)->evaluate(
                    basis(i)->standardize(location[i])
                    );

            // Evaluate the variance now for each associated basis value
            double basis_eval = 0; // Sum of [coeff_n * P_n{x}]
            for (unsigned int j = 0; j <= basis(i)->order(); j++)
            {
                basis_eval += d_variance[i][j] * basis_res[j];
            }

            // Now accumulate the multiplication:
            // Evaluation = Multiplication_Product (basis_eval's)
            var *= basis_eval;
        }
        return var;
    }


public:
    // >>> INTROSPECTION

    //! \brief Finalizes the particles
    void finalize(const double divisor = 1.0) override
    {
        for (unsigned int i = 0; i < basis().size(); i++)
        {
            // Normalize each coefficient set if not previously normalized
            //! \todo Do we want to do this?
            if (!basis(i)->attr()->normalize())
            {
                basis(i)->normalize(d_coeff[i]);
                basis(i)->normalize(d_variance[i]);
            }
        }

        // Now normalize by the requested value and associated volume
        normalize (divisor * (num_scored() > 0 ? double(num_scored()) : 1.0));
    }

    //! \brief Normalizes the coefficient and variance sets by some value
    void normalize(const double divisor = 1.0) override
    {
        // Ensure the divisor is valid
        Insist(divisor > tolerance,
                "The " << type()
                << " requires a positive, non-zero\n"
                << "normalization factor to normalize");
        for (unsigned int i = 0; i < basis().size(); i++)
        {
            for (unsigned int order = 0; order <= basis(i)->order(); order++)
            {
                d_coeff[i][order] /= divisor;
                d_variance[i][order] /= divisor;
            }
        }
    }

}; // end class Separable_FET

 //---------------------------------------------------------------------------//
} // end namespace FET

//---------------------------------------------------------------------------//
#endif // src_FET_Manager_FET_Tallies_Separable_FET_hh
//---------------------------------------------------------------------------//
// end of src/FET/Manager_FET/Tallies/Separable_FET.hh
//---------------------------------------------------------------------------//
