//---------------------------------------------------------------------------//
/*!
 * \file  src/Tallies/Base/States.hh
 * \brief States class declaration.
 */
//---------------------------------------------------------------------------//
#ifndef src_Tallies_Base_States_hh
#define src_Tallies_Base_States_hh

namespace FET
{

// Work-arounds for macros defining the ENUM states
#ifdef pre_process
#undef pre_process
#endif
#ifdef process
#undef process
#endif
#ifdef post_process
#undef post_process
#endif

//===========================================================================//
/*!
 * \brief Enumerates the states of an object at any time during an execution
 *
 * Specifies possible states allowed for data to take. This includes
 * pre-processing, processing, and post-processing data.
 *
 * The \c pre_process state characterizes data that has not yet been
 * modified during the simulation, such as prior to scoring FET coefficients.
 *
 * The \c processing state is exemplary of scoring FET coefficients. Data in
 * this state signifies data that is actively being manipulated during the
 * simulation.
 *
 * The \c post_process state is characteristic of data that is not longer being
 * manipulated. Thus, data in this state will no longer be added to. In the
 * context of FET coefficient sets, this is characteristic of AFTER particles
 * have been scored and the coefficients have been scaled.
 */
//===========================================================================//
enum class process_state{
    pre_process = 0,
    processing,
    post_process
};


//---------------------------------------------------------------------------//
} // end namespace FET

//---------------------------------------------------------------------------//
#endif // src_Tallies_Base_States_hh
//---------------------------------------------------------------------------//
// end of src/Tallies/Base/States.hh
//---------------------------------------------------------------------------//
