//---------------------------------------------------------------------------//
/*!
 * \file  src/FET/Manager_FET/Tallies/Separable_FET.cc
 * \brief Separable FET class declaration.
 */
//---------------------------------------------------------------------------//

#include <vector>

#include "Constants.hh"
#include "FET_DBC.hh"
#include "Separable_FET.hh"

namespace FET
{
//---------------------------------------------------------------------------//
/*!
 * \brief Resets the coefficients and associated variances
 */
/*
void Separable_FET::reset() noexcept
{
    // Ensure the object was constructed with some basis sets
    Require(basis().size() > 0);

    // Now allocate the coefficient data for each associated basis set
    d_coeff.resize(basis().size());
    d_variance.resize(basis().size());
    for (unsigned int i = 0; i < basis().size(); i++)
    {
        // Set array size for each basis set
        d_coeff[i].resize(basis(i)->order() + 1, 0.0);
        d_variance[i].resize(basis(i)->order() + 1, 0.0);

        // Set all values to 0.00 (for repeat uses)
        for (unsigned int order = 0; order <= basis(i)->order(); order++)
        {
            d_coeff[i][order] = 0.00;
            d_variance[i][order] = 0.00;
        }
    }
}
*/

//---------------------------------------------------------------------------//
/*!
 * \brief Finalizes the FET coefficients and variances
 *
 * Finalizes the FET coefficients and variances. Note this method simply
 * normalizes the coefficients and associated variances by the provided
 * argument.
 *
 * Normalization occurs as follows for each basis set utilized and for each
 * order:
 * \f[
 * a_{n} = \frac{c_{\psi_{n}}}{N} a_{n}
 * \f]
 *
 * Where \f$ c_{\psi_{n}} \f$ is the l-2 norm of the basis set, and \f$ N \f$
 * is some normalization constant that is desired by the client.
 *
 * \todo Add basis set normalization (if NOT done during scoring)?
 */
/*
double Separable_FET::evaluate(const Separable_FET::Vec_Dbl location)
{
    if (!contains(location) || basis().size() == 0) return 0.0;
    double val = 1.0;
//    const auto zero_order = d_coeff[0][0];// * d_coeff[1][0];// * d_coeff[2][0];
    double zero_order = 1.0;
    for (unsigned int i = 0; i < (basis().size()); i++) {
        // Evaluate the basis set:
        zero_order *= d_coeff[i][0];

        const auto std_loc = basis(i)->standardize(location[i]);
        const auto wgt_fct = basis(i)->weight_fct(std_loc);
        const auto basis_res = basis(i)->evaluate(std_loc);
        // Evaluate coefficients now for each associated basis value
        double basis_eval = 0; // Sum of [coeff_n * P_n{x}]
        for (int j = 0; j <= basis(i)->order(); j++) {
            const auto ortho_const = basis(i)->orthonorm_const(j);
            basis_eval += d_coeff[i][j] * ortho_const * wgt_fct * basis_res[j];
        }

        // Now accumulate the multiplication:
        // Evaluation = Multiplication_Product (basis_eval's)
        val *= basis_eval;
    }
    val /= zero_order;
    d_flux.push_back(val);
    return val;
}
*/
//---------------------------------------------------------------------------//
/*!
 * \brief Evaluates the FET variance at a point
 *
 * Evaluates the FET variance at a given point. See documentation from the \c
 * Basis_FET object's \c contains member for further detail and assumptions on
 * the location argument.
 *
 * \todo Ensure, mathematically, this is how variance is treated for FETs
 */
/*
double Separable_FET::variance(const Separable_FET::Vec_Dbl location)
    const noexcept
{
    // Require that the location is within the domain
    // Note: This is NOT an issue during production, but good for checking
    //       during development (debug)
    if (!contains(location)) return 0.0;

    // Now evaluate the polynomial
    double var = 1.0;
    for (unsigned int i = 0; i < basis().size(); i++)
    {
        // Evaluate the basis set:
        const auto basis_res = basis(i)->evaluate(
                basis(i)->standardize(location[i])
                );

        // Evaluate the variance now for each associated basis value
        double basis_eval = 0; // Sum of [coeff_n * P_n{x}]
        for (unsigned int j = 0; j <= basis(i)->order(); j++)
        {
            basis_eval += d_variance[i][j] * basis_res[j];
        }

        // Now accumulate the multiplication:
        // Evaluation = Multiplication_Product (basis_eval's)
        var *= basis_eval;
    }
    return var;
}
*/

//---------------------------------------------------------------------------//
} // end namespace FET

//---------------------------------------------------------------------------//
// end of src/FET/Manager_FET/Tallies/Separable_FET.cc
//---------------------------------------------------------------------------//
