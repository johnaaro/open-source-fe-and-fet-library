//---------------------------------------------------------------------------//
/*!
 * \file  src/Tallies/Base/FET_Base.hh
 * \brief FET_Base class declaration.
 */
//---------------------------------------------------------------------------//
#ifndef src_Tallies_Base_FET_Base_hh
#define src_Tallies_Base_FET_Base_hh

#include "Poly_Manager.hh"
#include "Particle.hh"
#include "Basis_Poly.hh"
#include "Name.hh"
#include "Results_Base.hh"

namespace FET
{
//===========================================================================//
/*!
 * \class FET_Base
 * \brief General structure and API of FET objects
 *
 * Provides the underlying structure and functionality of FET objects. It acts
 * as a general API for both the separable and convolved FET objects, as well
 * as any others that may be created not utilizing those structures.
 */
/*!
 * \example src/Tallies/Base/test/tstFET_Base.cc
 *
 * Test of the FET_Base object
 */
//===========================================================================//
template<class data_container, class particle_type = Particle<double>>
class FET_Base : public Name
{
    using Base     = Name;
public:

    //@{
    //! Public type aliases
    using data_type           = typename data_container::data_type;
    using poly_manager        = Poly_Manager<Basis_Poly<data_type>>;
    using key_type            = Poly_Manager<>::key_type;
    using number_type         = unsigned int;
    using space_type          = std::vector<data_type>;
    using SP_Base             = Poly_Manager<>::SP_Base;
    using Vec_Dbl             = std::vector<double>;
    using Vec_Vec_Dbl         = std::vector<Vec_Dbl>;
    //@}


private:
    // >>> IMPLEMENTATION DATA

    /*!
     * \brief Polynomial manager
     *
     * Managements the set of polynomials provided to the FET object. Parts of
     * the API are exposed here for adding, removing, and querying the manager.
     */
    Poly_Manager<> d_poly;

    /*!
     * \brief FET data (contains all coefficients and variances)
     *
     * Container of the coefficient and variance data for the FET object. None
     * of the API is exposed to the user except to query the entire object
     * itself.
     */
    data_container d_data;


    /*!
     * \brief Contains the mapping between the space provided to the FET and
     * the actual orientation o the FET
     */

protected:

    /*!
     * \brief The number of scored particles
     *
     * Counts the number of particles that were actually scored in the FET for
     * the duration of processing.
     */
    number_type d_num_scores = 0;


public:
    // >>> CONSTRUCTOR/DESTRUCTORS

    //! \brief Basic Constructor
    //template<T> todo::remove?
    FET_Base(std::string& name,
             std::vector<SP_Base> polynomials)
        : Base(name)
    { if(!polynomials.empty()) d_poly.insert(polynomials); }

    //@{
    //! Copy control (copy, move, and destructor)
    FET_Base(const FET_Base&) = default;
    FET_Base& operator=(const FET_Base&) = default;
    FET_Base(FET_Base&&) = default;
    FET_Base& operator=(FET_Base&&) = default;
    virtual ~FET_Base() = default;
    //@}


public:
    // >>> SETTERS

    //! \brief Adds a polynomial to the FET
    void insert(const SP_Base& poly)
    {
        d_poly.insert(poly);
        d_data.insert(poly->num_terms());
    }

    //! \brief Adds a set of polynomials to the FET
    void insert(const std::vector<SP_Base>& polys)
    {
        for(auto& it : polys)
        { insert(it); }
    }

    //! \brief Clears the polynomial and coefficient/variance dataset
    void reset()
    {
        d_poly.reset();
        reset_data();
    }

    /*!
     * \brief Clears the coefficient/variance dataset
     *
     * Meant to clear, or reset, the coefficient/variance dataset. That is,
     * remove all items from the dataset.
     * \note Each FET object with a unique container for the dataset must
     * implement this method.
     */
    virtual void reset_data()
    { /* * */ }


public:
    // >>> GETTTERS

    //! \brief Returns the FET object type
    virtual const std::string type() const noexcept
    { return "FET Base"; }

    virtual Vec_Vec_Dbl coefficients() const
    {
        Vec_Dbl v = {1,2,3};
        Vec_Vec_Dbl v2 = {v,v,v};
        return v2;
    }
    virtual Vec_Vec_Dbl variance() const
    {
        Vec_Dbl v = {1,2,3};
        Vec_Vec_Dbl v2 = {v,v,v};
        return v2;
    }
    virtual Vec_Dbl flux() const
    {
        Vec_Dbl v = {1,2,3};
        return v;
    }

    virtual Vec_Vec_Dbl locations() const
    {
        Vec_Dbl v = {1,2,3};
        Vec_Vec_Dbl v2 = {v,v,v};
        return v2;
     }
    virtual Vec_Dbl energies() const
    {
        Vec_Dbl v = {1,2,3};
        return v;
    }

public:
    // >>> EVALUATION

    /*!
     * \brief Evaluates the FET at a single domain point
     *
     * This method provides the pre-processing for FET evaluation. FET
     * evaluation occurs by providing the FET with a single domain point,
     * namely one that corresponds to the every domain point contained by the
     * FET object.
     */
    virtual data_type evaluate(const space_type& location) const
    {
        // Throw an error
        Not_Implemented("variance");

        // Use variables to avoid DEBUG compiler errors
        FET_IGNORE(location);
        return 0.0;
    }

    //! \brief Returns the variance of the coefficients at a point
/*    virtual data_type variance(const space_type& location) const
    {
        // Throw an error
        Not_Implemented("variance");

        // Use variables to avoid DEBUG compiler errors
        FET_IGNORE(location);
        return 0.0;
    }

*/

    virtual void evaluate_flux(const Particle<>& p) const{

        Not_Implemented("flux");

        FET_IGNORE(p);
    }

public:
    // >>> INTROSPECTION

    //! \brief Describes the object
    //! \todo Implement 'describe' for FET objects
    virtual void describe(std::ostream& os) const
    {
        // Throw an error
        Not_Implemented("describe");

        // Use the variable to ignore DEBUG errors
        FET_IGNORE(os);
    }

    //! \brief Returns if a point is within the domain of the FET
    bool contains(const space_type&) const noexcept;

    //! \brief Scores a particle
    virtual void score(const Particle<data_type>& p) const
    {
        // Throw an error
        Not_Implemented("score");

        // Use the variable to ignore DEBUG errors
        FET_IGNORE(p);
    }

    /*!
     * \brief Returns the volume of the FET space
     *
     * Calculates the volume occupied by the FET polynomials assuming
     * non-repeated and independent spaces exist in the FET (i.e. no overlap)
     * \see Poly_Manager::volume
     */
    data_type volume() const
    { return d_poly.volume(); }

    //! \brief Finalizes the coefficient and variance sets
    virtual void finalize(const double divisor = 1.0)
    {
        // Throw an error
        Not_Implemented("Finalized");

        // Use the variable to ignore DEBUG errors
        FET_IGNORE(divisor);
    }

    //! \brief Normalizes the coefficients and variances
    virtual void normalize()
    { Not_Implemented("normalize"); }


}; // end class FET_Base

 //---------------------------------------------------------------------------//
} // end namespace FET

//---------------------------------------------------------------------------//
#endif // src_Tallies_Base_FET_Base_hh
//---------------------------------------------------------------------------//
// end of src/Tallies/Base/FET_Base.hh
//---------------------------------------------------------------------------//
