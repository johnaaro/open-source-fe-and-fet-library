//---------------------------------*-C++-*-----------------------------------//
/*!
 * \file   src/Tallies/Base/test/tstResults_Base.cc
 * \brief  Results_Base class test.
 */
//---------------------------------------------------------------------------//

#include <iostream>

#include <cmath>
#include <stdexcept>
#include <type_traits>

#include "FET_Assertions.hh"
#include "Constants.hh"
#include "FET_DBC.hh"

#include "Results_Base.hh"

namespace FET
{
namespace TEST
{

    //! \brief Tests the construction of the \c Results_Base object
    void TEST_Results_Base_Constructor()
    {
        // Test defaults
        Results_Base<> foo{};
        Insist(foo.scaling() == Results_Base<>::default_scaling,
                "The default scaling factor was not found as required.");
        if (!std::is_same<double, Results_Base<>::data_type>::value)
        {
            Insist(false,
                    "The default type could not be recognized as 'double'.");
        }
        Insist(foo.pre_processing(),
                "The default state was not utilized as required.");
        Insist(!foo.scaled(),
                "The object was found to be scaled upon construction.");

        // Test when using 'double'
        Results_Base<double> soo{5.0};
        if (!std::is_same<double, Results_Base<double>::data_type>::value)
        { Insist(false, "Specialization via 'double' failed."); }
        Insist(std::abs(soo.scaling() - 5.0) <= tolerance,
                "The scaling factor was not set as constructed.");

        // Test for other type (i.e. float)
        if (!std::is_same<float, Results_Base<float>::data_type>::value)
        { Insist(false, "Specialization via 'float' failed."); }
    }

    //! Tests the scaling functionality provided
    void TEST_Results_Base_Scaling()
    {
        Results_Base<> foo{1.0};

        // Test adding of scale factors
        foo.add_scaling(2.0);
        foo.add_scaling(5.0);
        Insist(std::abs(foo.scaling() - 2.0 * 5.0) <= tolerance,
                "'add_scaling' failed to work as designed.");

        // Test resetting scale factor
        foo.set_scaling(1.0);
        Insist(foo.scaling() == 1.0, "'set_scaling' failed.");

        // Test scaling data now
        foo.scale(); // Shouldn't do anything when scaling = 1
        foo.scale(1.0);
        foo.set_scaling(5.0);
        try
        {
            foo.scale();
            throw std::logic_error(
                    "Attemping to scale didn't operate as prescribed.");
        }
        catch (const not_implemented_error& e)
        {
            // Works as prescribed (throws upon internal call to
            // 'scale_data')
        }

        // Finalize data now (no errors since normalization and scaling
        // completed
        foo.finalize(100);
    }

    //! \brief Tests the state of the object
    void TEST_Results_Base_State()
    {
        Results_Base<> foo{};

        // Test for preprocssing
        Insist(foo.pre_processing(),
                "Failed to be in pre-processing state by default.");
        foo.set_state(process_state::pre_process);
        Insist(foo.state() == process_state::pre_process,
                "'set_state' failed to set to pre-processing.");

        // Test for processing
        foo.set_state(process_state::processing);
        Insist(foo.processing(), "Failed to recognize as in scoring state.");

        // Test for post-processing
        foo.set_state(process_state::post_process);
        Insist(foo.post_processing(),
                "Failed to recognize as in post-processing state.");
    }

    //! \brief Performs all test for the \c Results_Base object
    void TEST_Results_Base()
    {
        TEST_Results_Base_Constructor();
        TEST_Results_Base_Scaling();
        TEST_Results_Base_State();
    }


} // End of namespace TEST
} // End of namespace FET


int main()
{
    FET::TEST::TEST_Results_Base();
    return 0;
}

//---------------------------------------------------------------------------//
// end of src/Tallies/Base/test/tstResults_Base.cc
//---------------------------------------------------------------------------//
