//---------------------------------*-C++-*-----------------------------------//
/*!
 * \file   src/Tallies/Base/test/tstBasis_Tally.cc
 * \brief  Basis_FET class test.
 */
//---------------------------------------------------------------------------//

#include <memory>

#include "FET_Macros.hh"
#include "FET_Assertions.hh"
#include "FET_DBC.hh"

#include "Domain.hh"
#include "Legendre_Attr.hh"
#include "Basis_Poly.hh"
#include "Legendre_Poly.hh"
#include "Separable_FET.hh"

#include "Particle.hh"

namespace FET
{
namespace TEST
{
    //! \brief Tests construction of the \c Separable_FET object
    Separable_FET TEST_Separable_FET_Construction()
    {
        // Make some domains
        const Domain<double> dom1("x", -10.0,  0.0);
        const Domain<double> dom2("y",   0.0, 10.0);
        const Domain<double> dom3("z", -10.0, 10.0);

        // Create attributes
        const Legendre_Attr attr1("x basis set",      5, dom1);
        const Legendre_Attr attr2("y basis set",     18, dom2);
        const Legendre_Attr attr3("z basis set",      0, dom3);

        // Create basis sets
        const Legendre_Poly<> l_poly1(attr1);
        const Legendre_Poly<> l_poly2(attr2);
        const Legendre_Poly<> l_poly3(attr3);

        // Create the vector
        std::vector<std::shared_ptr<Basis_Poly<>>> basis_sets(3);
        basis_sets[0] = std::make_shared<Legendre_Poly<>>(l_poly1);
        basis_sets[1] = std::make_shared<Legendre_Poly<>>(l_poly2);
        basis_sets[2] = std::make_shared<Legendre_Poly<>>(l_poly3);

        // Construct properly now
        Separable_FET fet("Some FET", basis_sets);
        fet.reset();

        // Now test the various getters (also ensures proper construction)
        Insist(fet.type() == "Separable FET", "Object type failed.");

        // Return the object for further testing
        return fet;
    }


    //! \brief Tests the specialized implementations of \c Separable_FET
    void TEST_Separable_FET_Specialized(Basis_FET& fet)
    {
        // Test \c evaluate
        const double eval_dummy = fet.evaluate({0.0, 0.0, 0.0});
        FET_IGNORE(eval_dummy);

        // Test \c variance
        const double var_dummy = fet.variance({0.0, 0.0, 0.0});
        FET_IGNORE(var_dummy);

        // Test \c finalize
        fet.finalize(100.0);
        fet.normalize(2.0);
        try
        {
            fet.finalize(0.0);
            throw "Finalize failed to throw an error for invalid usage.";
        } catch (assertion& e)
        {
            // Successfully handled invalid normalization factor
        }
    }

    //! \brief Performs all test of the \c Separable_FET API and implementation
    void TEST_Separable_FET()
    {
        // Test construction and get object
        auto fet = TEST_Separable_FET_Construction();

        // Test specialized methods
        TEST_Separable_FET_Specialized(fet);
    }


} // End of namespace TEST
} // End of namespace FET


int main()
{
    try {
        FET::TEST::TEST_Separable_FET();
    } catch (char const* message) {
        std::cerr << message << std::endl;
        return 1;
    }
    return 0;
}

//---------------------------------------------------------------------------//
// end of src/Tallies/Base/test/tstBasis_Tally.cc
//---------------------------------------------------------------------------//
