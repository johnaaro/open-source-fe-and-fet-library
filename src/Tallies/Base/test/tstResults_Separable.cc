//---------------------------------*-C++-*-----------------------------------//
/*!
 * \file   src/Tallies/Base/test/tstResults_Separable.cc
 * \brief  Results_Separable class test.
 */
//---------------------------------------------------------------------------//

#include <iostream>

#include <cmath>
#include <stdexcept>
#include <type_traits>

#include "FET_Assertions.hh"
#include "Constants.hh"
#include "FET_DBC.hh"

#include "Results_Separable.hh"

namespace FET
{
namespace TEST
{

    //! \brief Tests the construction of the \c Results_Separable object
    void TEST_Results_Separable_Constructor()
    {
        // Test defaults
        Results_Separable<> foo{};
        Insist(foo.scaling() == Results_Separable<>::default_scaling,
                "The default scaling factor was not set properly.");

        Insist(foo.empty() && foo.size() == 0,
                "The default size was non-zero.");
    }

    //! Tests the general API of the results object wrapping the container
    void TEST_Results_Separable_API()
    {
        Results_Separable<> foo{};

        // Try inserting some data
        foo.insert("x", 12);
        foo.insert(std::make_pair("y", std::vector<double>(15)));
        Insist(foo.size() == 2, "Failed to insert 2 unique items.");

        // Clear contents and insert
        foo.reset();
        Insist(foo.empty(), "Failed to clear the object.");
        foo.insert("x", 12);
        foo.insert("y",  9);
        foo.insert("z",  5);
        Insist(foo.contains("z"), "Failed to recognize element in container.");

        // Try erasing an element
        foo.erase("z");
        Insist(!foo.contains("z"),
                "Failed to erase an element in the container.");
        Insist(!foo.contains("bar"), "Found a non-existent item.");

        // Insert an element via [] (with 20 elements) and test 'find'
        foo["z"] = std::vector<double>(20, 1.0);
        Insist(foo.at("z").size() == 20, "Insertion via [] failed.");
        foo.find("z").resize(0);
        Insist(foo.at("z").size() ==  0, "Usage of 'find' failed.");

        // Clear the object now and start scoring
        foo.clear();
        foo["x"] = std::vector<double>(12, 2.0); // A 12th order dataset in 'x'
        foo.pre_process();
        Insist(foo["x"][0] - 0.0 < tolerance,
                "Pre-processing failed to reset data values (" << foo["x"][0]);
        foo.increment("x", std::vector<double>(200, 5));
        Insist(foo["x"][0] - 5.0 < tolerance,
                "Incrementing failed to add to existing data values.");

        // Now finalize (scales by factor of 5)
        foo.finalize(5.0);
        Insist(foo.extract("x")[0] - 1.0 < tolerance,
                "Finalize and extract failed.");
    }


    //! \brief Performs all test for the \c Results_Separable object
    void TEST_Results_Separable()
    {
        TEST_Results_Separable_Constructor();
        TEST_Results_Separable_API();
    }


} // End of namespace TEST
} // End of namespace FET


int main()
{
    FET::TEST::TEST_Results_Separable();
    return 0;
}

//---------------------------------------------------------------------------//
// end of src/Tallies/Base/test/tstResults_Separable.cc
//---------------------------------------------------------------------------//
