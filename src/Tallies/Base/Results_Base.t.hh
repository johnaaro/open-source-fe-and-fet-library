//---------------------------------------------------------------------------//
/*!
 * \file  src/Tallies/Base/Results_Base.t.hh
 * \brief Results_Base template specialization.
 */
//---------------------------------------------------------------------------//
#ifndef src_Tallies_Base_Results_Base_t_hh
#define src_Tallies_Base_Results_Base_t_hh

#include <cmath>

#include "Results_Base.hh"

#include "Constants.hh"
#include "Logger.hh"
#include "States.hh"

namespace FET
{

//---------------------------------------------------------------------------//
/*!
 * \brief Finalizes the data contained by the object for post-processing
 *
 * Finalizes the encapsulated data by scaling it and moving the object to a
 * post-processed state. The internal data is meant to be unchanged once
 * finalized (i.e. in a post-processing state).
 *
 * Data is scaled in the amount of that provided here multiplied by any scaling
 * factor previously specified via \c set_scale or \c add_scaling.
 *
 * The object is moved to a post-processing state at completion of this method.
 */
template<typename T>
void Results_Base<T>::finalize(const Results_Base<T>::scaling_type val)
{
    // Check/set the current state
    if (finalized()) return;
    if (!post_processing())
    {
        if (pre_processing())
        {
            log(WARNING) << "The data has not yet been processed."
                         << flush_next(WARNING)
                         << "Moving to post-processing state...\n";
        }
        set_state(state_type::post_process);
    }

    // Apply scaling factors:
    scale(scaling() * val);
}


//---------------------------------------------------------------------------//
/*!
 * \brief Scales all the data by the pre-specified scaling factor
 *
 * Scales the encapsulated data by the object's specified scaling factor. The
 * scaling factor may be specified prior by using the \c set_scale or \c
 * add_scaling methods. A scaling factor may also be provided here to perform a
 * simple scaling of all data at any point.
 *
 * The scaling factor set by \c set_scale or \c add_scaling is used for scaling
 * when no argument is provided, otherwise data is only scaled by the argument.
 *
 * The object is moved to a post-processing state at completion of this method.
 */
template<typename T>
void Results_Base<T>::scale(Results_Base<T>::scaling_type val)
{
    // Move to post-processing state
    if (!post_processing())
    { set_state(state_type::post_process); }

    // Set scaling factor when default is used
    if (std::abs(val - Results_Base<T>::default_scaling) <= tolerance)
    { val = scaling(); }

    // Warn on subsequent scaling attempts
    if (scaled())
    {
        log(WARNING) << "Performing a subsequent scaling (value = "
                     << val << ").\n";
    }
    else
    { d_scaled = true; }

    // Don't scale if scaling factor is close to 1.0
    if (std::abs(val - 1.0) <= tolerance) return;
    scale_data(val);
}


} // end namespace FET

#endif // src_Tallies_Base_Results_Base_t_hh
//---------------------------------------------------------------------------//
// end of src/Tallies/Base/Results_Base.t.hh
//---------------------------------------------------------------------------//
