//---------------------------------------------------------------------------//
/*!
 * \file  src/Tallies/Base/Basis_FET.cc
 * \brief Basis FET class declaration.
 */
//---------------------------------------------------------------------------//

#include <iostream>

#include "FET_DBC.hh"
#include "Basis_FET.hh"

namespace FET
{
//---------------------------------------------------------------------------//
/*!
 * \brief Validates that a basis set is supported and does not exist yet
 *
 * Ensures that the basis set is supported by the FET objects and (b) does not
 * already exist within the FET's basis sets.
 */
/*
void Basis_FET::validate_basis_set(
        const Basis_FET::SP_Basis_Poly basis_set) const
{
    // Require that domain be valid by FET usage
    Insist (basis_set->attr()->domain().label() == "x" ||
            basis_set->attr()->domain().label() == "y" ||
            basis_set->attr()->domain().label() == "z" ||
            basis_set->attr()->domain().label() == "r" ||
            basis_set->attr()->domain().label() == "theta" ||
            basis_set->attr()->domain().label() == "phi",
            "An basis set on the " << basis_set->attr()->domain().label()
                << " domain is not supported within FETs.");

    // Ensure the domain does NOT already exist
    Insist (!domain_exists(basis_set->attr()->domain().label()),
            "The " << basis_set->attr()->domain().label()
                << " domain cannot be duplicated within the FET.");
}
*/

//---------------------------------------------------------------------------//
/*!
 * \brief Adds a vector of basis sets to the FET object
 */
/*
void Basis_FET::add_basis_sets(
        const std::vector<std::shared_ptr<Basis_Poly<>>> basis_sets)
{
    // Add all non-null pointing shared_ptrs to the basis set vector
    for (unsigned int i = 0; i < basis_sets.size(); i++)
    {
        if (!(basis_sets[i] == nullptr )) add_basis_set(basis_sets[i]);
    }
}
*/

//---------------------------------------------------------------------------//
/*!
 * \brief Adds a basis set to the FET after validating it
 */
/*
void Basis_FET::add_basis_set(const Basis_FET::SP_Basis_Poly basis_set)
{
    // Validate the basis set
    validate_basis_set(basis_set);

    // Add the basis set
    d_basis.push_back(basis_set);

    // Add the domain label quick reference
    d_domain.push_back(basis_set->attr()->domain().label());
}
*/

//---------------------------------------------------------------------------//
/*!
 * \brief Returns a basis set with the desired domain label
 */
/*
Basis_FET::SP_Basis_Poly Basis_FET::basis(
        const std::string label) const noexcept
{
    // Ensure the label exists in the FET
    Require(domain_exists(label));

    // Obtain index of the basis set
    const auto label_it = std::find(d_domain.begin(), d_domain.end(), label);
    const auto index = unsigned(int(
                std::distance(d_domain.begin(), label_it)));

    // Return the basis set
    return basis(index);
}
*/

//---------------------------------------------------------------------------//
/*!
 * \brief Returns a boolean flag if the point is within the domain of the FET
 *
 * Returns a flag for if the location provided lies within the FET's domain.
 * It is assumed that the location has dimensions maching that of the FET.
 * Given this notation, it is best that software clients are careful when
 * establshing the FET objects to ensure valid use cases are given.
 *
 * As an example, a Basis_FET with basis sets having dimensions in x, z, and y
 * will assume the location is given as {x, z, y}.
 *
 * In the event the FET object will use non-Cartesian coordinates, the client
 * is expected to transform the location into one compatible with the location
 * of the FET object.
 */
/*
bool Basis_FET::contains(const Basis_FET::Vec_Dbl location) const noexcept
{
    // Ensure the size is that of the FET
    Require(location.size() == basis().size());

    // Iterate over each domain and verify if within
    for (unsigned int i = 0; i < location.size(); i++)
    {
        if (!basis(i)->attr()->domain().contains(location[i]))
        {
            return false;
        }
    }
    return true;
}
*/

//---------------------------------------------------------------------------//
} // end namespace FET

//---------------------------------------------------------------------------//
// end of src/Tallies/Base/Basis_FET.cc
//---------------------------------------------------------------------------//
