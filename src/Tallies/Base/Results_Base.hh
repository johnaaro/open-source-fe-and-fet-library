//---------------------------------------------------------------------------//
/*!
 * \file  src/Tallies/Base/Results_Base.hh
 * \brief Results_Base class declaration.
 */
//---------------------------------------------------------------------------//
#ifndef src_Tallies_Base_Results_Base_hh
#define src_Tallies_Base_Results_Base_hh

#include <iostream>
#include <string>
#include <vector>

#include "FET_Macros.hh"
#include "FET_DBC.hh"
#include "States.hh"

namespace FET
{

//===========================================================================//
/*!
 * \class Results_Base
 * \brief General structure and API of FET results
 *
 * Provides a general structure and API for results to FETs. This base class
 * provides common functionality that will be desired in objects containing
 * actual data.
 *
 * \note Usage of the object's various API methods may characterize the
 * state of the data.
 */
/*!
 * \example src/Tallies/Base/test/tstResults_Base.cc
 *
 * Test of the Results_Base object
 */
//===========================================================================//
template<typename T = double>
class Results_Base
{
public:

    //@{
    //! Public type aliases
    using data_type        = T;
    using key_type         = std::string;
    using scaling_type     = data_type;
    using state_type       = FET::process_state;
    //@}

    //! \brief The default normalization factor (i.e. divisor of results)
    static constexpr data_type default_scaling = 1;

    //! \brief The default state of the object
    static constexpr state_type default_state = state_type::pre_process;


private:
    // >>> IMPLEMENTATION DATA

    //! \brief Normalization type
    data_type d_scaling = default_scaling;

    //! \brief Current state (pre-processing, processing, post-processing)
    state_type d_state = default_state;

    //! \brief Flags if data has been scaled
    bool d_scaled = false;


public:
    // >>> CONSTRUCTOR/DESTRUCTORS

    //! Default Constructor
    Results_Base(const scaling_type scaling = default_scaling)
        : d_scaling(scaling)
    { /* * */ }

    //@{
    //! Copy control (copy, move, and destructor)
    Results_Base(const Results_Base&) = default;
    Results_Base& operator=(const Results_Base&) = default;
    Results_Base(Results_Base&&) = default;
    Results_Base& operator=(Results_Base&&) = default;
    virtual ~Results_Base() = default;
    //@}


public:
    // >>> GETTERS

    //! Returns the current scaling factor
    const scaling_type scaling() const noexcept
    { return d_scaling; }

    //! Returns a flag for if the data has been scaled
    bool scaled() const noexcept
    { return d_scaled; }

    //! Flags if the data has been finalized
    bool finalized() const noexcept
    { return scaled(); }

    //! Returns the current state of the object
    state_type state() const noexcept
    { return d_state; }


public:
    // >>> SETTERS

    //! \brief Resets the underlying data structure by clearing it's contents
    virtual void reset() noexcept
    { set_state(state_type::pre_process); }

    //! Sets the current state of the object
    void set_state(const state_type new_state) noexcept
    { d_state = new_state; }

    //! Resets the scaling factor to that provided
    void set_scaling(const scaling_type new_scale) noexcept
    { d_scaling = new_scale; }

    //! Adds another scaling factor for the data
    void add_scaling(const scaling_type added_scaling) noexcept
    { set_scaling(scaling() * added_scaling); }


public:
    // >>> INTROSPECTION

    //! Returns if the object is in a pre-processing state
    bool pre_processing() const noexcept
    { return state() == state_type::pre_process; }

    //! Returns if the object is in a processing state
    bool processing() const noexcept
    { return state() == state_type::processing; }

    //! Returns if the object is in a post-processing state
    bool post_processing() const noexcept
    { return state() == state_type::post_process; }

    //! Finalizes the object for post-procesing (scales data)
    void finalize(const scaling_type val = default_scaling);

    //! Scales all the data by the current scaling factor
    void scale(scaling_type val = default_scaling);

    //! Describes the object
    virtual void describe(std::ostream& = std::cout) const
    { Not_Implemented("describe"); }


protected:

    //! Performs the scaling of internal data (dependent on data structure(s))
    virtual void scale_data(const scaling_type val)
    {
        Not_Implemented("scale_data");
        FET_IGNORE(val);
    }


}; // end class Results_Base

 //---------------------------------------------------------------------------//
} // end namespace FET

//---------------------------------------------------------------------------//
// Inline and Template instantiations
//---------------------------------------------------------------------------//

// #include "Results_Base.i.hh"
#include "Results_Base.t.hh"

//---------------------------------------------------------------------------//
#endif // src_Tallies_Base_Results_Base_hh
//---------------------------------------------------------------------------//
// end of src/Tallies/Base/Results_Base.hh
//---------------------------------------------------------------------------//
