//---------------------------------------------------------------------------//
/*!
 * \file  src/FET/Manager_FET/Tallies/Convolved_FET.hh
 * \brief Convolved_FET class declaration.
 */
//---------------------------------------------------------------------------//
#ifndef src_FET_Manager_FET_Tallies_Convolved_FET_hh
#define src_FET_Manager_FET_Tallies_Convolved_FET_hh

#include <iostream>



namespace FET
{
//===========================================================================//
/*!
 * \class Convolved_FET
 * \brief Structure of cartesian basis sets for utilization in FETs
 *
 * \example
 */
//===========================================================================//
class Convolved_FET
{
public:
    //@{
    //! Public type aliases
    using Vec_Int           = std::vector<int>;
    using Vec_Dbl           = std::vector<double>;
    using Vec_Dbl_2         = std::vector<Vec_Dbl>;
    using Vec_Dbl_3         = std::vector<Vec_Dbl_2>;
    //@}

public:

    // Constructor
    Results_Convolved();

    //! Destructor
    virtual ~Results_Convolved();

    // >>> CONSTRUCTION/SETTERS
    void set_num_dimensions(int num_dimensions_)
    { num_dimensions = num_dimensions_; }

    void set_size_single_dimension(int dimension_, int size_)
    {
        switch(dimension)
        {
            case 0:
                d_convolved_results.resize(size_);
                break;
            case 1:
                for (auto &y : d_convolved_coefficients)
                    y.resize(size_);
                break;
            case 2:
                for (auto &y : d_convolved_coefficients)
                    for (auto &z : y)
                        z.resize(size_);
                break;
        }
    }

    void set_size_all_dimensions(int size_)
    {
        d_convolved_coefficients.resize(size_);
        for (auto &y : d_convolved_coefficients)
        {
            y.resize(size_);
            for (auto &z : y)
                z.resize(size_)
        }
    }

    // >>> INTROSPECTION/GETTERS

private:
    // >>> IMPLEMENTATION FUNCTIONS


private:
    // >>> IMPLEMENTATION DATA
    Vec_Dbl_3 d_convolved_coeffcients;
    int num_dimensions



}; // end class Results_Convolved

//---------------------------------------------------------------------------//
} // end namespace FET

//---------------------------------------------------------------------------//
#endif // src_FET_Manager_FET_Tallies_Convolved_FET_hh
//---------------------------------------------------------------------------//
// end of src/FET/Manager_FET/Tallies/Convolved_FET.hh
//---------------------------------------------------------------------------//
