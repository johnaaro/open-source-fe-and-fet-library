//---------------------------------*-C++-*-----------------------------------//
/*!
 * \file   src/Experimental/Template.hh
 * \brief  Template class declaration
 */
//---------------------------------------------------------------------------//
#ifndef src_Experimental_Template_hh
#define src_Experimental_Template_hh

// std:: headers
#include <iostream>
#include <string>

// std::.h headers


// TPL headers


// Project headers
#include "FET_DBC.hh"

namespace FET
{
//===========================================================================//
/*!
 * \class Template
 * \brief Provides a template class structure and layout for reference
 *
 * Provides a template class structure that can be copied to generate new
 * objects and features for the project.
 */
/*!
 * \example test/tstTemplate.cc
 *
 * Tests the Template object
 *
 */
//===========================================================================//
class Template
{
  public:
    // Public type aliases
    //@{
    //! Public type aliases
    using string         = std::string;
    //@}

    // Default values
    static constexpr double example = 3.14159;


  private:
    // >>> IMPLEMENTATION DATA

    //! \brief Sample member documentation
    string b_sample = ""; // Provide initializer where possible


  public:
    // >>> CONSTRUCTION

    //! \brief Simple constructor using both arguments
    Template(const string& s)
        : b_sample(s)
    { /* * */ }

    //@{
    //! Copy control (default, copy, move, and destructor)
    Template(const Template&) = default;
    Template& operator=(const Template&) = default;
    Template(Template&&) = default;
    Template& operator=(Template&&) = default;
    virtual ~Template() = default;
    //@}


  public:
    // >>> SETTERS


    //! \brief Sample method documentation (brief)
    // Note: 'noexcept' is used everywhere possible to aid in efficiency
    // Note: 'const' parameters ought to be used
    void set_sample(const string& s) // lack of 'noexcept' due to 'Require()'
    {
        // Validate member data prior to setting it
        Require(!s.empty());
        b_sample = s;
    }


  public:
    // >>> GETTERS

    //! \brief Returns the value stored
    // Note: 'noexcept' is used everywhere possible to aid in efficiency
    // Note: 'const' is used everywhere possible to ensure safety
    const string& sample() const noexcept
    { return b_sample; }


  public:
    // >>> INSTROSPECTION

    //! \brief Method that uses data, but doesn't set it
    // Note: these will likely be mostly 'const noexcept'
    void describe() const noexcept
    { std::cout << sample() << "\n"; }


}; // end class Template

//---------------------------------------------------------------------------//
} // end namespace FET

//---------------------------------------------------------------------------//
// INLINE AND TEMPLATE SPECIALIZATIONS
//---------------------------------------------------------------------------//

#include "Template.i.hh"
#include "Template.t.hh"

//---------------------------------------------------------------------------//
#endif // src_Experimental_Template_hh
//---------------------------------------------------------------------------//
// end of src/Experimental/Template.hh
//---------------------------------------------------------------------------//
