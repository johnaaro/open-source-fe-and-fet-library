//---------------------------------------------------------------------------//
/*!
 * \file  src/harness/Constants.hh
 * \brief Declares various constants used in the FET library
 * \note  Copyright (c) 2019 Oak Ridge National Laboratory, UT-Battelle, LLC.
 */
//---------------------------------------------------------------------------//
#ifndef src_harness_Constants_hh
#define src_harness_Constants_hh

namespace FET
{

  //@{
  //! \brief Whole numbers
  static constexpr double
      zero       = 0.0,
      one        = 1.0,
      two        = 2.0,
      three      = 3.0,
      four       = 4.0,
      five       = 5.0,
      six        = 6.0,
      seven      = 7.0,
      eight      = 8.0,
      nine       = 9.0,
      ten        = 10.0,
      hundrend   = 100.0,
      thousand   = 1000.0,
      million    = 1000000.0;
  //@}


  //@{
  //! \brief Fractions
  static constexpr double
      one_half       = one / two,
      one_third      = one / three,
      one_fourth     = one / four,
      one_fifth      = one / five,
      one_sixth      = one / six,
      one_seventh    = one / seven,
      one_eighth     = one / eight,
      one_tenth      = one / ten,
      one_hundreth   = one / hundrend,
      one_thousandth = one / thousand,
      one_millionth  = one / million;
  //@}


  //@{
  //! \brief Physical constants
  static constexpr double
      pi          = 3.14159265358979323846;
  //@}


  //@{
  //! \brief Derived constants
  static constexpr double
      two_pi      = two * pi,
      four_pi     = four * pi,
      inv_pi      = one / pi,
      inv_two_pi  = one / two_pi,
      inv_four_pi = one / four_pi;
  //@}


  //! \brief Tolerance
  //! Tolerance limits for divide by zero, real number comparisons, etc.
  static constexpr double
      tolerance        = 1.0E-12,
      div_zero_limit   = tolerance;

//---------------------------------------------------------------------------//
} // end namespace FET

//---------------------------------------------------------------------------//
#endif // src_harness_Constants_hh
//---------------------------------------------------------------------------//
// end of src/harness/Constants.hh
//---------------------------------------------------------------------------//
