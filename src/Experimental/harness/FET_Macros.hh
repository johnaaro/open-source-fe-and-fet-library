//---------------------------------------------------------------------------//
/*!
 * \file  src/harness/FET_Macros.hh
 * \brief Declares various macros for the FET library
 * \note  Copyright (c) 2019 Oak Ridge National Laboratory, UT-Battelle, LLC.
 */
//---------------------------------------------------------------------------//
#ifndef src_harness_FET_Macros_hh
#define src_harness_FET_Macros_hh

namespace FET
{

//============================================================================//
/*!
 * \def FET_IGNORE(VAR)
 *
 * Mark the variable as ignored by the compiler
 *
 * This macro simply \c (void)'s the variable given to it to not generate
 * warnings by the compiler when \c -Wall or \c -Wextra flags are used
 */
//============================================================================//
#define FET_IGNORE(var) \
    (void)sizeof(var)


//============================================================================//
/*!
 * \def FET_UNLIKELY(condition)
 *
 * Mark the results of this condition to be "unlikely" for efficiency purposes
 *
 * This asks the compiler to move the section of code to a "cold" part of the
 * instructions, improving instruction locality. It should be used primarily
 * for error checking conditions.
 */
//============================================================================//
#if defined(__clang__) || defined(__GNUC__)
    // GCC and Clang support the same builtin
    #define FET_UNLIKELY(COND) __builtin_expect(!!(COND), 0)
#else
    // No other compilers have similar builtin
#define FET_UNLIKELY(COND) COND
#endif


//============================================================================//
/*!
 * \def FET_UNREACHABLE
 *
 * Mark a point in code as being impossible to reach in normal execution.
 */
//============================================================================//
#if defined(__clang__)
#define FET_UNREACHABLE __builtin_unreachable()
#elif defined(__GNUC__)
    #if __GNUC__ >= 7
        #define FET_UNREACHABLE __builtin_unreachable()
    #else
        // Workaround for GCC bug
        #define FET_UNREACHABLE __builtin_trap()
    #endif
#elif defined(_MSC_VER)
#define FET_UNREACHABLE __assume(false)
#else
#define FET_UNREACHABLE
#endif


//============================================================================//
/*!
 * \def FET_NORETURN
 *
 * Mark a function as *never* returning normally: it always calls 'throw',
 * 'abort', 'exit', etc.
 *
 * This attribute should be applied to helper functions for throwing error
 * messages.
 */
//============================================================================//
#if defined(__clang__) || defined(__GNUC__)
#define FET_NORETURN __attribute__((noreturn))
#elif defined(_MSC_VER)
#define FET_NORETURN __declspec(noreturn)
#else
#define FET_NORETURN
#endif


//---------------------------------------------------------------------------//
} // end namespace FET

//---------------------------------------------------------------------------//
#endif // src_harness_FET_Macros_hh
//---------------------------------------------------------------------------//
// end of src/harness/FET_Macros.hh
//---------------------------------------------------------------------------//
