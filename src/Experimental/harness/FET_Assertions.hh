//---------------------------------------------------------------------------//
/*!
 * \file  src/harness/Assertion.hh
 * \brief Assertion method declaration.
 * \note  Copyright (c) 2019 Oak Ridge National Laboratory, UT-Battelle, LLC.
 */
//---------------------------------------------------------------------------//
#ifndef src_harness_Assertion_hh
#define src_harness_Assertion_hh

#include <sstream>
#include <stdexcept>
#include <string>

#include "FET_Macros.hh"

namespace FET
{

//===========================================================================//
/*!
 * \def Assertion_Level
 * \brief Flags to print file and line of an assertion when thrown
 *
 * The \c Assertion_Level macro specifies whether or not the file and line
 * associated with the assertion will be printed when thrown. This is OFF for
 * Release builds (=0), and ON for DEBUG builds (=1).
 *
 * The \c Assertion_Level defaults to 1 for Debug builds.
 *
 * \note The FET library uses CMake to define this variable. The definition
 * should \em never need to declared here.
 */
//===========================================================================//
#if !defined(Assertion_Level)
#define Assertion_Level 1
#endif

//===========================================================================//
/*!
 * \class assertion
 * \brief Exception notification class for FET specific assertions.
 *
 * This class is derived from std::runtime_error.  In fact, this class
 * provides no significant change in functionality from std::runtime_error.
 * This class provides the following features in addition to those found in
 * std::runtime_error:
 *
 * -# FET::assertion does provide an alternate constructor that allows us
 *    to automatically insert file name and line location into error messages.
 * -# It provides a specialized form of std::runtime_error.  This allows
 *    FET code to handle FET specific assertions differently from
 *    generic C++ or STL exceptions.  For example
 *    \code
 *    try
 *    {
 *       throw FET::assertion( "My error message." );
 *    }
 *    catch ( FET::assertion &a )
 *    {
 *       // Catch FET exceptions first.
 *       cout << a.what() << endl;
 *       exit(1);
 *    }
 *    catch ( std::runtime_error &e )
 *    {
 *       // Catch general runtime_errors next
 *       cout << e.what() << endl;
 *    }
 *    catch ( ... )
 *    {
 *       // Catch everything else
 *        exit(1);
 *    }
 *    \endcode
 *
 * \note Assertion should always be thrown as objects on the stack and caught
 *       as references.
 *
 * \sa \ref Assertion_Level
 */
/*!
 * \example src/harness/test/tstAssertion.cc
 *
 * Assertion and Assertion examples.
 */
//===========================================================================//
    class assertion : public std::logic_error
    {
        typedef std::logic_error Base;
    public:

        //! \brief  Default constructor for ds++/assertion class.
        explicit assertion( std::string const & msg )
                : Base ( msg )
        {
            /* empty */
        }

        //! \brief Specialized constructor for FET::assertion class.
        assertion(
                std::string const & cond,
                std::string const & file,
                int const line )
                : Base (build_message( cond, file, line ) )
        {
            /* empty */
        }

    private:
        //! \brief Helper function to build error message
        std::string build_message( std::string const & cond,
                                              std::string const & file,
                                              int         const line) const
        {
            std::ostringstream myMessage;
            myMessage << "Assertion failure: " << cond
                      << "\n ^^^ at " << file << ":" << line;
            return myMessage.str();
        }
    };


//===========================================================================//
/*!
 * \class not_implemented_error
 * \brief Specialization for un-implemented features.
 *
 * Providing a thin specialization means this assertion can be handled
 * separately, perhaps with an extra apology to the user.
 */
//===========================================================================//
    class not_implemented_error : public assertion
    {
        typedef assertion Base;
    public:
        //! \brief Constructor
        not_implemented_error(
                const char* const msg,
                const char* const file,
                const int         line )
                : Base(build_notimpl_message(msg, file, line))
        {
            /* empty */
        }

    private:
        //! \brief Builds the error message
        std::string build_notimpl_message(
                const char* const msg,
                const char* const file,
                const int         line)
        {
            std::ostringstream out;
            out << "Regrettably, " << msg << " is not currently implemented.";
    #if Assertion_Level > 0
            out << "\n ^^^ at " << file << ":" << line;
    #else
            // Debug mode is off, so don't trouble the user with the particulars
        FET_IGNORE(file);
        FET_IGNORE(line);
    #endif

            return out.str();
        }
    };


//===========================================================================//
/*!
 * \class not_configured_error
 * \brief Specialization for un-configured features.
 *
 * Providing a thin specialization means this assertion can be handled
 * separately, perhaps with an extra apology to the user.
 */
//===========================================================================//
    class not_configured_error : public assertion
    {
        typedef assertion Base;
    public:
        //! \brief Constructor
        not_configured_error(
                const char* const msg,
                const char* const file,
                const int         line )
                : Base(build_notconf_message(msg, file, line))
        {
            /* empty */
        }

    private:
        //! \brief Build the error message
        std::string build_notconf_message(
                const char* const msg,
                const char* const file,
                const int         line)
        {
            std::ostringstream out;
            out << "Support for " << msg << " is not enabled in this build "
                << "configuration.";
    #if Assertion_Level > 0
            out << "\n ^^^ at " << file << ":" << line;
    #else
            // Debug mode is off, so don't trouble the user with the particulars
        FET_IGNORE(file);
        FET_IGNORE(line);
    #endif

            return out.str();
        }
    };


//===========================================================================//
/*!
 * \class validation_error
 * \brief Specialization for value validation
 *
 * Providing a thin specialization means this assertion can be handled
 * separately (e.g. raising a ValueError in the Python wrapper)
 */
//===========================================================================//
    class validation_error : public assertion
    {
        typedef assertion Base;
    public:
        //! \brief Explicit constructor
        explicit validation_error(
                const std::string& msg)
                : Base(msg)
        {
            /* empty */
        }
    };


//===========================================================================//
/*!
 * \class interrupt_error
 * \brief Indicate that a user keyboard interrupt was not handled.
 */
//===========================================================================//
    class interrupt_error : public std::runtime_error
    {
        typedef std::runtime_error Base;
    public:
        //! \brief Explicit constructor
        explicit interrupt_error()
                : Base("Unhandled keyboard interrupt in the FET library")
        {
            /* * */
        }
    };


//---------------------------------------------------------------------------//
} // end namespace FET

//---------------------------------------------------------------------------//
#endif // src_harness_Assertion_hh
//---------------------------------------------------------------------------//
// end of src/harness/Assertion.hh
//---------------------------------------------------------------------------//
