////---------------------------------------------------------------------------//
///*!
// * \file  src/harness/test/tstComparisons.cc
// * \brief Test of the comparison functions
// * \note  Copyright (c) 2019 Oak Ridge National Laboratory, UT-Battelle, LLC.
// */
////---------------------------------------------------------------------------//
//
//#include "Comparisons.hh"
//#include "FET_DBC.hh"
//
//namespace FET
//{
//    namespace TEST
//    {
//
//        //! \brief Tests the various comparison functions
//        void TEST_Comparisons()
//        {
//            // Create some explicit types
//            const int         foo_i = 1;
//            const int64_t     foo_li = 1;
//            const float       foo_f = 1.0;
//            const double      foo_d = 1.0;
//            const long double foo_ld = 1.0;
//
//            // Now perform comparisions
//            Insist(is_equivalent(foo_i, 1), "Exact integer comparison failed.");
//            Insist(is_equivalent(foo_li, 1),
//                   "Exact long integer comparison failed.");
//            Insist(is_equivalent(foo_i - 1), "Default comparison failed.");
//            Insist(is_approximate(foo_f, 1),
//                   "Approximate float comparison failed.");
//            Insist(is_approximate(foo_d, 1),
//                   "Approximate double comparison failed.");
//            Insist(is_approximate(foo_ld, 1),
//                   "Approximate long double comparison failed.");
//            Insist(is_approximate(foo_f - 1),
//                   "Default comparison failed for decimal type.");
//            Insist(is_approximate(foo_ld, 0.0, 1.1),
//                   "Approximate float comparison failed.");
//
//            // Now check for failure to match
//            Insist(!is_equivalent(1, 5), "Failed to find inequivalency.");
//            Insist(!is_approximate(5.0, 10.0, 1.0),
//                   "Failed to find not approximate.");
//        }
//
//
//    } // End of namespace TEST
//} // End of namespace FET
//
//
//int main()
//{
//    FET::TEST::TEST_Comparisons();
//    return 0;
//}
//
////---------------------------------------------------------------------------//
//// end of src/harness/test/tstComparisons.cc
////---------------------------------------------------------------------------//



//---------------------------------------------------------------------------//
/*!
 * \file  src/harness/test/tstComparisons.cc
 * \brief Test of the comparison functions
 * \note  Copyright (c) 2019 Oak Ridge National Laboratory, UT-Battelle, LLC.
 */
//---------------------------------------------------------------------------//

#include "Comparisons.hh"
#include "FET_DBC.hh"

namespace FET
{
namespace TEST
{

    //! \brief Tests the various comparison functions
    void TEST_Comparisons()
    {
        // Create some explicit types
        const int         foo_i = 1;
        const int64_t     foo_li = 1;
        const float       foo_f = 1.0;
        const double      foo_d = 1.0;
        const long double foo_ld = 1.0;

        // Now perform comparisions
        Insist(is_equivalent(foo_i, 1), "Exact integer comparison failed.");
        Insist(is_equivalent(foo_li, 1),
                "Exact long integer comparison failed.");
        Insist(is_equivalent(foo_i - 1), "Default comparison failed.");
        Insist(is_approximate(foo_f, 1),
                "Approximate float comparison failed.");
        Insist(is_approximate(foo_d, 1),
                "Approximate double comparison failed.");
        Insist(is_approximate(foo_ld, 1),
                "Approximate long double comparison failed.");
        Insist(is_approximate(foo_f - 1),
                "Default comparison failed for decimal type.");
        Insist(is_approximate(foo_ld, 0.0, 1.1),
                "Approximate float comparison failed.");

        // Now check for failure to match
        Insist(!is_equivalent(1, 5), "Failed to find inequivalency.");
        Insist(!is_approximate(5.0, 10.0, 1.0),
                "Failed to find not approximate.");
    }


} // End of namespace TEST
} // End of namespace FET


int main()
{
    FET::TEST::TEST_Comparisons();
    return 0;
}

//---------------------------------------------------------------------------//
// end of src/harness/test/tstComparisons.cc
//---------------------------------------------------------------------------//
