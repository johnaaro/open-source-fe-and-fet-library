//---------------------------------------------------------------------------//
/*!
 * \file  src/harness/test/tstFET_FET_DBC.cc
 * \brief Test of the FET_FET_DBC implementation
 * \note  Copyright (c) 2019 Oak Ridge National Laboratory, UT-Battelle, LLC.
 */
//---------------------------------------------------------------------------//

#include <iostream>
#include <stdexcept>

#include "FET_Assertions.hh"
#include "FET_DBC.hh"

namespace FET
{
namespace TEST
{

    //! \brief Tests the \c FET_FET_DBC_Level is within limits
    void TEST_FET_FET_DBC_level()
    {
        if (!(0 <= FET_DBC_Level && FET_DBC_Level <= 7))
        {
            throw std::out_of_range("In invalid FET_FET_DBC level was specified!");
        }
        return;
    }


    //! \brief test the \c FET_ASSERT_ macro
    void TEST_FET_FET_DBC_assert()
    {
        try {
            FET_ASSERT_(false);
        } catch (assertion& e) {
            // caught correct thrown value
        } catch (...) {
            throw std::logic_error("The fet_assert macro failed!");
        }
        return;
    }


    //! \brief test the \c FET_NOASSERT_ macro
    void TEST_FET_FET_DBC_noassert()
    {
        try {
            FET_NOASSERT_(false);
        } catch (...) {
            throw std::logic_error("the fet_noassert macro failed!");
        }
        return;
    }


    //! \brief Tests the \c macro
    void TEST_FET_FET_DBC_Require()
    {
        // Try for valid:
        try {
            Require(true);
        } catch (...) {
            throw std::logic_error("A valid require failed!");
        }

        // Try for invalid:
        try {
            Require(false);
        } catch (assertion& e) {
            // Caught
        } catch (...) {
            throw std::logic_error("An invalid require didn't throw properly!");
        }

        // Macro is off or behaved correctly
        return;
    }


    //! \brief Tests the \c Check macro
    void TEST_FET_FET_DBC_Check()
    {
        // Try for valid:
        try {
            Check(true);
        } catch (...) {
            throw std::logic_error("A valid check failed!");
        }

        // Try for invalid:
        try {
            Check(false);
        } catch (assertion& e) {
            // Caught
        } catch (...) {
            throw std::logic_error("An invalid check didn't throw properly!");
        }

        // Macro is off or behaved correctly
        return;
    }


    //! \brief Tests the \c Ensure macro
    void TEST_FET_FET_DBC_Ensure()
    {
        // Try for valid:
        try {
            Ensure(true);
        } catch (...) {
            throw std::logic_error("A valid ensure failed!");
        }

        // Try for invalid:
        try {
            Ensure(false);
        } catch (assertion& e) {
            // Caught
        } catch (...) {
            throw std::logic_error("An invalid ensure didn't throw properly!");
        }

        // Macro is off or behaved correctly
        return;
    }


    //! \brief Tests the \c Remember macro
    void TEST_FET_FET_DBC_Remember()
    {
        Remember(int x = 5;)
        // ...
        Ensure(x == 5);
        return;
    }


    //! \brief Tests the \c Insist macro
    void TEST_FET_FET_DBC_Insist()
    {
        // Try for valid:
        Insist(true, "Checking for true");

        // Try for invalid:
        try {
            Insist(false, "Checking for false");
        } catch (assertion& e) {
            // Caught
            return;
        } catch (...) {
            throw std::logic_error("An invalid insist didn't throw properly!");
        }
        return;
    }


    //! \brief Tests the \c Not_Implemented macro
    void TEST_FET_FET_DBC_not_Implemented()
    {
        try {
            Not_Implemented("Some feature");
        } catch (assertion& e) {
            // Caught
            return;
        }
        throw std::logic_error("Not_Implemented failed to work properly!");
        return;
    }


    //! \brief Tests the \c Not_Configured macro
    void TEST_FET_FET_DBC_not_Configured()
    {
        try {
            Not_Configured("Some feature");
        } catch (assertion& e) {
            // Caught
            return;
        }
        throw std::logic_error("Not_Configured failed to work properly!");
        return;
    }


    //! \brief Tests the \c Not_Reachable macro
    void TEST_FET_FET_DBC_not_Reachable()
    {
#if FET_FET_DBC_Level > 0
        // Do stuff...
        try
        {
        Not_Reachable();
        }
        catch (assertion& e)
        {
            // Caught
            return;
        }
        throw std::logic_error("Not_Reachable failed to work properly!");
#endif
        return;
        Not_Reachable();
    }


    //! \brief Tests the \c Validate macro
    void TEST_FET_FET_DBC_Validate()
    {
        // Try for valid:
        Validate(true, "A 'true' validation case");

        // Try for not valid:
        try
        {
            Validate(false, "A 'false' validation case");
        }
        catch (assertion& e) {
            // Caught
            return;
        }
        throw std::logic_error(
                "An invalid 'Validate' call did not throw as expected!");
        return;
    }


    //! \brief Tests all FET_FET_DBC macros
    void TEST_FET_FET_DBC()
    {
        TEST_FET_FET_DBC_level();
        TEST_FET_FET_DBC_assert();
        TEST_FET_FET_DBC_noassert();
        TEST_FET_FET_DBC_Require();
        TEST_FET_FET_DBC_Check();
        TEST_FET_FET_DBC_Ensure();
        TEST_FET_FET_DBC_Remember();
        TEST_FET_FET_DBC_Insist();
        TEST_FET_FET_DBC_not_Implemented();
        TEST_FET_FET_DBC_not_Configured();
        TEST_FET_FET_DBC_not_Reachable();
        TEST_FET_FET_DBC_Validate();
    }


} // End of namespace TEST
} // End of namespace FET


int main()
{
    FET::TEST::TEST_FET_FET_DBC();
    return 0;
}

//---------------------------------------------------------------------------//
// end of src/harness/test/tstDomain.cc
//---------------------------------------------------------------------------//
