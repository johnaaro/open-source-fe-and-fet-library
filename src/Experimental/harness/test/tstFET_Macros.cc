//---------------------------------------------------------------------------//
/*!
 * \file  src/harness/test/tstFET_FET_Macros.cc
 * \brief Test of the Macro implementations
 * \note  Copyright (c) 2019 Oak Ridge National Laboratory, UT-Battelle, LLC.
 */
//---------------------------------------------------------------------------//

#include <stdexcept>

#include "FET_Macros.hh"

namespace FET
{
namespace TEST
{
    //! \brief Tests the \c FET_IGNORE macro
    //! \note An error will be generated ONLY IN DEBUG MODE
    void TEST_Macro_IGNORE(const int i, const double x)
    {
        FET_IGNORE(i);
        FET_IGNORE(x);
    }


    //! \brief Tests the \c FET_UNLIKELY macro
    //! \note This test ensures that \c FET_UNLIKELY reduces to a boolean
    void TEST_Macro_UNLIKELY()
    {
        if (FET_UNLIKELY(false))
        {
            // Do something here...
        }
        else
        {
            // Do something here...
        }
    }


    //! \brief Tests the \c FET_UNREACHABLE macro
    void TEST_Macro_UNREACHABLE()
    {
        if (true)
        {
            return;
        }
        FET_UNREACHABLE;

        // This should not be reached for compilers with built-ins
        throw std::out_of_range("The UNREACHABLE macro failed.");
    }


    //! \brief Tests the \c FET_NORETURN macro
    FET_NORETURN
        void TEST_Macro_NORETURN()
    {
        throw "No return works";
    }


    //! \brief Runs all Macro test
    void TEST_FET_FET_Macros()
    {
        TEST_Macro_IGNORE(1, 10.0);
        TEST_Macro_UNLIKELY();
        TEST_Macro_UNREACHABLE();
        try
        {
            TEST_Macro_NORETURN();
        }
        catch (char const* e)
        {
        }
        catch (...)
        {
            throw std::logic_error(
                    "The NORETURN macro did not function properly!");
        }
    }


} // End of namespace TEST
} // End of namespace FET


int main()
{
    FET::TEST::TEST_FET_FET_Macros();
    return 0;
}

//---------------------------------------------------------------------------//
// end of src/harness/test/tstFET_FET_Macros.cc
//---------------------------------------------------------------------------//
