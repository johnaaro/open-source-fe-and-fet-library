//---------------------------------------------------------------------------//
/*!
 * \file  src/harness/test/tstCppStandard.cc
 * \brief Test of the Macro implementations
 * \note  Copyright (c) 2019 Oak Ridge National Laboratory, UT-Battelle, LLC.
 */
//---------------------------------------------------------------------------//

#include <stdexcept>

#include "CppStandard.hh"

namespace FET
{

/*!
 * \namespace TEST
 * \brief Contains all examples for the FE/FET library
 *
 * Contains all examples (i.e. test) for the FE/FET library. Tests are meant
 * to provide full coverage of the APIs, with the intent to cover all valid and
 * invalid use cases.
 *
 * Some methods primarly used by the TEST namespace are contained in the parent
 * FET namespace instead due to the structure of the TEST namespace only
 * existing within the test directories, not within the libraries themselves.
 * Further, consumers of the libraries may desire the provided functionality
 * for their own test or usage.
 */
namespace TEST
{

    //! \brief Simply prints the current C++ standard used
    void TEST_CppStandard()
    {
        print_cpp_standard();
    }


} // End of namespace TEST
} // End of namespace FET


int main()
{
    FET::TEST::TEST_CppStandard();
    return 0;
}

//---------------------------------------------------------------------------//
// end of src/harness/test/tstCppStandard.cc
//---------------------------------------------------------------------------//
