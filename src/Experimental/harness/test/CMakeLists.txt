##---------------------------------------------------------------------------##
##
## src/harness/test/CMakeLists.txt
##
## Copyright (C) 2019 Oak Ridge National Laboratory, UT-Battelle, LLC.
##---------------------------------------------------------------------------##
## CMAKE for the harness library test
##---------------------------------------------------------------------------##
cmake_minimum_required (VERSION 3.12.4)
PreventInSourceBuild()

##---------------------------------------------------------------------------##
## TESTING
##---------------------------------------------------------------------------##
set(DEPENDENCIES
  harness
  )

# C++ Std testing
create_test(
  "CppStandard"                      # Name of the test
  SOURCES "tstCppStandard.cc"        # Sources for the test
  DEPENDENCIES ${DEPENDENCIES}       # Libraries test requires
  )

# Macro testing
create_test(
  "FET_Macros"                          # Name of the test
  SOURCES "tstFET_Macros.cc"            # Sources for the test
  DEPENDENCIES ${DEPENDENCIES}      # Libraries test requires
  )

# Assertion testing
create_test(
  "FET_Assertions"                      # Name of the test
  SOURCES "tstFET_Assertions.cc"        # Sources for the test
  DEPENDENCIES ${DEPENDENCIES}      # Libraries test requires
  )

# Macro testing
create_test(
  "FET_DBC"                             # Name of the test
  SOURCES "tstFET_DBC.cc"               # Sources for the test
  DEPENDENCIES ${DEPENDENCIES}      # Libraries test requires
  )

# Comparison testing
create_test(
  "Comparisons"
  SOURCES "tstComparisons.cc"
  DEPENDENCIES ${DEPENDENCIES}
  )

##---------------------------------------------------------------------------##
##                   end of src/harness/test/CMakeLists.txt
##---------------------------------------------------------------------------##
