//---------------------------------------------------------------------------//
/*!
 * \file  src/harness/CppStandard.hh
 * \brief Declares various macros for the FET library
 * \note  Copyright (c) 2019 Oak Ridge National Laboratory, UT-Battelle, LLC.
 */
//---------------------------------------------------------------------------//
#ifndef src_harness_CppStandard_hh
#define src_harness_CppStandard_hh

#include <iostream>

//============================================================================//
/*!
 * \namespace FET
 * \brief Provides the Functional Expansion Tally library framework
 * \author Chase Juneau, Aaron Johnson, Katherine Wilsdon, Leslie Kerby
 *
 * Contains the entire Functional Expansion Tally (FET) library framework. This
 * includes all objects and features from the ground-up, including
 * design-by-contract functionality through complex FET generation and
 * management of a set of FETs through simple wrapper commands.
 *
 * The framework is divided into several sub-libraries, presently being:
 * - harness
 * - core
 * - Polynomials (Poly_Attr)
 * - Tallies
 * - Managers
 * - Experimental
 *
 * Each library builds upon the previous, adding functionality where desired or
 * deemed necessary to provide a standalone library with little to no
 * dependency on third party libraries.
 *
 */
namespace FET
{

//============================================================================//
/*!
 * \def FET_VERSION
 * \brief Defines the FET library version string
 *
 * Defines the FET library version string for FET usage
 */
#ifdef FET_VERSION
#undef FET_VERSION
#endif
#define FET_VERSION 1.0.0


//============================================================================//
/*!
 * \brief Defines the FET library author list
 *
 * Defines the FET library authors
 */
const std::string FET_authors =
    "Chase Juneau, Aaron Johnson, Leslie Kerby, Katherine Wilsdon";


//============================================================================//
/*!
 * \def CPP_STD
 * \example src/harness/test/tstCppStandard.cc Test of C++ standard macro
 *
 * Defines more simply the standard the compiler abides by
 */
#ifndef __cplusplus
    #error A C++ compiler is required for this library.
#else
    #if __cplusplus < 201103L   // Prior to C++11 standard
        #define CPP_STD 3
    #elif __cplusplus < 201402L   // Prior to C++14 standard
        #define CPP_STD 11
    #elif __cplusplus < 201703L   // Prior to C++17 standard
        #define CPP_STD 14
    #elif __cplusplus < 202002L   // Prior to C++20 standard
        #define CPP_STD 17
    #elif __cpluscplus >= 202002L // At/After C++20 standard
        #define CPP_STD 20
    #endif
#endif

// Error if C++11 isn't used
#if CPP_STD < 11
    #error This library requires the usage of C++11 or greater.
#endif


//============================================================================//
/*!
 * \brief Prints the library name, version, and authors
 * \param os A stream to print too
 */
void print_FET_library(std::ostream& os = std::cout) noexcept
{
    os << "Using the FET library (v. FET_VERSION), by " << FET_authors << "\n";
}


//============================================================================//
/*!
 * \brief Prints the C++ standard used by the compiler
 * \param os A stream to print too
 */
void print_cpp_standard(std::ostream& os = std::cout) noexcept
{
    os << "Compiled using the C++" << CPP_STD << " standard.\n";
}


//---------------------------------------------------------------------------//
} // end namespace FET

//---------------------------------------------------------------------------//
#endif // src_harness_CppStandard_hh
//---------------------------------------------------------------------------//
// end of src/harness/CppStandard.hh
//---------------------------------------------------------------------------//
