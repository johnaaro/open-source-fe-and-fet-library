//---------------------------------------------------------------------------//
/*!
 * \file  src/harness/Comparisons.hh
 * \brief Comparisons class declaration.
 * \note  Copyright (c) 2019 Oak Ridge National Laboratory, UT-Battelle, LLC.
 */
//---------------------------------------------------------------------------//
#ifndef src_harness_Comparisons_hh
#define src_harness_Comparisons_hh

#include <cmath>

#include "Constants.hh"

namespace FET
{

//===========================================================================//
/*!
 * \brief Flags if the values exactly match
 * \param lhs The left hand side of the equality
 * \param rhs The right hand side of the equality
 * \example src/harness/test/tstComparisons.cc Test of comparisons
 */
template<typename T, typename U = T>
bool is_equivalent(const T lhs, const U rhs = 0) noexcept
{ return lhs == static_cast<T>(rhs); }


//===========================================================================//
/*!
 * \brief Flags if the values are similar or approximate
 * \param lhs The left hand side of the equality
 * \param rhs The right hand side of the equality
 * \param delta The maximum difference by which 'lhs' and 'rhs' may vary
 * \example src/harness/test/tstComparisons.cc Test of comparisons
 *
 * Flags if the values provided are similar in value. Further, the user may
 * provide a difference \f$ \Delta \f$ by which to check for similarity.
 *
 * This is intended to provide a quick method to check for near equality of
 * float, double, and long double variables. Further, no change is required for
 * other integer types.
 */
template<typename T, typename U = T, typename V = T>
bool is_approximate(const T lhs, const U rhs = 0.0,
        const V delta = static_cast<V>(1.0E-12))
    noexcept
{ return std::abs(lhs - static_cast<T>(rhs)) < static_cast<T>(delta); }


//---------------------------------------------------------------------------//
} // end namespace FET

//---------------------------------------------------------------------------//
#endif // src_harness_Comparisons_hh
//---------------------------------------------------------------------------//
// end of src/harness/Comparisons.hh
//---------------------------------------------------------------------------//
