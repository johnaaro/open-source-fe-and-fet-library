//---------------------------------------------------------------------------//
/*!
 * \file  src/Managers/Manager_FET.hh
 * \brief Manager_FET class declaration.
 */
//---------------------------------------------------------------------------//
#ifndef src_Managers_Manager_FET_hh
#define src_Managers_Manager_FET_hh

#include "Manager_Base.hh"

#include <map>
#include <string>
#include <vector>

#include "FET_Base.hh"
#include "Particle.hh"
#include "Separable_FET.hh"

namespace FET
{

//===========================================================================//
/*!
 * \class Manager_FET
 * \brief Simplified FET manager
 *
 * Contains any number of underlying FET objects and interfaces all provided
 * data to them. Consumers may use multiple managers for different FET types,
 * such as separable, convolved, track-length, surface crossing, and so forth.
 *
 * \todo Make default template for the managed data type
 */
/*!
 * \example src/Managers/test/tstFETManager.cc
 *
 * Test of the Manager_FET object
 */
//===========================================================================//
template<class managed_FET = FET_Base<std::vector<double>>, class data_packet = Particle<>>
class Manager_FET : public Manager_Base<managed_FET>
{
    using Base     = Manager_Base<managed_FET>;
public:

    //@{
    //! Public type aliases
    using data_type       = double;
    using Vec_Dbl         = std::vector<double>;
    using Vec_Vec_Dbl     = std::vector<Vec_Dbl>;
    //@}


public:
    // >>> CONSTRUCTOR/DESTRUCTORS

    //! \brief Basic Constructor
    Manager_FET(typename Base::string name,
                 typename Base::string desc = "",
                 const std::vector<managed_FET> &items = {})
        : Base(name, desc, items)
    { /* * */ }

    //@{
    //! Copy control (copy, move, and destructor)
    Manager_FET(const Manager_FET&) = default;
    Manager_FET& operator=(const Manager_FET&) = default;
    Manager_FET(Manager_FET&&) = default;
    Manager_FET& operator=(Manager_FET&&) = default;
    virtual ~Manager_FET() = default;
    //@}


public:
    // >>> GETTTERS

    //! \brief Returns the FET object type
    virtual const std::string type() const noexcept
    { return "FET Manager"; }

    Vec_Vec_Dbl coefficients() const override
    {
        //todo::add functionality for multiple managed fet's, currently assuming only 1
        return this->d_data[0].coefficients();
    }

    Vec_Vec_Dbl variance() const override
    {
        //todo::add functionality for multiple managed fet's, currently assuming only 1
        return this->d_data[0].variance();
    }

    Vec_Dbl flux() const override
    {
        //todo::add functionality for multiple managed fet's, currently assuming only 1
        return this->d_data[0].flux();
    }

    Vec_Vec_Dbl locations() const override
    {
        return this->d_data[0].locations();
    }

    Vec_Dbl energies() const override
    {
        return this->d_data[0].energies();
    }

public:
    // >>> INTROSPECTION

    //! \brief Resets the managed object (clears data in all anticipated cases)
    void reset() noexcept { DerivedMethodLoop(reset()); }

    //! \brief Pre-processes the managed objects
    void pre_process() noexcept { DerivedMethodLoop(pre_process()); }

    //! \brief Processes the data_packet object in each of the managed objects
    void process(const data_packet& data)
    { score(data); }

    //! \brief Alias for \c process
    void score(const data_packet& data) {
        //DerivedMethodLoop(score(data));
        for(unsigned int i=0;i<this->d_data.size();++i)
        {
            //must specify this-> to be able to look out of function scope
            //to parent members
            this->d_data[i].score(data);
        }
    }

    //! \brief Alias for \c process evaluate flux
    void evaluate_flux(const data_packet& data){
        for(unsigned int i=0; i < this->d.data.size(); ++i){
            this->d_data[i].evaluate_flux(data);
        }
    }

    //! \brief Finalizes the data contained in the Managed object
    void finalize(const data_type val = 1.0)
    { DerivedMethodLoop(finalize(val)); }

}; // end class Manager_FET

 //---------------------------------------------------------------------------//
} // end namespace FET

//---------------------------------------------------------------------------//
#endif // src_Managers_Manager_FET_hh
//---------------------------------------------------------------------------//
// end of src/Managers/Manager_FET.hh
//---------------------------------------------------------------------------//
