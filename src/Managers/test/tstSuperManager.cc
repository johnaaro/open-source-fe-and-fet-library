//---------------------------------------------------------------------------//
/*!
 * \file  src/Managers/test/tstFETManager.cc
 * \brief Test of the FETManager object
 */
//---------------------------------------------------------------------------//

#include <stdexcept>

#include "FET_DBC.hh"
#include "Manager_FET.hh"
#include "Managed_Object.hh"

namespace FET
{
namespace TEST
{
    // Standard manager for FET objects
    using Sub_Manager  = Manager_FET<Managed_Object<>, double>;

    // Manager of FET managers
    using Main_Manager = Manager_FET<Sub_Manager, Sub_Manager::data_type>;

    //! \brief Creates a simple base manager
    Main_Manager
    TEST_FETManager_Create()
    {
        // Create some sub-managers
        Sub_Manager manny(
                "Manny the manager",
                "Manny is a level 3 manager");
        for (unsigned int i = 3; i < 6; ++i)
        { manny.insert(Managed_Object<>(i)); }

        Sub_Manager fanny(
                "Fanny the manager"
                "Fanny is a level 0 manager");
        for (unsigned int i = 23; i < 26; i++)
        { fanny.insert(Managed_Object<>(i)); }


        // Assemble the big boss
        Main_Manager big_boss(
                "Big boss",
                "The super big boss at my company",
                std::vector<Sub_Manager>{manny, fanny});

        return big_boss;

    }


    //! \brief Propagates calls to all sub-managers
    void TEST_FETManager_Operate(
            Main_Manager manager)
    {
        manager.reset();
        manager.score(5.0);
        manager.finalize(1.0);
    }


    //! \brief Performs all test of the \c FETManager object
    void TEST_FETManager()
    {
        auto foo = TEST_FETManager_Create();
        TEST_FETManager_Operate(foo);
    }

} // End of namespace TEST
} // End of namespace FET


int main()
{
    FET::TEST::TEST_FETManager();
    return 0;
}

//---------------------------------------------------------------------------//
// end of src/Managers/test/tstFETManager.cc
//---------------------------------------------------------------------------//
