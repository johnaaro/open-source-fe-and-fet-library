//---------------------------------------------------------------------------//
/*!
 * \file  src/Managers/test/tstBaseManager.cc
 * \brief Test of the BaseManager object
 */
//---------------------------------------------------------------------------//

#include <iostream>
#include <stdexcept>

#include "FET_DBC.hh"
#include "Manager_Base.hh"
#include "Managed_Object.hh"

namespace FET
{
namespace TEST
{
    //! \brief Creates a simple base manager
    Manager_Base<Managed_Object<>>
    TEST_BaseManager_Create()
    {
        Manager_Base<Managed_Object<>> manny(
                "Manny the manager",
                "Manny is a level 3 manager");
        Insist(manny.empty(), "Manager not empty at construction");
        unsigned int key = 0;
        manny.insert(Managed_Object<>(key));
        Insist(manny.contains(key),
                "Failed to insert object into manager");
        manny.erase(key);
        Insist(manny.size() == 0, "Failed to erase element from manager.");

        // Insert some elements and return object
        for (unsigned int i = 3; i < 6; ++i)
        { manny.insert(Managed_Object<>(i)); }
        return manny;
    }


    //! \brief Performs all operations provided by the base manager
    void TEST_BaseManager_Operate(
            Manager_Base<Managed_Object<>> manager)
    {
        manager.describe(std::cout);
    }


    //! \brief Performs all test of the \c BaseManager object
    void TEST_BaseManager()
    {
        auto manny = TEST_BaseManager_Create();
        TEST_BaseManager_Operate(manny);
    }

} // End of namespace TEST
} // End of namespace FET


int main()
{
    FET::TEST::TEST_BaseManager();
    return 0;
}

//---------------------------------------------------------------------------//
// end of src/Managers/test/tstBaseManager.cc
//---------------------------------------------------------------------------//
