//---------------------------------------------------------------------------//
/*!
 * \file  src/Managers/test/tstFETManager.cc
 * \brief Test of the FETManager object
 */
//---------------------------------------------------------------------------//

#include <stdexcept>

#include "FET_DBC.hh"
#include "Manager_FET.hh"
#include "Managed_Object.hh"

namespace FET
{
namespace TEST
{
    //! \brief Creates a simple base manager
    Manager_FET<Managed_Object<>, double>
    TEST_FETManager_Create()
    {
        Manager_FET<Managed_Object<>, double> manny(
                "Manny the manager",
                "Manny is a level 3 manager");

        // Insert some elements and return object
        for (unsigned int i = 3; i < 6; ++i)
        { manny.insert(Managed_Object<>(i)); }
        return manny;
    }


    //! \brief Performs all operations provided by the base manager
    void TEST_FETManager_Operate(
            Manager_FET<Managed_Object<>, double> manager)
    {
        manager.reset();
        manager.score(5.0);
        manager.finalize(1.0);
    }


    //! \brief Performs all test of the \c FETManager object
    void TEST_FETManager()
    {
        auto manny = TEST_FETManager_Create();
        TEST_FETManager_Operate(manny);
    }

} // End of namespace TEST
} // End of namespace FET


int main()
{
    FET::TEST::TEST_FETManager();
    return 0;
}

//---------------------------------------------------------------------------//
// end of src/Managers/test/tstFETManager.cc
//---------------------------------------------------------------------------//
