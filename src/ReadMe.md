# FE and FET Library Source Code and Practices

The main source code for the FE and FET library is contained here. General practices, albeit vague, are outlined briefly here.


## Code Modifications

A basic changelog is utilized to characterize and track changes to the FE/FET code, maintained here. The [ChangeLog file](../ChangeLog.md) contains further detail.


## In-Source Documentation

The source code utilizes doxygen-compliant commenting practices to aid in automatically generating documentation for the code, its structure, and its usage/API. To generate documentation via Doxygen, simply make a build directory and Makefile via `CMake`, and `build docs` to generate both `html` and `LaTeX` documentation for the Shift FET code. Please refer to pre-existing source files to understand the style and general practices utilized for this library.

To better understand how to utilize doxygen, please refer to the [Doxygen manual](http://www.doxygen.nl/index.html).


## Contribute

When contributing to this repository, please first discuss the change you wish to make via issue, email, or any other method with the owners of this repository before making a change. See the contribute section in the [main page](../ReadMe.md) for more details.


## Questions?

Please direct all questions to the active ExaSMR team, a sub-set of the Computational Engineering And Data Science (CEADS) group at the Idaho State University (ISU), hosted by [Dr. Leslie Kerby](mailto:kerblesl@isu.edu). As of the most recent modification of this file, this team includes, but may not be limited to, the following:

Active developers:
+ [Dr. Leslie Kerby](mailto:kerblesl@isu.edu)
+ [Aaron Johnson](mailto:johnaaro@isu.edu)
+ [Chase Juneau](mailto:junechas@isu.edu)
+ [Emily Elzinga](mailto:elziemil@isu.edu)
+ [Kallie McLaren](mailto:mclakall@isu.edu)
+ [Patience Lamb](mailto:lambpati@isu.edu)

Previous developers:
+ [Katherine Wilsdon](mailto:wilskat7@isu.edu)
+ [Steven Wacker](mailto:wackstev@isu.edu)
+ [Ryan Stewart](mailto:stewryan@isu.edu)



## Acknowledgments

This work was supported by the ExaScale Computing Project (17-SC-2-SC).

<img src="../images/logos/ecp-logo.png" width=150>

<img src="../images/logos/ISULogo.png" width=125>
<img src="../images/logos/ornlLogo-Green.png" width=100>
