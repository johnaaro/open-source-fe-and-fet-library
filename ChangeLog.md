# FE/FET Library Changelog

All notable changes to this project are to be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

> Note that very little versioning is utilized within this project at present.


## [Unreleased]

This section documents all expected changes within the next few releases. These features will include:

## 2020-10-27: v0.0.2 (Merge ...)

Updated contributor and active developer lists to accurately reflect current team
and give proper credits.

### Changed:
+ Added new contributors
+ Moved previous contributors to inactive list

-------------------------------------------------------

## 2020-XX-XX: v0.0.3 (Merge !36, ...)

Added functionality to complete API path for consumers. Improved FET functionality and added varying FET types.

### Added
+ Completed functionality for library install (make install) into local usr/include for use in third party programs
+ Added FETConfig.cmake as dependency for library install (make install)
+ Implementation of Shift_FET_API as link between shift and the FET library

### Changed:
+ Update to ChangeLog
+ Update to pin_FE_6order.omn to proper omnibus input to test with, new omnibus input coming from Patience
+ Rename of appropriate harness files to have "FET_" prefix to not have issue with Omnibus files of similar name
+ Update to all includes referencing previously renamed files
+ Add missing dependencies for Managers/CMakeLists.txt
+ Rename of various template types in use, ex. managed_type is now managed_FET, due to clash with Omnibus over similar naming schemes
+ Fix to omnibus/omn/tally.py to match input style requirements for new information needed for FETs
+ FET API implementation in Shift_Tallies.hh/.cc changed to match new naming convention and usage for FETs
+ Shift now builds FET as a third party library only if it can find the package
+ Bugfixes in FET_Base with templating
+ Symlinker tool updated for correct linking only. Old files removed from linking

### Removed:
+ Old Shift output functionality removed TODO:: add new from new shift functionality
+ Shift Tally_Writer functionality commented out temporarily  for same reason as above
+ Various leftover references to old FE_Cell_Tally functionality removed
+ No longer need to track omnibus/shift_managers/cmakelists and shiftinstantiate.cmake




## 2020-XX-XX: v0.0.2 (Merge !34, ...)

Added functionality to complete API path for consumers. Improved FET functionality and added varying FET types.

### Added
+ Abstract manager object for general manager, FET manager, etc. with associated examples/tests.
+ TODO: Add surface crossing, multi-dimensional track length flux, current, etc., FET objects

### Changed:
+ TODO: Basic FET layout

### Removed:
+ TODO: Deprecated FET Objects




## 2020-05-08: v0.0.1 (Merge !26)

Provides a major refactor for the majority of the pre-existing FE/FET library. Provides several new utilities for FE/FET creation and usage.

### Added:
+ Various utilities to abstract data
+ Templatization for the type of data utilized (flexible for float, double, etc.)
+ Some Shift tools (DBC, Logger) for usage here
+ Tests for all supported objects
+ Improved style, const-correctness, etc., of library
+ Split the library from the Shift code, with own CMake modules and scripts

### Changed
+ Moved pre-existing code to a [deprecated](./src/deprecated) folder. These files may need to be extracted for future use.
+ Updated documentation
+ Setup of prior polynomials, if existed

### Removed
+ Dependencies on Shift and external libraries



> IMPORTANT! The remainder of this ChangeLog addressed DEPRECATED features/functionality.


## [Commit-merge hash]: 2019-08-01 (Merge !21 !22)

Overview of commit-merge hash

### Added:
+ Add various omnibus test files sourced from previous work done on the project. All test files are held in /examples and separated in sub directories based on where they came from/what they do
+ Add various python files related to omnibus post-processing
+ Add HDF5 write capabilities, currently writing separable and convolved coefficients
+ Add get_separable_coefficients() to extract a single dimensions coefficients from the current convolved matrix
+

### Changed
+ Further modify directory structure to more easily separate files into appropriate locations based on which package they belong to, e.g. Omnibus, Shift, Transcore, etc
+ Modify Tally_Writer::write to accept an FE_Cell_Tally rather than FE_Tally_Result
+ Update symlink script
+ Update ReadMe
+ Update Changelog entries

### Removed
+ Include of things removed (of importance) here. This may include test cases, class-functions, etc.
+ Remove various +1 and off by one errors throughout

-------------------------------------------------------

## [7494c364]: 2019-07-15 (Merge !20)

### Added:
+ Add definition for const shared pointer for fe cell tallies
+ Add getter for const shared pointer for fe cell tallies
+ Add spheres.gg.omn (required by mesh_tallies.omn)

### Changed
+ Clean up directory structure
+ Modify ReadMe files
+ Update symlink script for new directory structure
+ Include most omnibus files in .gitignore

-------------------------------------------------------

## [7bf828e8]: 2019-07-10 (Merge !19)

### Added:
+ Changelog entries for previous merges

### Changed
+ Move files around to create a directory structure
+ Update setupSymlinks script
+ Update documentation

### Removed
+ Removed unnecessary lines in cmake files

### Fixed
+ Fix for const correctness in overridden functions in fe_cell_tally.cc/.hh

-------------------------------------------------------

## [998573b1]: 2019-07-03 (Merge !17 !18)

Overview of commit-merge hash

### Added:
+ Add mesh_tallies.omn for reference/outline for creating input .omn files
+ Add functional_expansion to list of valid tally types in shift_tallies.cc
+ Add README for new tools directory
+ Add Docs/ directory for better structure of the Shift FET project
+ Add configuration file for generating documentation in Docs/ directory via doxygen

### Changed
+ Moved tools into previously created tools directory
+ Improvement to setupSymLinks.sh

-------------------------------------------------------

## [97123ec3]: 2019-07-01 (Merge !15 !16)

Readability updates and creation/addition of tools directory to keep developed tools bundled together for convenience.

### Added:
+ README addition of tools directory and description of various tools developed

### Changed
+ Readability updates in FE_Tally_Result.cc in switch/case statements
+ 80 column rule adjustments
+ Developed tools moved to new directory

-------------------------------------------------------

## [7b6a7137]: 2019-07-02 (Merge !14)

Update to new Exnihilo/Shift changes corrected. Code builds to 100% after errors corrected.

### Changed
+ Manual merge update to new Exnihilo/Shift code, included changes to CMAKE files, location of included header files, and additions/deletions from other developers that would be included in a standard merge

### Fixed
+ Override/-fpermissive errors overriding from inherited tally.hh fix by adding mutable to d_fe_results and p_coeff in fe_cell_tally.hh/.cc
+ Fix errors where mc::Reaction and robus::reaction::Reaction_type were being used incorrectly as params and pointers
+ Fix pointer equals int in fe_cell_tally_attr

-------------------------------------------------------

## [9f3dccb9]: 2019-06-25 (Merge !13)

Refactor of sections of Ryan's code related to implementation of Legendre Polynomial calculation and large else/if blocks that were changed to switch/case. All previous code from Ryan is left as is except for an off by one error fix described below to facilitate testing the improvements made by our refactors. Currently our code is left commented out.

### Added:
+ Add tstLegendrePolynomial.cc to test runtimes of three variations of calculation style, a non-memoized fully recursive approach, a memoized fully recursive approach, and a memoized hybrid direct calculation and recursive approach

+ Add grphLegendrePolynomial.py to graph the runtimes of three variations of calculation style and display the margin of error between multiple runs

### Changed
+ Change implementation of Pn() from memoized full recursive formula to a hybrid direct/recursive memoized formula
+ Change large else if blocks to switch/case statements due to the number of cases involved. Switch/case have better performance and readability with more than five cases

### Fixed
+ Fix off by one error in Pn() calculations. Vector for each order was created, but was one short, e.g. if order 5 specified, a 5 value vector was created and filled with values for orders 0,1,2,3,4, thereby actually missing the 5th order calculation

-------------------------------------------------------

## [f83ca4e7]: 2019-06-25 (Merge !8)

### Fixed
+ Update calls in Ryan's code to external functions that were changed in the new Shift files and commented out Ryan's outdated code

-------------------------------------------------------

## [ca9af2ae]: 2019-06-21 (Merge !7)

Merging in new code changes from Shift into our repository

### Added:
+ New blocks of code from other Shift developers merged into our repo

### Removed
+ Code from other Shift developers removed during merge

-------------------------------------------------------

## [e53bfd2a] 2019-06-20 (Merge !5, !6)

### Added:
+ Created a script to aid in linking the FET code to a Exnihilo source code base (see Merge !5, !6)

-------------------------------------------------------

## [da26ee61]: 2019-06-04 (Merge !4)

Addition of ReadMe and other documentation that was missing at the time of acquisition of the project from Ryan's previous work. Documentation was found to be lacking in some regards and this is meant to address this lack. A changelog has also been added to better and more clearly map the overall scope of the changes made and the reasoning behind them.

### Added:
+ Add missing comments throughout files
+ Add ReadMe.md
+ Add functionalExpansionDetails.md
+ Add functionalExpansionTallyDetails.md
+ Add changelog.md

### Fixed:
+ Adjusted existing comment style to be in line with Doxygen standards
+ Incomplete, inconsistent, and incorrect comments corrected

-------------------------------------------------------

## [fed21951]: 2019-05-28 (Merge !2, !3)

Merged **all** of Ryan's work previously into the master branch. Additions, modifications, removals, and bugfixes are *not* documented here at this time regarding this merge. Refer to the commit hash for further details.

-------------------------------------------------------

## TEMPLATE: [Commit-merge hash]: YYYY-MM-DD (Merge !X)

Overview of commit-merge hash

### Added:
+ Include what was added here (functionality, features, etc.)

### Changed
+ If appropriate, add any changes here that may be noteworthy or of importance (ie. API modifications, implementation modifications, etc.)

### Removed
+ Include of things removed (of importance) here. This may include test cases, class-functions, etc.

### Fixed
+ Include all bug fixes that were embedded in the commit/merge here, if you can. Perhaps reference the commit hash for this (possibly excessive).
